/**
 * Created by   :宁建浩
 * Created time :2017/5/12/9:38
 * File explain :提现
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    bootTable: ["bootstrap-table-dist/dist/js/bootstrap-table"],
    bootTableCN: ["bootstrap-table-dist/dist/locale/bootstrap-table-zh-CN"],
    config: ["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["bootTable"], function () {
    require(["layer", "zui", "boot", "bootTableCN", "config"], operationCodeFun)
  });
});
function operationCodeFun() {
  "use strict";
  var QHConfig = $("body").data("config");
  var manageInfo = QHConfig.getSearchDataFun();//获取地址栏信息
  var bankList = null;
  //layer框架配置
  layer.config({
    path: "../../libs/layer/"
  });
  getBankList();
  function getBankList() {
    $.ajax(tokenAjax({
      type: "GET",
      async: false,
      url: QHConfig.url.bankCardList,
      success: bankRender
    }));
  }

  //跳转到添加银行卡页面
  $(".payCash-addBtn").on("click", jumpBankList);
  //余额渲染
  $(".cl-balance").html(manageInfo.balance);
  //点击下一步事件
  $(".next-step").on("click", nextStepFun);
  //选择银行事件
  $("#id-bank-con").on("click", ".payCash-add", bankChangeFun);
  //提现金额事件
  $(".payCash-withdrawal").on("blur", moneyFun);
  //添加银行卡回调
  function jumpBankList() {
    layer.open({
      type: 2,
      title: false,
      closeBtn: 0,
      area: ["100%", "100%"],
      scrollbar: false,
      content: `../bank-manage/bank-manage.html?&balance=${manageInfo.availableBalance}&operName=${manageInfo.operName}&transfer=withdraw`
    });
  }

  //点击下一步事件回调
  function nextStepFun() {
    var balance = parseFloat($(".payCash-withdrawal").val());//提现金额
    var el = $("#id-bank-con").find(".payCash-choose-checked");//所选银行的标签
    var bankjson = el.data("bank");
    if (!bankjson) {
      $(".bank-card-show").show().find(".bank-toggle").html("请选择要提现的银行卡");
      return
    } else {
      $(".bank-card-show").hide();
    }
    var moneyFlag = false;//验证flag
    if (!balance) {
      $(".payCash-money-ifo").show().find(".cl-balance").html(`请输入提现金额`);
      moneyFlag = true;
    } else if (balance > manageInfo.balance) {
      moneyFlag = true;
      $(".payCash-money-ifo").show().find(".cl-balance").html(`您的余额只有${manageInfo.balance}元`);
    }
    if (!moneyFlag) {
      var msg = {
        bankCardId: bankjson.id,//银行卡的id
        accountId: bankjson.accountId,//账户id
        operName: manageInfo.operName,//操作者账户
        amount: balance,//金额
        bankImg: bankjson.logo,//logo
        bankName: bankjson.bankName,//银行名称
        bankCard: bankjson.bankcardNo,//银行卡
        isPublic: bankjson.isPublic,//是否对公
        bankUnionName: bankjson.bankUnionName,//支行名称
        bankUnionCode: bankjson.bankUnionCode,//支行code
        bankTime: $("#id-money-time").html()//
      };
      if (balance > 50000 && msg.isPublic == "1" && !msg.bankUnionCode) {
        msg.el = 'id-not-pass';
      } else {
        msg.el = 'id-yes-pass';
      }
      layer.open({
        type: 2,
        title: false,
        closeBtn: 0,
        area: ["100%", "100%"],
        scrollbar: false,
        content: `../person-present/person-present.html?bankCardId=${msg.bankCardId}&accountId=${msg.accountId}&operName=${msg.operName}&amount=${msg.amount}&bankImg=${msg.bankImg}&bankName=${msg.bankName}&bankCard=${msg.bankCard}&payType=${msg.isPublic}&el=${msg.el}&bankUnionName=${msg.bankUnionName}&bankTime=${msg.bankTime}&userCode=${manageInfo.userCode}&bankCode=${bankjson.bankCode}`
      });
    }
  }

  //银行讯息渲染
  function bankRender(res) {
    var html = "";//银行容器
    var cls = "";//判断是否默认选择
    bankList = res.data;//银行卡列表
    if (bankList && bankList.length != 0) {
      $.each(bankList, function (k, v) {
        if (v.bankName === "中国建设银行") {
          if (!v.isPublic) {
            cls = "ed";
          } else {
            cls = "ed";
          }
        } else {
          cls = "";
        }
        var txt = '<span class="payCash-company-logo">企业</span>';
        html += `<div class="payCash-add">
              <!-- 现在为未选择的时候 选中的时class名为payCash-choose-checked -->
              <span class="payCash-choose-check${cls}" data-bank='${JSON.stringify(v)}'></span>
              <span class="payCash-choose-bankLogo"><span style="background:url(${v.logo}) no-repeat;background-size:cover"></span>${v.bankName}</span>
              <span class="payCash-bankTail ">尾号：<span>${v.bankcardNo.substring(v.bankcardNo.length - 4, v.bankcardNo.length)}</span></span>
              <span class="paycash-bankType payCash-company-bankType">储蓄卡</span>${v.isPublic ? "" : txt}
          </div>`;

      });
      $("#id-bank-con").html(html);
    }
    var view = $("#id-bank-con").find(".payCash-choose-checked");
    view.each(function () {
      if ($(this).siblings("span").hasClass("payCash-company-logo")) {
        $(this).parent().siblings(".payCash-add").find(".payCash-choose-checked").removeClass("payCash-choose-checked").addClass("payCash-choose-check")
      }
    });
  }

  //选择银行
  function bankChangeFun() {
    $(this).addClass("bank-selected").find(".payCash-choose-check").addClass("payCash-choose-checked").removeClass("payCash-choose-check").parent().siblings(".payCash-add").removeClass("bank-selected").find(".payCash-choose-checked").removeClass("payCash-choose-checked").addClass("payCash-choose-check")
  }

  //提现金额事件
  function moneyFun() {
    var txt = this.value;
    var reg = /^\d+(?:\.\d{1,2})?$/;
    if (!txt) {
      $(".payCash-money-ifo").show().find(".cl-balance").html("请输入提现金额");
    } else {
      if (!reg.test(txt)) {
        this.value = "";
      } else {
        $(".payCash-money-ifo").hide();
        if (txt <= 50000) {
          $("#id-money-time").html("2小时内到账");
        } else {
          $("#id-money-time").html(timeComputed());
        }
      }

    }
  }

  //大额计算到账事件
  function timeComputed() {
    var d = new Date();
    var xq = d.getDay(),
      h = d.getHours(),
      m = d.getMinutes(),
      sj = (h < 10 ? '0' : '') + h + ':' + (m < 10 ? '0' : '') + m;

    if (xq >= 1 && xq <= 5) {
      return "1-3天内到账";
    } else if (xq == 6) {
      return "3-5天内到账";
    } else if (xq == 7) {
      return "2-4天内到账";
    }
  }
}