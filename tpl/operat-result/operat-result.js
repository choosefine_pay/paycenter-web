/**
 * Created by   :wangshuzhong
 * Created time :2017/5/11
 * File explain :施小包账户-充值--成功页面
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"]
  }
});
require(["jquery"], function () {
  require(["layer","zui","boot"], operationCodeFun)
});
//代码容器

function operationCodeFun() {
  var str = location.search;
  var json = {};
  str = str.slice(1).split("&");
  for (var i = 0; i < str.length; i++) {
    var result = str[i].split("=");
    json[result[0]] = result[1];
  };
  //layer框架配置
  layer.config({
    path:"../../libs/layer/"
  });
  //页面渲染
  if(json.success=="true"){
    $(".recharge-success").show();
  }else if(json.success=="false"){
    $(".recharge-fail").show();
  }
  /*事件操作*/
  var time = 5;
  setInterval(function(){
    time--;
    $(".cmn-time").text(time);
    if(time < 0){
      window.close();
    }
  },1000);
  /*to do 其他函数*/

}