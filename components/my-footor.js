define(['vue'],function(Vue){
	function getFootTemplate(){
		return '<div class="-footor">版权所有:趋恒科技</div>'
	}
	var footor=Vue.extend({
		template: getFootTemplate()
	});
	Vue.component('my-footor', footor);
});