/**
 * Created by   :Wangshuzhong
 * Created time :2017-04-05
 * File explain :交易账务--月交易明细--应收款
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    bootTable: ["bootstrap-table-dist/dist/js/bootstrap-table"],
    bootTableCN: ["bootstrap-table-dist/dist/locale/bootstrap-table-zh-CN"],
    dateTime: ["datetimepicker/js/datetimepicker"],
    comConfig: ["../common/js/common-config"],
    QHPagination: ["../common/js/qhpager"],
    temp: ["template"]
  }
});
require(["jquery"], function () {
  require(["QHPagination", "bootTable"], function () {
    require(["temp", "zui", "layer", "dateTime", "bootTableCN", "comConfig"], operationCodeFun)
  })
});
function operationCodeFun(template) {
  // 由于使用require框架整合，我们需要为layer.js配置读取css文件的路径
  layer.config({
    path: "../../libs/layer/"
  })

  //变量声明
  var QHConfig = $("body").data("config");
  var roleId = window.sessionStorage.getItem("seller_role");
  var thisMonth = QHConfig.getSearchDataFun();//获取上个页面传过来的年月
  var yearMonth = thisMonth.operationDate.split("-");//拆分年月
  var tableHead = $("#id-user-account-table");//伪表格表头
  var pager = window.qhPaginationFactroy("receivable");//分页器功能获取
  var thisStartTime = thisMonth.operationDate + "-01";//开始时间
  var thisEndTime = thisMonth.operationDate + "-" + getLastDay(yearMonth[0],yearMonth[1]);//结束时间
  var _Data = {};//表格数据容器
  var detailCon = null; // 展开详情容器;
  var _detailData = {name: ""}; // 详情表格数据 name 表格名

  //表格数据请求参数
  var ajaxData = {
    pageNum : 1,
    pageSize : 10,
    startTime : thisStartTime,
    endTime : thisEndTime
  };
  // 展开详情所需参数
  var detailParams = {
    id:0
  };

  // 表格数据渲染参数
  var fakeTableOpt = {
    shipmentNumber : {name : "发货单号：", clsName : "cmn-detail-shipmentNumber"},
    sendTime : {name : "发货日期：", clsName : "cmn-detail-sendTime"},
    categoryName : {name : "品种：", clsName : "cmn-detail-categoryName cmn-breed-text"},
    projectName : {name : "项目：", clsName : "cmn-detail-projectName cmn-project-text"},
    totalAmountReceivable : {name : "应收总金额（元）：", clsName : "cmn-detail-totalAmountReceivable"}
  };

  // 分页栏渲染参数
  var pagerOptions = {
    tag_Class: 'cmn-page_tag',//分页标签默认样式
    tag_on_Class: 'cmn-page_tag_on',//分页标签选中样式
    input_Class: 'cmn-pageInput',//分页搜索框样式
    showTags: 5,//标签省略距离
    pageSizes:[],
    pageSearch: true//是否启用页面搜索跳转
  };
  //end

  //页面初始化
  //角色为材料时；没有交易类型可选
  console.log(roleId)
  if(roleId == "mater") {
    $(".cmn-buyType").hide();
  }
  QHConfig.chooseDate(); // 日期选择初始化
  getProjectName();//获取项目名称
  tableHead.bootstrapTable();//应收款表格渲染

  // 伪表格生成文本前置
  QHConfig.fakeTable($("#fakeTableCon"), "lg", fakeTableOpt);
  //end

  //事件绑定
  //点击面包屑返回交易账户页面
  $(".cmn-backTo-tradeAccount").on("click",function() {
    parent.layer.closeAll();
  });
  //点击前往实收款页面
  $("#id-buy-btn-rent").on("click",function() {
    //实收款页面地址
    var _yearMonth = "operationDate="+thisMonth.operationDate;
    var str = "../mdu-month-trade-receipts/mdu-month-trade-receipts.html?"+_yearMonth;
    var turnEle = $(this).find("a");

    turnEle.attr("href",str);
    turnEle[0].click();
  });
  // 发货日期开始时间
  $("[data-name='receiveStartTime']").on("change", function () {
    $("[data-name='receiveEndTime']").datetimepicker("setStartDate", $(this).val());
  });
  // 发货日期结束时间
  $("[data-name='receiveEndTime']").on("change",function(){
    $("[data-name='receiveStartTime']").datetimepicker("setEndDate", $(this).val());
  });
  // 清空条件按钮点击事件绑定
  $("#id-acc-clear-btn").on("click", function () {
    $(".cmn-query-container").find("[data-name]").val(""); // 清空查询条件
    $("[data-name='receiveStartTime']").datetimepicker("setStartDate", new Date(0)).datetimepicker("setEndDate", new Date());
    $("[data-name='receiveEndTime']").datetimepicker("setStartDate", new Date(0)).datetimepicker("setEndDate", new Date());
    $.extend(ajaxData, getQueryData()); // 更新查询请求数据
  });
  // 查询按钮点击事件绑定
  $("#id-acc-query-btn").on("click", function () {
    $.extend(ajaxData, getQueryData()); // 更新查询请求数据
    fakeTableRefresh(); // 刷新伪表格
  })[0].click();// 默认点击查询按钮进行表格初始化
  //未输入框去除特殊字符
  $("[data-name='shipmentNumber'],[data-name='merchandiseName']").on("blur",QHConfig.specialCharTest);
  //end

  //to do 其他功能函数
  /*
   * 根据年份获取项目名称，渲染到页面上
   * @author wangshuzhong
   * @Date   2017/4/07
   * */
  function getProjectName() {
    $.ajax(tokenAjax({
      url:QHConfig.url.projectByTime,
      data:{yearDate:thisMonth.operationDate},
      dataType:"json",
      success:function(data){
        var _data = data.data;
        var oHtml = "<option value=''>全部</option>";

        if(_data) {
          for(var i=0;i<_data.length;i++) {
            oHtml += `<option value="${_data[i].projectCode}">${_data[i].projectName}</option>`;
          }
        }
        $("#id-projectName").html(oHtml);
      }
    }))
  }
  /*
   * 查询条件收集
   * @return queryMsg 查询数据，用户选择或输入需要查询的内容
   * @author wangshuzhong
   * @Date   2017/4/07
   * */
  function getQueryData() {
    var queryMsg = {}; // 声明数据载体
    $(".cmn-query-container").find("[data-name]").each(function () {
      var k = $(this).data("name"); // 字段名
      var val = $(this).val(); // 值

      queryMsg[k] = val; // 注入载体
    });
    return queryMsg; // 返回查询数据
  }

  /*
   * 伪表格刷新
   * 仿bootstrapTable刷新操作
   * @author wangshuzhong
   * @Date   2017/4/05
   * */
  function fakeTableRefresh() {
    if(pager.getPage()){
      ajaxData.pageNum = pager.getPage();
    }
    // 数据请求
    fakeTableDataAjaxFun();
    // 生成伪表格HTML文本
    var fakeHtml = template("fakeTableHtml", _Data);

    // 注入表格容器
    $("#fakeTableCon").html(fakeHtml);
    renderPagerFun();

    //发货日期转换
    sendTimeAndTotalPriceTurn();

    // 伪表格展开收起按钮点击
    $(".cmn-hide-show-btn").on("click", hideAndShowClick);
    QHConfig.fakeTableDetailFun("lg");
  }
  /*
   * 伪表格数据请求
   *
   * */
  function fakeTableDataAjaxFun() {
    for(var key in ajaxData) {
      if(ajaxData[key] == "") {
        delete ajaxData[key];
      }
    };//删除为空参数
    $.ajax(tokenAjax({
      url: QHConfig.url.receivables,
      data: ajaxData,
      async: false,
      dataType: "json",
      success: function (data) {
        _Data = data.data;
        if(_Data.list.length>0){
          $("#tip").hide();
        }else {
          $("#tip").show();
        }
      }
    }));
  }
  /*
   * 渲染分页器
   * 引用组件功能对分页器渲染
   * */
  function renderPagerFun() {
    pager.init($("#id-table-pager"), fakeTableRefresh, pagerOptions);// 初始化分页器对象
    pager.setParameters(_Data.pageNum, _Data.pageSize, _Data.pages);// 设置分页器参数
    pager.render(_Data.pageNum);//渲染
  }
  /*
   * 展开收起按钮点击事件
   * 业务部分，渲染详情部分
   * @author wangshuzhong
   * @Date   2017/4/05
   * */
  function hideAndShowClick() {
    var i = $(this).data("i"); // 获取下标
    var that = this; // 明确指向
    detailCon = $(this).parent().parent().next(); // 为当前详情容器赋值
    detailParams.id = _Data.list[i].id;
    if (detailCon.html() == "") {
      // 如果详情容器中不存在内容则为其渲染 否则不做操作
      _detailData.name = $(that).attr("href").slice(1) + "table";
      detailTableRender();
      detailTableFmt(i);
    }
    ;
  }
  /*
   * 详情表格渲染
   * 为当前详情容器加入表格
   * */
  function detailTableRender() {
    var detailHtml = template("id-detail-text", _detailData);
    detailCon.html(detailHtml);
  }
  /*
   * 详情表格初始化
   * 初始化表格
   * */
  function detailTableFmt(index) {
    // 初始化表格
    $("#" + _detailData.name).bootstrapTable({
      url: QHConfig.url.receivablesDetail,
      queryParams: function () {
        // 传参
        return detailParams;
      },
      ajax:tableAjax,
      responseHandler: function (res) {
        // 数据处理
        var needData = QHConfig.BSTTableResHandler(res, resDataFmt);
        needData.index = index;
        return needData;
      },
      onLoadSuccess: detailTableLoadSuc
    });
  }
  /*
   * 详情表格加载成功回调函数
   * */
  function detailTableLoadSuc() {
    var $table = $("#"+_detailData.name); // 详情表格
    var allTr = $table.find("tbody tr");

    // 隐藏默认分页器
    detailCon.find(".fixed-table-pagination").hide();
    for(var i=0;i<allTr.length;i++) {
      allTr.eq(i).find("td").eq(0).addClass("cmn-goods-text");
      allTr.eq(i).find("td").eq(2).addClass("cmn-remarks-text");
    }
  }
  /*
   * 表格详情加工
   * @author wangshuzhong
   * @Date   2017/4/07
   * */
  function resDataFmt($data) {
    //租赁状态
    //if($data.orderType == 2) {
    //  $data.actualPrice = QHConfig.turnMoney($data.actualPrice,2) + "元/天";//租赁单价
      //if($data.status == 1){//租赁状态下判断是否离场
      //  $data.status = "未离场";
      //}else{
      //  $data.status = "已离场";
      //}
    //}else {//采购状态
      $data.actualPrice = QHConfig.turnMoney($data.actualPrice,2) + "元";//采购单价
      //$data.status = "-";//采购状态下不显示租赁状态
      //$data.leaseTerm = "-";//采购状态下不显示租期
    //}
    //$data.orderType = $data.orderType == 1 ? "采购" : "租赁" ;
    $data.receiveTime = QHConfig.timeFmt($data.receiveTime);
    $data.amountDue = QHConfig.turnMoney($data.amountDue,2);//金额
  }
  /*
   * 外层伪表格显示形式转换，发货日期“2017-04-07”，应收总金额“100.00”；
   * @author wangshuzhong
   * @Date   2017/4/07
   * */
  function sendTimeAndTotalPriceTurn() {
    for(var i=0;i< $(".cmn-detail-sendTime").length;i++) {
      var sendTime = parseInt($(".cmn-detail-sendTime").eq(i).text());//发货时间
      var totalPrice = parseFloat($(".cmn-detail-totalAmountReceivable").eq(i).text());//应收总金额

      $(".cmn-detail-sendTime").eq(i).text(QHConfig.timeFmt(sendTime));
      $(".cmn-detail-totalAmountReceivable").eq(i).text(QHConfig.turnMoney(totalPrice,2));
    }
  }
  /*
   * 获取某月的最后一天
   * @param year,年份
   * @param month,月份
   * @author wangshuzhong
   * @Date   2017/4/07
   * */
  function getLastDay(year,month) {
    var new_year = year;    //取当前的年份
    var new_month = month++;//取下一个月的第一天，方便计算（最后一天不固定）

    if(month>12) {
      new_month -=12;        //月份减
      new_year++;            //年份增
    }
    var new_date = new Date(new_year,new_month,1);                //取当年当月中的第一天

    return (new Date(new_date.getTime()-1000*60*60*24)).getDate();//获取当月最后一天日期
  }
  //end
}