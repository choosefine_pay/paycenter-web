/**
 * Created by   :wangshuzhong
 * Created time :2017/5/11
 * File explain :施小包账户-银行卡管理-新增个人与企业银行卡
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    config:["../common/js/common-config"],
    qhSelect:["../common/js/qh-selectors"]
  }
});
require(["jquery"], function () {
  require(["layer","zui","boot","config","qhSelect"], operationCodeFun)
});

function operationCodeFun() {
  //layer框架配置
  layer.config({
    path:"../../libs/layer/"
  });
  const QHConfig = $("body").data("config");//配置文件常量
  var flag = QHConfig.getSearchDataFun().myFlag;//获取上个页面信息;
  UseWatchingSelector(window);//输入选择对象
  //声明输入下拉
  var options={
    watchingInput:$(".breach-bank"),
    textField:"ubankName",
    valueField:"bankUnionCode",
    //页面的容器
    saveValueAt:$("#id-hide-input-company"),
    ajaxUrl:[],
    width:340,
    ajaxParams:[{paramName:"data-code",paramValue:$(".breach-bank")}]
  };
  var ws=window.addWatchInputing(options);//实例化

  /*页面渲染*/
  $(".transfer-notice").hide();//隐藏提示
  renderInfo();//页面渲染
  rederMoreBank();//选择其他银行

  /*事件操作*/
  //返回我的账户
  $(".back-to-myAccount").on("click",function() {
    parent.parent.layer.closeAll();
  });
  //返回我的银行卡管理
  $(".back-to-myBank").on("click",function() {
    parent.layer.closeAll();
  });
  //tab切换个人与企业新增
  $(".add-tab").on("click",tabAddBankCard);
  //开户名
  $(".transfer-people-import").on("blur",checkedUserName);
  //银行卡号校验
  $(".transfer-money-import").on("keyup blur",checkedBankCard);
  //选择其他银行后下一步按钮
  $(".next-step").on("click",clickOtherBankFun);
  //新增个人银行卡
  $(".add-personal-card").on("click",addPersonalCard);
  //增加企业银行卡
  $(".add-company-card").on("click",addCompanyCard);
  //开户名与卡号聚焦失焦，清除显示隐藏
  $(".for-clear").on("focus",function() {
    if($(this).val() != "") {
      $(this).siblings(".clear-img").css("visibility","visible");//显示清除按钮
    }
  }).on("blur",function() {
    setTimeout(function(){
      $(".clear-img").css("visibility","hidden");//隐藏清除按钮
    },300);
  }).on("keyup",function() {
    if($(this).val() == "") {
      $(this).siblings(".clear-img").css("visibility","hidden");//隐藏清除按钮
    }else {
      $(this).siblings(".clear-img").css("visibility","visible");//隐藏清除按钮
    }
  });
  //清除内容
  $(".clear-img").on("click",function(){
    $(this).siblings(".for-clear").val("");//清除内容
    $(this).siblings(".for-clear")[0].focus();
    $(this).css("visibility","hidden");
    return false;
  });
  //查看施小包支付协议
  $(".sxb-sever").on("click",openSXBText);
  //点击选择框获取省列表，与省列表的显示与隐藏
  $("#chooseCidy").one("click",function(){
    getAllProvince();//获取一次省列表
  }).on("click",function(){
    $(".first").toggle();//省列表显示隐藏
  });
  //总列表hover事件，鼠标进入市列表显示，鼠标离开列表，省列表隐藏，市区列表隐藏
  $(".listWrap").hover(function(){
    $(".second").show();
  },function(){
    $(".first").hide();
    $(".second").hide();
  }).on("click", ".second_item", function () {
    var ccode = $(this).attr("city_code");//市编号
    var pName = $(this).attr("parent_name");//省名称
    var oldCode = $("#chooseCidy").attr("code");//老code
    $("#chooseCidy").html(pName + "/" + $(this).html());//页面的值
    $("#chooseCidy").attr("code", ccode);
    $(".first,.second").hide();
    var newCode = $("#chooseCidy").attr("code");//新code

    if(oldCode&&newCode != oldCode) {
      if( $(".breach-bank").val() != "") {
        getInfoByCode();
      }
      $(".breach-bank").val("");
      $("#id-hide-input-company").val("");
    }
  });
  //鼠标进入省列表，根据省code获取市列表
  $(".first").on("mouseenter", ".first_item", function () {
    var provinceCode = $(this).attr("province_code");//获取当前省编号
    var provinceName = $(this).text().replace(/(^\s*)|(\s*$)/g,"");//省名称

    getCityList(provinceCode,provinceName);//生成市列表
  });
  //输入查询支行信息
  $(".breach-bank").on("focus",getInfoByCode);

  /*to do 其他函数*/
  /*
   * 页面渲染
   * */
  function renderInfo() {
    //新增个人还是企业
    if(flag == 0) {
      $(".accord-scale").removeClass("active");
      $(".accord-fixed-money").addClass("active");
      $(".add-company-bank").removeClass("hidden");
      $(".add-person-bank").addClass("hidden");
    }else {
      $(".accord-scale").addClass("active");
      $(".accord-fixed-money").removeClass("active");
      $(".add-company-bank").addClass("hidden");
      $(".add-person-bank").removeClass("hidden");
    };
  }
  //选择更多银行
  function rederMoreBank() {
    $.ajax(tokenAjax({
      type : "get",
      url : QHConfig.url.bankType,
      success : function(data) {
        var _data = data.data;
        renderBankInfo(_data);//渲染银行卡信息
      }
    }));
  }
  //生成银行卡
  function renderBankInfo(_data) {
    var oHtml = "";//代码容器
    //信息有误，不渲染
    if(!_data) {
      return;
    }
    for(var i=0;i<_data.length;i++) {
      oHtml += `<div class="choice-bank" data-src="${_data[i].logoPc}" data-bankCode="${_data[i].bankCode}" data-bankName="${_data[i].bankName}">
            <div class="choice-bank-detail">
              <div class="choice-bank-choose"></div>
              <img class="choice-bank-logo" src="${_data[i].logoPc}" alt="">
            </div>
          </div>`;
    }
    $(".choice-bank-name").append(oHtml);
    //渲染图片；和银行名称和bankcode
    var first = $(".choice-bank").eq(0);//默认的合作银行

    $(".choice-bank").eq(0).addClass("active");
    $(".choice-bank-choose").eq(0).addClass("choice-bank-choose-active").removeClass("choice-bank-choose");
    $("#id-bank").attr({src:first.attr("data-src"),bankCode:first.attr("data-bankCode"),bankName:first.attr("data-bankName")});
    //选择其他银行鼠标点击事件
    $(".choice-bank-name .choice-bank").on("click",otherBankFun);
  }
  //选择其他银行点击事件回调
  function otherBankFun(){
    $(this).addClass("active").find(".choice-bank-choose").removeClass("choice-bank-choose")
      .addClass("choice-bank-choose-active")
      .parents(".choice-bank").siblings(".choice-bank").removeClass("active").find(".choice-bank-choose-active")
      .removeClass("choice-bank-choose-active").addClass("choice-bank-choose");
  }
  //选择其他银行后下一步点击回调
  function clickOtherBankFun(){
    var choosedBank = $(".choice-bank-name>.active");
    var oldBank = $("#id-bank").attr("bankCode");

    $("#id-bank").attr({src:choosedBank.attr("data-src"),bankCode:choosedBank.attr("data-bankCode"),bankName:choosedBank.attr("data-bankName")});
    var newBank = $("#id-bank").attr("bankCode");
    if(newBank != oldBank) {
      if( $(".breach-bank").val() != "") {
        getInfoByCode();
      }
      $(".breach-bank").val("");
      $("#id-hide-input-company").val("");
    }
  }
  /*
   * tab切换个人与企业新增银行卡
   * */
  function tabAddBankCard() {
    $(this).addClass("active").siblings().removeClass("active");
    if($(".accord-scale").hasClass("active")) {
      $(".add-person-bank").removeClass("hidden");
      $(".add-company-bank").addClass("hidden");
    }else {
      $(".add-company-bank").removeClass("hidden");
      $(".add-person-bank").addClass("hidden");
    }
  }
  /*
   * 银行卡号校验
   * */
  function checkedBankCard() {
    this.value=this.value.replace(/[^\d]/g,'');
  }
  /*
   * 开户名
   * */
  function checkedUserName() {
    var userNameReg = new RegExp("[`%~!@#$^&*=|{}':;',\\[\\].<>/?~！@#￥……&*——|{}【】‘；：”“'。，、？]","g");

    this.value=this.value.replace(userNameReg,'');
  }
  /*
   * 个人银行卡新增
   * */
  function addPersonalCard() {
    var name = $(".person-userName").val();//开户名
    var cardNum = $(".person-bankCardNum").val();//银行卡号
    var personalData = null;//请求所需的参数

    $(".transfer-notice").hide();//隐藏所有提示
    if(name == "") {
      $(".person-userName").parent().siblings(".transfer-notice").show();//提示显示
      $(".person-userName")[0].focus();//聚焦为空的
      return;
    };
    if(cardNum == "") {
      $(".person-bankCardNum").parent().siblings(".transfer-notice").show();//提示显示
      $(".person-bankCardNum")[0].focus();//聚焦为空的
      return;
    }
    //新增个人请求
    personalData = {
      bankAccountName:name,
      bankcardNo:cardNum
    };
    postAddPersonal(personalData);

  }
  /*
   * 企业银行卡新增
   * */
  function addCompanyCard() {
    var name = $(".company-userName").val();//开户名
    var cardNum = $(".company-bankCardNum").val();//银行卡号
    var areaCode = $("#chooseCidy").attr("code");//开户行市区code
    var breachBank = $(".breach-bank").val();//支行名称
    var thisBank = $("#id-bank").attr("bankCode");//开户行
    var breachBankCode = $("#id-hide-input-company").val();//支行code
    var companyData = null;//请求所需的参数
    var bankName = $("#id-bank").attr("bankName");//银行名
    var timer = null;//延时器

    $(".transfer-notice").hide();//隐藏所有提示
    //判断开户名
    if(name == "") {
      $(".company-userName").parent().siblings(".transfer-notice").show();//提示显示
      $(".company-userName")[0].focus();//聚焦为空的
      return;
    };
    //判断开户行
    if(thisBank == "") {
      $("#id-bank").siblings(".transfer-notice").show();//提示显示
      return;
    };
    //判断卡号
    if(cardNum == "") {
      $(".company-bankCardNum").parent().siblings(".transfer-notice").show();//提示显示
      $(".company-bankCardNum")[0].focus();//聚焦为空的
      return;
    }
    //开户行所在地
    if(!areaCode) {
      $("#chooseCidy").parent().parent().siblings(".transfer-notice").show();//提示显示
      return;
    }
    //支行
    if(breachBankCode == "") {
      $(".breach-bank").siblings(".transfer-notice").show();//提示显示
      $(".breach-bank")[0].focus();//聚焦为空的
      return;
    }
    //新增企业请求
    companyData = {
      bankAccountName:name,//银行账户名称
      bankcardNo:cardNum,//银行卡号
      bankCardLength:cardNum.length,//银行卡长度
      bankUnionCode:breachBankCode,//支行联行号
      bankUnionName:breachBank,//支行名称
      bankName:bankName,//银行名称
      bankCode:thisBank//行号
    };
    var thisFlag = $(".add-company-card").attr("data-flag");//防连续点击
    if(thisFlag == 0) {
      layer.msg("正在处理，请勿重复点击！");
      return;
    }
    $(".add-company-card").attr("data-flag","0");
    clearTimeout(timer);
    timer = setTimeout(function(){
      $(".add-company-card").attr("data-flag","1");
    },5000);
    postAddCompany(companyData);
  }
  /*
   * 个人新增请求
   * */
  function postAddPersonal(ajaxData) {
    $.ajax(tokenAjax({
      type:"post",
      url:QHConfig.url.addBankCard,
      data:JSON.stringify(ajaxData),
      contentType:"application/json",
      success:function(data) {
        if(data.status == 200) {
          layer.msg("新增个人银行卡成功");
          setTimeout(function() {
            parent.layer.closeAll();
          },2000)
        }else {
          layer.msg(data.message);
        }
      }
    }))
  }
  /*
   *企业新增请求
   * */
  function postAddCompany(ajaxData) {
    $.ajax(tokenAjax({
      type:"post",
      url:QHConfig.url.companyBankCard,
      data:JSON.stringify(ajaxData),
      contentType:"application/json",
      success:function(data) {
        if(data.status == 200) {
          layer.msg("新增企业银行卡成功");
          //跳转到企业认证页面
          setTimeout(function() {
           parent.layer.closeAll();
          },1000)
        }else {
          layer.msg(data.message);
        }
      }
    }))
  }
  /*
   * 一次性获取省列表
   * @author wsz
   * @Date   2017/05/12
   */
  function getAllProvince(){
    $.ajax(tokenAjax({
      type:"get",
      url:QHConfig.url.bankProvince,
      contentType:"application/json",
      success:function(data) {
        var provinceData = data.data;//省列表数组
        var provinceList="";//用于存储省列表字符串
        for(var i=0;i<provinceData.length;i++){
          provinceList += `<div class="first_item" province_code="${provinceData[i].provinceCode}">
            ${provinceData[i].provinceName}
          </div>`;
        }
        $(".first").append(provinceList);
      }
    }))
  }
  /*
   * 根据省编号获取该省下边的市区
   * @param  code   省编号
   * @param name 省名称
   * @author wsz
   * @Date   2017/05/12
   */
  function getCityList(code,name){
    $.ajax(tokenAjax({
      type:"get",
      url:QHConfig.url.bankCity + code,
      contentType:"application/json",
      success:function(data) {
        var cityData = data.data;//市列表数组
        var cityList="";//用于存储市列表字符串

        for(var i=0;i<cityData.length;i++){
          cityList += `<div class="second_item" city_code="${cityData[i].areaCode}" parent_name="${name}">${cityData[i].cityName}</div>`;
        }
        $(".second").html(cityList);
      }
    }))
  }
  /*
   * 查看施小包支付协议
   * @author wsz
   * @Date   2017/05/12
   */
  function openSXBText() {
    layer.open({
      title: ["施小包服务协议","background:#009CFF;color:#fff;font-size:16px"],
      shade: 0.5,
      move:false,
      closeBtn:1,
      resize:false,
      scrollbar:false,
      type: 2,
      area: ['515px', '600px'],
      content: "../common/sxb-service-agreement.html",
      success: function(layero,index) {
        var closeBtn = $(".layui-layer-setwin a");//layer的title关闭按钮
        var sure = layer.getChildFrame(".service-agreement-btn",index);//确定按钮

        //初始化
        closeBtn.css({"background":"url(../../common/img/private/white-error.png) no-repeat"});
        //同意协议
        sure.on("click",function() {
          layer.close(index);
        })
      }
    })
  }
  /*
   * 聚焦查询支行，先获取数据
   * */
  function getInfoByCode() {
    var areaCode = $("#chooseCidy").attr("code");//市区code
    var bankCode = $("#id-bank").attr("bankCode");//银行类型code

    if(areaCode&&bankCode) {
      getBreachInfo(bankCode,areaCode);//获取支行信息
    }
  }
  /*
   * 根据市code和银行类型，查询支行信息
   * */
  function getBreachInfo(bankCode,areaCode) {
    $.ajax(tokenAjax({
      type:"get",
      async:false,
      url:QHConfig.url.getBreachBank + bankCode + "/" + areaCode,
      contentType:"application/json",
      success:function(data) {
        //将获取的列表放进下拉框
        ws.setData(data.data);
        ws.render(data.data);
      }
    }))
  }
}