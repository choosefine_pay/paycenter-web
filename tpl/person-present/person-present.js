/**
/**
 * Created by   :宁建浩
 * Created time :2017/5/12/9:38
 * File explain :提现
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    config: ["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["layer", "zui", "boot", "config"], operationCodeFun)
});
function operationCodeFun() {
  "use strict";
  var QHConfig = $("body").data("config");
  var manageInfo = QHConfig.getSearchDataFun();//银行数据
  var inputInter = null;
  //判断是否是大额提现
  if (Number(manageInfo.amount) <= 50000) {
    $(".small-time").hide();
  }
  //layer框架配置
  layer.config({
    path: "../../libs/layer/"
  });
  //忘记密码
  $('.payCash-confirm-forget').on("click",forgetPwd);
  //返回修改事件
  $(".payCash-confirm-return").on("click", backParent);
  //小额提交事件
  $("#id-small-btn").on("click", smallSub);
  //判断是否是对公
  $("#" + manageInfo.el).removeClass("hide");
  //大额提现
  $("#id-big-btn").on("click", bigSub);
  //银行下拉点击事件
  $("#chooseCity").on("click", bankSelectClick);
  //输入支行名称
  $(".breach-bank").on("keyup", function (e) {
    var target = e.target;
    keyupInputFun(target)
  });
  //点击选择支行
  $(".bank-watch-list").on("click", ".third_item", function (e) {
    var target = e.target;
    getBreachBank(target);
  });
  //省份移出隐藏，移入城市显示，
  $(".listWrap").on("mouseleave", bankSelectBlur).on("mouseenter", ".first", preSelectFun)
    .on("mouseenter", ".first_item", enterProFun).on("click", ".second", function (e) {
    var target = e.target;
    clickSecondFun(target);
  });
  //知道了返回
  //渲染数据
  $(".payCash-confirm-money").html(manageInfo.amount);
  $(".id-bank-img").css({background: `url(' ${manageInfo.bankImg} ') no-repeat`,backgroundSize:"contain"});
  $(".id-bank-name").html(manageInfo.bankName);
  $(".id-bank-card").html(manageInfo.bankCard);
  $(".id-bank-union").html(manageInfo.bankUnionName);
  $(".wait-money-time").html(manageInfo.bankTime);
  $(".know-btn").on("click", backMyCount);
  if (manageInfo.payType == "1") {
    $(".id-bank-type").html("个人");
  } else if (manageInfo.payType == "0") {
    $(".id-bank-type").html("企业");
  }
  //返回上一层
  function backParent() {
    parent.layer.closeAll();
  }

  //小额提交
  function smallSub() {
    var pwd = $("#id-small-pwd").val();//密码
    var msg = {
      payPass: pwd
    };
    if(!pwd){
      $(".payCash-confirm-pwdFio").show().find(".money-pwd-times").html("支付密码不能为空");
    }else{
      payPwd(msg);//调用验证密码
    }

  }

  //大额提现
  function bigSub() {
    var pwd = $("#id-big-pwd").val();//密码
    var breachBank = $(".breach-bank").val();//支行名称
    var bigFlag = true;
    var msg = {
      payPass: pwd,
      bankUnionCode: $("#id-hide-input-company").val(),//支行号码
      bankUnionName: $(".breach-bank").val(),//支行名称
      accountBankcardId: manageInfo.bankCardId//银行卡id
    };
    if (!breachBank) {
      layer.msg("请选择支行");
      return;
    } else if (!pwd) {
      $(".payCash-confirm-pwdFio").show().find(".money-pwd-times").show().html("请输入密码");
      return;
    }
    payPwd(msg, bigFlag);
  }

  //获得银行所在地
  var cityEvent = {
    provinceData: null,//银行所在省
    cityData: {},//银行所在市
    breachBank: null,//支行
    //获取省份
    getProData: function () {
      $.ajax(tokenAjax({
        url: QHConfig.url.bankProvince,
        type: "GET",
        async: false,
        success: function (res) {
          var html = "";
          cityEvent.provinceData = res.data;
          $.each(res.data, function (k, v) {
            html += `<div class="first_item" data-code="${v.provinceCode}" data-name="${v.provinceName}">${v.provinceName}</div>`;
          });
          $(".first").html(html);
        }
      }));
    },
    //通过id获取城市
    getCityData: function (code) {
      if (!cityEvent.cityData[code]) {
        $.ajax(tokenAjax({
          url: QHConfig.url.bankCity + code,
          type: "GET",
          async: false,
          success: function (res) {
            cityEvent.cityData[code] = res.data;
          }
        }));
      }
    },
    //获取支行
    getBankBreach: function (cityCode) {
      $.ajax(tokenAjax({
        type: "GET",
        async: false,
        url: QHConfig.url.getBreachBank + manageInfo.bankCode + "/" + cityCode,
        success: function (res) {
          cityEvent.breachBank = res.data;
        }
      }));
    }
  };
  cityEvent.getProData();//默认加载省份
  //验证支付密码
  function payPwd(msg, bigFlag) {
    var bigMoney = false;
    var _data = {
      accountBankcardId: msg.accountBankcardId,//账号银行卡id
      bankUnionCode: msg.bankUnionCode,//支行code
      bankUnionName: msg.bankUnionName//支行名称
    };
    if (bigFlag) {
      //设置支行
      $.ajax(tokenAjax({
        type: "PUT",
        async: false,
        data: JSON.stringify(_data),
        url: QHConfig.url.setUBankCodeAndUbankName,
        contentType: "application/json;charset=UTF-8",
      }).done(function (res) {
        bigMoney = true;
      }));
    }
    var data = {
      operName: manageInfo.operName,//打款人
      bankCardId: manageInfo.bankCardId,//银行卡id
      accountId: manageInfo.accountId,//账户id
      amount: manageInfo.amount,//账户金额
      password: msg.payPass//密码
    };
    //判断知否大额
    if (bigFlag) {
      data.accountBankcardId = manageInfo.bankCardId;
      data.bankUnionCode = _data.bankUnionCode;
      data.bankUnionName = _data.bankUnionName;
    }
    // console.log(data);
    //提现
    $.ajax(tokenAjax({
      type: "POST",
      data: JSON.stringify(data),
      contentType: "application/json;charset=UTF-8",
      url: QHConfig.url.withdraw,
      success: function (res) {
        if (res.status == 400) {
          $(".money-pwd-times").html(res.message).parent().show();
          if(res.message.indexOf("分钟")!=-1){
            $(".confirm-prompt-contanier").hide();
            $(".tautology").show();
            timeCountDown(res.message)
          }
        } else if (res.status == 200) {
          $("#" + manageInfo.el).addClass("hide");
          $(".payCash-main-top").hide();
          $(".money-pwd-times").parent().hide();
          $(".withdraw-result-contanier").removeClass("hide");
          var url = `../payCash-confirm/payCash-confirm-result.html?time=${manageInfo.bankTime}`;
          jumpPage(url);
        }
      },
      error: function () {
        layer.msg("网络错误");
      }
    }));
  }

  //返回我的账户
  function backMyCount() {
    parent.parent.parent.layer.closeAll();
  }

  //银行下拉点击事件回调
  function bankSelectClick() {
    $(".listWrap,.first").show();
  }

  //移出隐藏
  function bankSelectBlur() {
    $(".first,.second").hide();
  }

  //市区显示
  function preSelectFun() {
    $(".second").show();

  }

  //城市显示
  function enterProFun() {
    var code = $(this).data("code");
    cityEvent.getCityData(code);
    var html = "";
    $.each(cityEvent.cityData, function (k, v) {
      if (code == k) {
        $.each(v, function (m, n) {
          html += `<div class="second_item" data-code="${n.areaCode}" data-name="${n.cityName}">${n.cityName}</div>`
        });
      }
    });
    $(".second").html(html);
  }

  //城市点击
  function clickSecondFun(event) {
    var cityCode = $(event).data("code");
    var cityName = $(event).data("name");
    $(".listWrap").hide();
    $("#chooseCity").html(cityName);
    cityEvent.getBankBreach(cityCode);

  }

  //输入支行名称回调
  function keyupInputFun(e) {
    var txt = '';
    var html = "";
    var bkn = {};
    $(".bank-watch-list").show();
    clearTimeout(inputInter);
    inputInter = window.setTimeout(function () {
      txt = $(e).val();
      if (!cityEvent.breachBank) {
        return;
      } else {
        $.each(cityEvent.breachBank, function (k, v) {
          bkn[v.ubankName] = v.bankUnionCode;
        });
        $.each(bkn, function (k, v) {
          if (k.indexOf(txt) != -1) {
            html += `<div class="third_item" data-code="${v}" data-name="${k}">${k}</div>`;
          }
        });
        $(".bank-watch-list").html(html);
      }
    }, 500);
  }

  //设置分行名称
  function getBreachBank(e) {
    var bankName = e.innerHTML;
    var bankCode = $(e).data("code");
    $(".breach-bank").val(bankName);
    $("#id-hide-input-company").val(bankCode);
    $(".bank-watch-list").hide();
  }

  //结果跳转
  function jumpPage(url) {
    layer.open({
      type: 2,
      title: false,
      closeBtn: 0,
      area: ["100%", "100%"],
      scrollbar: false,
      content: url
    });
  }
  //忘记密码回调
  function forgetPwd(){
    layer.open({
      title: false,
      shade: 0.8,
      move: false,
      closeBtn: 0,
      resize: false,
      scrollbar: false,
      type: 2,
      area: ['100%', '100%'],
      content: "../forget-payment-password/forget-payment-password.html?pay=true"
    });
  }
  function timeCountDown(msg){
    var minuteNum= msg.match(/\d/ig);
    var countDown=null;
    var m=0;
    var s=0;
    if(minuteNum[0]==1){
      m=9;
      s=59;
      countDown=setInterval(function(){
        if(s<10){
          $('.tautology-time').html(m+'分0'+s+"秒");
        }else{
          $('.tautology-time').html(m+'分'+s+"秒");
        }
        s--;
        if(s<0){
          s=59;
          m--;
        }
        if(m==0&&s==0){
          $(".sensitize-orange-btn").css("background","#faa500");
          clearInterval(countDown);
        }
      },1000);
    }else{
      m=minuteNum[1];
      s=minuteNum[2]+minuteNum[3];
      countDown=setInterval(function(){
        if(s<10){
          $('.tautology-time').html(m+'分0'+s+"秒");
        }else{
          $('.tautology-time').html(m+'分'+s+"秒");
        }
        s--;
        if(s<0){
          s=59;
          m--;
        }
        if(m==0&&s==0){
          clearInterval(countDown);
        }
      },1000);
    }

  }
  //激活按钮
  function clickBtn(){
    if($(".tautology-time").html()=="0分01秒"){
      $(".input-password-contanier").show();
      $(".tautology").hide();
    }
  }
}
