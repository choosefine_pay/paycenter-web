define(['vue'],function(Vue){
	function getMenuTemplate(){
		return '<div>'+
            '<div class="index-aside-nav-logo">'+
                '<div class="index-nav-logo">'+
                    '<img class="index-company-logo">'+
                '</div>'+
                '<div class="index-nav-logo-title"></div>'+
            '</div>'+
            '<div class="index-aside-nav-menu">'+
                '<ul class="index-first-nav">'+
				    '<template v-for="fst in username">'+
					'<li :class="fst.children?\'index-delivery\':\'\'" onclick="subToggle()">'+
					
						'<div>'+
							'<span class="index-first-nav-icon"></span>'+
							'<a v-bind:href="fst.url" v-bind:target="fst.target"><span class="index-first-nav-title" :class="fst.children?\'index-first-nav-down\':\'\'">{{fst.name}}</span></a>'+
						'</div>'+
						'<ul class="index-second-nav">'+
							'<template v-for="sec in fst.children">'+
							'<li><a v-bind:href="sec.url" v-bind:target="sec.target">{{sec.name}}</a></li>'+
							'</template>'+
						'</ul>'+
						
					'</li>'+
					'</template>'+
				'</ul>'+
            '</div>'+
        '</div>'
	}
	function getJson(){
		return [
			{
			"name": "支付",
			"url": "javascript:void(0);",
			"target":"",
			"beforeImg": "s_Deliver_goods.png",
			"afterImg": "s_Deliver_goods_click.png",
			"children": [
			  {
				"name": "支付页面",
				"url": "../../tpl/mdu-buyer-pay/mdu-buyer-pay.html",
				"target":"_blank",
				"children": null
			  }
			]
		  },
		   {
			"name": "我的账户",
			"url": "../../tpl/mdu-sxb-account/mdu-sxb-account.html",
			"target":"_self",
			"beforeImg": "shixiaobao1.png",
			"afterImg": "shixiaobao1_click.png",
			"children": null
		  },
		  /*
		  {
			"name": "交易账务",
			"url": "../../tpl/mdu-trade-account/mdu-trade-account.html",
			"beforeImg": "s_transaction.png",
			"afterImg": "s_transaction_click.png",
			"children": null
		  },
		  */
		  {
			"name": "账单",
			"url": "../../tpl/mdu-bill/mdu-bill.html",
			"target":"_self",
			"beforeImg": "s_transaction.png",
			"afterImg": "s_transaction_click.png",
			"children": null
		  },
		  {
			"name": "设置支付密码",
			"url": "../../tpl/set-payment-password/set-payment-password.html",
			"target":"_blank",
			"beforeImg": "s_transaction.png",
			"afterImg": "s_transaction_click.png",
			"children": null
		  },
		  {
			"name": "修改支付密码",
			"url": "../../tpl/mdu-modify-paypass/mdu-modify-paypass.html",
			"target":"_blank",
			"beforeImg": "s_transaction.png",
			"afterImg": "s_transaction_click.png",
			"children": null
		  },
		  {
			"name": "忘记支付密码",
			"url": "../../tpl/mdu-forget-paypass/mdu-forget-paypass.html",
			"target":"_blank",
			"beforeImg": "s_transaction.png",
			"afterImg": "s_transaction_click.png",
			"children": null
		  }
		]
	}
	var menu=Vue.extend({
		props: ['username'],
		template: getMenuTemplate()
	});
	Vue.component('my-menu',menu);
	//初始化公共头部
	window.initMenu=function(_vue){
		var appM = new _vue({
		  el: '#appMenu',
		  data: {
			message: getJson()
		  }
		})
	}
	window.subToggle=function(){
	}
});