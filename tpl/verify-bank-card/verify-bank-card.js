/**
 * Created by   :wngshuzhong
 * Created time :2017/5/12
 * File explain :验证企业银行卡
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    config: ["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["layer", "zui", "boot", "config"], operationCodeFun)
});
//代码容器

function operationCodeFun() {
  var userCode = window.sessionStorage.getItem("userCode");
  const QHConfig = $("body").data("config");//配置文件常量
  var search = QHConfig.getSearchDataFun();//获取上个页面信息;
  //layer框架配置
  layer.config({
    path: "../../libs/layer/"
  });
  //页面渲染，渲染可供选择的更多银行
  renderInfo();//页面渲染

  //事件操作
  //返回我的账户;不同页面进来；页面层级不一样；0代表从银行卡列表进来；1调兵新增的时候进来的
  $(".back-to-myAccount").on("click",function() {
    if(search.flag == 0) {
      parent.parent.layer.closeAll();
    }else {
      parent.parent.parent.layer.closeAll();
    }
  });
  //返回我的银行卡管理
  $(".back-to-myBank").on("click",function() {
    if(search.flag == 0) {
      parent.layer.closeAll();
    }else {
      parent.parent.layer.closeAll();
    }
  });
  $(".transfer-people-import").on("keyup blur", moneyKeyUpFun);//输入验证，1分到5毛
  //返回按钮
  $(".return-btn").on("click", function () {
    parent.layer.closeAll();
  });
  //确定按钮
  $(".confirm-btn").on("click", sureChecked);
  //尚未收到打款no-get-money
  $(".no-get-money").on("click",noGetMoney);

  /*to do 其他函数*/
  /*
   * 页面渲染
   * */
  function renderInfo() {
    var nowDate = QHConfig.timeFmt(new Date().getTime(), 5);
    var bankCardNum = "";

    for(var i=0;i<search.bankCardLength-4;i++) {
      bankCardNum += "*";
    }
    $(".now-data").text(nowDate);//时间
    $(".bank-account-name").text(search.bankAccountName);//开户名
    $(".bank-name").text(search.bankName);//银行
    $(".bank-num").text(bankCardNum + search.bankcardNo.substr(search.bankcardNo.length-4));//银行卡号
  }

  //金额输入事件回调
  function moneyKeyUpFun() {
    var val = $(this).val();
    var reg = new RegExp("^[0]+\.?[0-9]{0,2}$");
    if (!reg.test(val)) {
      $(this).val("");
    }
  }
  //未收到打款
  function noGetMoney() {
    layer.confirm("请拨打0571-81608755联系客服寻求帮助",{
      title:"温馨提醒"
    },function(){
      layer.closeAll();
    });
  }

  /*
   * 验证打款金额
   * */
  function sureChecked() {
    var accountBankcardId = search.accountBankCardId;//银行卡绑定id
    var validAmount = $(".transfer-people-import").val();//账户银行卡收到的随机金额
    var ajaxData = null;//传值储存对象

    $(".transfer-notice").hide();
    //金额为空
    if (validAmount == "") {
      $(".transfer-people-import").parent().siblings(".transfer-notice").show();//提示显示
      return;
    }
    ajaxData = {
      accountBankcardId: accountBankcardId,
      validAmount: validAmount
    };
    //接口请求验证企业银行卡
    $.ajax(tokenAjax({
      type: "put",
      url: QHConfig.url.verifyBankCard,
      data: JSON.stringify(ajaxData),
      contentType: "application/json",
      success: function (data) {
        if (data.status == 200) {
          parent.parent.layer.closeAll();//回到银行卡管理页面
        } else {
          layer.msg(data.message);
        }
      }
    }))
  }
}