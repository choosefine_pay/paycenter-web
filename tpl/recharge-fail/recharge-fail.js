/**
 * Created by   :王书中
 * Created time :2017/6/13
 * File explain :施小包账户-充值失败
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    config:["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["layer","zui","boot","config"], operationCodeFun)
});
//代码容器

function operationCodeFun() {
  //事件操作
  //返回我的账户
  $(".back-to-account").on("click",function() {
    parent.parent.layer.closeAll();
  });
}