/*
 *JavaScript Document
 Update 2017-6-23 input number
 By Ternwii
 *qhPagination for bootstrap table
 */
(function(root){
    'use strict';
    //开始构建分页栏
    function qhPagination(_name){
        if(_name)this.qpName=_name;
        else this.qpName='';
        this.tagHead=this.qpName+'_p_';
        this.pageSizes=[];
        this.pageSize=10;
    }
    /*
     *切换页时内外部执行
     */
    qhPagination.prototype.changePage=function(p){
        this.setPage(p);
        if(typeof(this.$outHandler)=="function")this.$outHandler(p,this.getPageSize);
        this.render(p);
    }
    /*
     *生成html
     */
    qhPagination.prototype.sizeListHtm=function(){
        var htm="每页显示&nbsp;<select class='pageor-select' data-name='"+this.tagHead+"size' id='"+this.tagHead+"select'>";
		if(this.pageSizes.length>0){
			for(var s=0;s<this.pageSizes.length;s++){
				if(this.pageSizes[s]==this.pageSize){
					htm+="<option selected value='"+this.pageSize+"'>"+this.pageSize+"</option>";
				}
				else{
					htm+="<option value='"+this.pageSizes[s]+"'>"+this.pageSizes[s]+"</option>";
				}
			}
			htm+="</select>&nbsp;条&nbsp;&nbsp;";
		}
        else htm='';
        return htm;
    };
    qhPagination.prototype.render=function(page){
        this.pageCurrent=parseInt(page);
        if(this.pageMax==0){
            if(this.$pageBox.length>0)this.$pageBox.html('');
            return;
        }
        //rendering.......
        var pageHtml=this.sizeListHtm();
        if (this.pageCurrent >this.showTags)
            pageHtml+= "<a class='"+this.page_item+"' id='"+this.tagHead+"start' href='javascript:void(0);'><i class='icon icon-step-backward'></i></a>";
        if (this.pageCurrent > 1)
            pageHtml += "<a class='"+this.page_item+"' id='"+this.tagHead+"prev' href='javascript:void(0);'><i class='icon icon-caret-left'></i></a>";
        for (var p = 1; p <= this.pageMax; p++) {
            if (p == this.pageCurrent){
                pageHtml += "<a class='"+this.page_item_on+"' id='"+this.tagHead+""+p+"' href='javascript:void(0);'>" + p + "</a>";
            }
            else if (p < this.pageCurrent - this.showTags) {
                if (pageHtml.indexOf("l_more") == -1)pageHtml += "l_more";
            } else if (p > this.pageCurrent + this.showTags) {
                if (pageHtml.indexOf("r_more") == -1)pageHtml += "r_more";
            } else
                pageHtml += "<a class='"+this.page_item+"' id='"+this.tagHead+""+p+"' href='javascript:void(0);'>" + p + "</a>";
        }
        if (this.pageCurrent < this.pageMax)
            pageHtml += "<a class='"+this.page_item+"' id='"+this.tagHead+"next' href='javascript:void(0);'><i class='icon icon-caret-right'></i></a>";
        if (this.pageCurrent < this.pageMax - this.showTags)
            pageHtml += "<a class='"+this.page_item+"' id='"+this.tagHead+"end' href='javascript:void(0);'><i class='icon icon-step-forward'></i></a>";
        pageHtml+="共"+this.pageMax+"页";
        pageHtml = pageHtml.replace("l_more","<a href='javascript:void(0);' class='"+this.page_item+"'>..</a>");
        pageHtml = pageHtml.replace("r_more","<a href='javascript:void(0);' class='"+this.page_item+"'>..</a>");
        if(this.pageSearch==1){
            pageHtml+=",到第&nbsp;<input class='pageor-input' id='"+this.tagHead+""+this.pageInput+"' value='"+this.pageCurrent+"' class='"+this.pageInput+
                "'/>&nbsp;页&nbsp;<button class='pageor-button' id='"+this.tagHead+"gonow' class='' href='javascript:void(0);'>确定</button>";
        }
        this.$pageBox.html(pageHtml);
        var that=this;
        $("."+this.page_item+",."+this.page_item_on).on('click',function(){
            var p=$(this).attr('id').replace(that.tagHead,'');
            if(p=='prev')p=that.pageCurrent-1;
            if(p=='next')p=that.pageCurrent+1;
            if(p=='start')p=1;
            if(p=='end')p=that.pageMax;
            if(p==0)p==1;
            else if(p==that.pageMax+1)p=that.pageMax;
            that.changePage(p);
        });
        $("#"+this.tagHead+"gonow").on('click',function(){
            var p=$("#"+that.tagHead+that.pageInput).val();
            if(p<1)p=1
            else if(p>that.getPages()){
                p=that.getPages();
            }
            that.changePage(p);
        });
        $("#"+this.tagHead+"select").on('change',function(){
            var p=$("#"+that.tagHead+that.pageInput).val();
            that.pageSize=$("#"+that.tagHead+'select').val();
            if(p<1)p=1
            else if(p>that.getPages()){
                p=that.getPages();
            }
            that.changePage(p);

        });
		$(".pageor-input").on('keyup',function(){
			if($(this).val().length==1){$(this).val($(this).val().replace(/[^1-9]/g,''))}else{$(this).val($(this).val().replace(/\D/g,''))}
			if($(this).val().length>3)$(this).val($(this).val().substr(0,3));
		});
		$(".pageor-input").on('afterpaste',function(){
			if($(this).val().length==1){$(this).val($(this).val().replace(/[^1-9]/g,''))}else{$(this).val($(this).val().replace(/\D/g,''))}
			if($(this).val().length>3)$(this).val($(this).val().substr(0,3));
		});
    };
    qhPagination.prototype.getInput=function(){
        return $("#"+this.pageInput);
    };
    /*初始化分页栏，参数说明如下
     *$_renderContainer,供分页栏放置的容器
     *$_outHandler,点击分页标签时外部需执行的操作
     *$_styles,为分页栏设置各项样式
     */
    qhPagination.prototype.init=function($_renderContainer,$_outHandler,$_styles){
        if($_styles){
            if($_styles.tag_Class)this.page_item=$_styles.tag_Class;
            if($_styles.tag_on_Class)this.page_item_on=$_styles.tag_on_Class;
            if($_styles.input_Class)this.pageInput=$_styles.input_Class;
            if($_styles.showTags)this.showTags=$_styles.showTags;
            if($_styles.pageSearch)this.pageSearch=$_styles.pageSearch;
            if($_styles.pageSizes)this.pageSizes=$_styles.pageSizes;
            if($_styles.pageSizeDefault)this.pageSize=$_styles.pageSizeDefault;
        }
        this.$pageBox=$_renderContainer;
        this.$outHandler=$_outHandler;
    };
    /*
     *一次设置这些参数
     */
    qhPagination.prototype.setParameters=function(pageNum,pageSize,pages){
        this.pageCurrent=pageNum;
        this.pageMax=pages;
        if(this.pageCurrent>this.pageMax)
            this.pageCurrent=this.pageMax;
    };
    /*
     *可单个设置这些参数
     */
    qhPagination.prototype.setPages=function(pages){
        this.pageMax=pages;
    };
    qhPagination.prototype.setPageSize=function(size){
        this.pageSize=size;
    };
    qhPagination.prototype.setPage=function(num){
        this.pageCurrent=num;
    };
    /*
     *可单个查看这些参数
     */
    qhPagination.prototype.getPages=function(){
        return this.pageMax;
    };
    qhPagination.prototype.getPageSize=function(){
        return this.pageSize;
    };
    qhPagination.prototype.getPage=function(){
        return this.pageCurrent;
    };
    root.QHPagination=new qhPagination();
    /*
     *从外部创建一个分页器入口
     */
    root.qhPaginationFactroy=function(_name){
        return new qhPagination(_name);
    };
})(window)