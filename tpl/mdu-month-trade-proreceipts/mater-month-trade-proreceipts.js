/**
 * Created by   :Wangshuzhong
 * Created time :2017-04-10
 * File explain :交易账务（项目）--月交易明细--实收款
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    bootTable: ["bootstrap-table-dist/dist/js/bootstrap-table"],
    bootTableCN: ["bootstrap-table-dist/dist/locale/bootstrap-table-zh-CN"],
    dateTime: ["datetimepicker/js/datetimepicker"],
    comConfig: ["../common/js/common-config"],
    QHPagination: ["../common/js/qhpager"]
  }
});
require(["jquery"], function () {
  require(["QHPagination", "bootTable"], function () {
    require(["zui", "layer", "dateTime", "bootTableCN", "comConfig"], operationCodeFun)
  })
});
function operationCodeFun() {
  // 由于使用require框架整合，我们需要为layer.js配置读取css文件的路径
  layer.config({
    path: "../../libs/layer/"
  })

  //变量声明
  var QHConfig = $("body").data("config");
  var parentInfo = parent.$("body").data("search");//获取上个页面传过来信息
  var _pjCode = parentInfo.projectCode;//项目编号
  var _pjName = parentInfo.projectName;//项目名称
  var _cmpCode = parentInfo.buyerCompanyCode;//公司code
  var pager = window.qhPaginationFactroy("projectReceipts");//实收款分页器
  //分页器配置
  var options={
    tag_Class:'cmn-page_tag',//分页标签默认样式
    tag_on_Class:'cmn-page_tag_on',//分页标签选中样式
    input_Class:'cmn-pageInput',//分页搜索框样式
    showTags:5,//标签省略距离
    pageSizes:[],
    pageSearch:true//是否启用页面搜索跳转
  };


  //end

  //页面初始化
  $(".cmn-bread-project").text(_pjName);//面包屑项目名称
  renderReceiptsTable();//渲染表格数据
  //end

  //事件绑定
  //点击面包屑返回交易账户页面
  $(".cmn-backTo-tradeAccount").on("click",function() {
    parent.parent.layer.closeAll();
  });
  //返回交易账务（项目）页面
  $(".cmn-active-breadcrumb").on("click",function() {
    parent.layer.closeAll();
  });
  //点击前往应收款页面
  $("#id-buy-btn-rent").on("click",function() {
    //应收款页面地址
    var strUrl = "../mdu-month-trade-proreceivable/mdu-month-trade-proreceivable.html";
    var turnEle = $(this).find("a");

    turnEle.attr("href",strUrl);
    turnEle[0].click();
  });
  // 清空条件按钮点击事件绑定
  $("#id-acc-clear-btn").on("click", function () {
    $(".cmn-query-container").find("[data-name]").val(""); // 清空查询条件
  });
  // 查询按钮点击事件绑定
  $("#id-acc-query-btn").on("click", function () {
    $("#id-user-project-table").bootstrapTable("refresh");// 刷新表格
  });
  //未输入框去除特殊字符
  $("[data-name='payNo'],[data-name='payer']").on("blur",QHConfig.specialCharTest);
  //end

  //to do 其他功能函数
  /*
   * 根据年月获取表格数据并渲染
   * @author wangshuzhong
   * @Date   2017/4/10
   * */
  function renderReceiptsTable() {
    $("#id-user-project-table").bootstrapTable({
      url:QHConfig.url.projectPaid,
      queryParams:getTableParams,
      ajax:tableAjax,
      responseHandler:function(res) {
        pager.setParameters(res.data.pageNum,res.data.pageSize,res.data.pages);
        pager.render(res.data.pageNum);
        return QHConfig.BSTTableResHandler(res,tradeReceiptsTableResData);
      },
      onLoadSuccess:function() {
        var allTr = $("#id-user-project-table tbody tr");

        $(".fixed-table-pagination").hide();//隐藏table自带分页样式
        for(var i=0;i<allTr.length;i++) {
          allTr.eq(i).find("td").eq(1).addClass("cmn-project-text");
        }

      }
    });
    pager.init($(".cmn-table-pager"),outHandler,options);
  }
  /*
   * 获取实付款表格参数
   * */
  function getTableParams(params) {
    var msg=gatherReceiptsData();

    params.pageNum = (params.offset/params.limit)+1;
    params.pageSize = params.limit;
    $.extend(params,msg);
    params.derection="asc";
    params.sort="id";
    params.date = parentInfo.operationDate;
    params.projectCode = _pjCode;
    params.buyerCompanyCode = _cmpCode;
    return params;
  }
  /*
   * 响应回调函数
   * */
  function tradeReceiptsTableResData($data,i) {
    $data.index = i + 1;//序号
    $data.createdAt = QHConfig.timeFmt($data.createdAt,2);//收款日期
    $data.totalPrice = QHConfig.turnMoney($data.totalPrice,2);//收款金额
  }
  /*
   * 获取查询条件
   * */
  function gatherReceiptsData() {
    var msg={};
    $(".cmn-query-container").find("[data-name]").each(function(){
      var k = $(this).attr("data-name"); // 字段名
      var val = $(this).val(); // 值

      if(val != "") {
        msg[k] = val;
      }
    });
    return msg;
  }
  //跳转也是表格刷新
  function outHandler(){
    //执行bootstrapTable分页跳转
    $("#id-user-project-table").bootstrapTable("selectPage",pager.getPage());
  }
}
