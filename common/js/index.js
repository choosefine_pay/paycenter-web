/*
 * Create by    : LiChao
 * Create time  : 2017/3/17
 * File explain : 主框交互文件 包含侧边导航栏交互
 **/

require.config({
  baseUrl: "libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
	vue: ["./vue2-4-1/vue"],
	header:["../components/my-header"],
    menu:["../components/my-menu"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    temp: ["template"],
    common: ["../common/js/common-config"],
    MD5:["//cdn.bootcss.com/blueimp-md5/2.7.0/js/md5"]
  }
});
require(["jquery"], function () {
  require(["vue","header","menu","temp","MD5", "zui", "layer", "common"], operationCodeFun);
});
function operationCodeFun(vue,header,menu,template,md5) {
  layer.config({path: "libs/layer/"});
  // 变量声明
  const QHConfig = $("body").data("config");
  var _Data = {}; // 导航栏数据载体
  var html = ""; // html数据载体
  var role = $("body").data("config").rolePages;
  //var rolePages = role + "-pages";
  var rolePages ="tpl";
  var forgetTips = {
    oldPassword:"请输入原密码",
    newPassword:"请输入新密码",
    sureNewPassword:"请确认新密码",
    differentPassword:"您两次输入的新密码不一致，请重新输入",
    samePassword:"输入的新密码与原密码一致，请重新输入"
  };//错误提示
  // end

  // 页面渲染初始化
  // 获取导航栏数据
  var _mData=[];
  $.ajax({
    url: "common/data/manage_type.json",
    async: false,
    success: function (res) {

      //urlFmt(res);

      function urlFmt(data) {
        $.each(data, function () {
          //this.url = role + "-" + this.url;
		  this.url = "mdu-" + this.url;
          if (this.children !== null) {
            urlFmt(this.children)
          }
        });
      }

	  _mData=res;
	  //alert(JSON.stringify(_mData));
    }
  });
  window.initTop(vue);
  window.initMenu(vue);

  // 页面初始化事件绑定
  // 卖家LOGO 点击事件并初始化被点击
  $(".index-nav-logo").on("click", navLogoClick)[0].click();
  //登出按钮点击事件绑定
  $("#id-login-out").on("click", function () {
    sessionStorage.removeItem('token');
    window.location.href = "common/login-page/login-page.html";
  });
  //修改密码事件
  $(".index-forget-password").on("click",layerForgetPassword);
  //end

  // 事件函数
  /*
   * 卖家LOGO点击事件
   * @author LiChao
   * @Date   2017/3/17
   * */
  function navLogoClick() {
    var businessUrl ="tpl/mdu-buyer-pay/mdu-buyer-pay.html";
    $("#id-main-frame").attr("src", businessUrl); // 跳转至商家中心
    asideNavRender(); // 渲染导航栏
    navClickBind(); // 导航栏点击事件绑定
  }

  function asideNavRender() {
    //$(".index-first-nav").html(html); // 导航栏渲染
    // 导航栏默认图片渲染
    $(".index-first-nav-icon").each(function (i) {
      $(this).css("background", "url(common/img/index/" + _mData[i].beforeImg + ") no-repeat center center");
    });
  }

  function navClickBind() {
    // 一级菜单点击事件绑定
    $(".index-first-nav > li").on("click", navFstMenuClick);
    // 二级菜单点击事件绑定
    //$(".index-second-nav > li").on("click", navSecMenuClick);
  }

  /*
   * 一级菜单点击事件
   * @author  LiChao
   * @Date    2017/3/17
   * */
  function navFstMenuClick() {
    var isActive = $(this).hasClass("active"); // 是否有active类名
    var isDelivery = $(this).hasClass("index-delivery"); // 是否是发货管理
    var iconSpan = $(this).find(".index-first-nav-icon"); // 图片容器
    var newIconName = ""; // 新图片地址文本
    var fieldName = $(this).attr("data-field"); // 对应文件名
    var iframeUrl = rolePages + "/" + fieldName + "/" + fieldName + ".html"; // 对应链接地址
    var otherActive = $(this).siblings(".active"); // 其他的选中元素

    // 如果存在被选中的
    if (otherActive.length > 0) {
      // 获取地址
      var otherIconName = otherActive.find(".index-first-nav-icon").css("background-image");
      // 修改地址 匹配地址中_click字符串，将其替换为空
      var newOtherIconName = otherIconName.replace(/_click/, function () {
        return ""
      });
    }
	
    // 没被选中时
    if (isActive == false) {
      $(this).addClass("active"); // 当前li添加active类名
      otherActive.removeClass("active"); // 去除其他li的active类名
      // 还原之前的图片
      if (otherActive.length > 0) {
        otherActive.find(".index-first-nav-icon").css("background-image", newOtherIconName);
      }
      // 不是发货管理时
      if (isDelivery == false) {
        // 修改图片地址
        newIconName = iconSpan.css("background-image").replace(/\.png/, function () {
          return "_click.png"
        });
        iconSpan.css("background-image", newIconName);
        // 收起发货管理子列表并修改箭头指向
        $(".index-delivery").find(".index-second-nav").addClass("hide")
          .parent().find(".index-first-nav-down").removeClass("active");
        $(".index-second-nav").find(".active").removeClass("active"); // 发货管理子集失去选中
        $("#id-main-frame").attr("src", iframeUrl); // 修改iframe地址
      } else {
        $(this).find(".index-first-nav-down").addClass("active"); // 修改右侧箭头指向
        $(this).find(".index-second-nav").removeClass("hide"); // 展开子列表
        $(this).find(".index-second-nav > li")[0].click(); // 触发二级菜单首项点击事件
      }
    } else {
      if (isDelivery == false) {
        $("#id-main-frame").attr("src", iframeUrl);
      }
    }
  }

  /*
   * 二级菜单点击事件
   * @author  LiChao
   * @Date    2017/3/17
   * */
  function navSecMenuClick() {
    var isActive = $(this).hasClass("active"); // 是否有active类名
    var fieldName = $(this).data("field"); // 对应文件名
    var iframeUrl = rolePages + "/" + fieldName + "/" + fieldName + ".html"; // 对应链接地址
    if (isActive == false) {
      $(this).addClass("active").siblings().removeClass("active");
      // 修改iframe地址
      $("#id-main-frame").attr("src", iframeUrl);
    } else {
      backToParent(iframeUrl);
    }
  }

  /*
   * 返回第一页
   * 当模块跳转到其他页面点击导航栏可以继续返回模块首页
   * @param  iframeUrl 模块对应的首页
   * @author LiChao
   * @Date   2017/3/23
   * */
  function backToParent(iframeUrl) {
    var frame = $("#id-main-frame");
    var frameSrc = frame.attr("src");
    if (iframeUrl !== frameSrc) {
      frame.attr("src", iframeUrl)
    }
  }

  /*
   * 商家信息修改未保存提示信息函数
   * @param  thisList 触发时点击的项，用于点确定按钮时再次触发点击事件
   * @author wsz
   * @Date   2017/3/22
   * */
  function noSaveHintFun(thisList) {
    //离开当前页面触发事件
  }
  /*
   * 修改密码弹窗
   * @author wsz
   * @Date   2017/07/07
   * */
  function layerForgetPassword() {
    layer.open({
      type: 2,
      closeBtn: 1,
      shade: 0.5,
      title: "修改密码",
      area: ['400px', '370px'],
      content: 'common/tpl/cmn-forget-password.html',
      success: function (l, index) {
        var sure = layer.getChildFrame(".cmn-save-btn", index);//确定按钮
        var cancel = layer.getChildFrame(".cmn-cancel-btn", index);//取消按钮
        var oldPassword = layer.getChildFrame(".old-password", index);//原密码
        var newPassword = layer.getChildFrame(".new-password", index);//新密码
        var sureNewPassword = layer.getChildFrame(".sure-new-password", index);//确认新密码
        var tips = layer.getChildFrame(".cmn-tips", index);//提示语
        var allInput = layer.getChildFrame(".cmn-input", index);//所有输入框

        //密码由6~20位数字、英文字母（区分大小写）组成
        allInput.on("input",onlyNumber);
        //点击确定按钮
        sure.on("click", function () {
          toChangePassword(oldPassword,newPassword,sureNewPassword,tips);
        });
        cancel.on("click", function () {
          layer.close(index);
        });
      }
    });
  }
  /*
   * 只能由数字字母组成
   * @author wsz
   * @Date   2017/07/07
   * */
  function onlyNumber() {
    var passwordReg = /[^0-9A-Za-z]/g;
    var thisVal = $(this).val();

    $(this).val(thisVal.replace(passwordReg,""));
  }
  /*
   * 点击确定按钮，判断是否符合修改密码的条件
   * @params oldPassword:原密码输入框；newPassword:新密码输入框；sureNewPassword:确认新密码输入框；tips：错误提示；
   * @author wsz
   * @Date   2017/07/07
   * */
  function toChangePassword(oldPassword,newPassword,sureNewPassword,tips){
    var oldVal = oldPassword.val();//原密码值
    var newVal = newPassword.val();//新密码值
    var sureNewVal = sureNewPassword.val();//确认密码值

    //判断原密码是否为空或者长度小于6位
    if(checkedPassword(oldPassword,tips,forgetTips.oldPassword)) {
      return;
    };
    //判断新密码是否为空或者长度小于6位
    if(checkedPassword(newPassword,tips,forgetTips.newPassword)) {
      return;
    };
    //确认新密码是否为空或者长度小于6位
    if(checkedPassword(sureNewPassword,tips,forgetTips.sureNewPassword)) {
      return;
    };
    //判断新密码与确认新密码是否相同
    if(sureNewVal != newVal) {
      handleInput(newPassword,tips,forgetTips.differentPassword,sureNewPassword);
      return;
    }
    //判断新密码是否与原密码一样
    if(newVal == oldVal) {
      handleInput(newPassword,tips,forgetTips.samePassword,sureNewPassword);
      return;
    }
    //验证通过，请求修改密码
    var needData = {
      oldPassword: md5(oldVal),
      newPassword: md5(newVal)
    };//需要的参数：原密码与新密码
    doChangeLoginPassword(needData,tips,oldPassword);
  }
  /*
   * 判断输入的密码是否为空和是否大于6位数
   * @params inputEle:输入框；tipEle:错误提示元素；tipText:提示语内容
   * @author wsz
   * @Date   2017/07/07
   * */
  function checkedPassword(inputEle,tipEle,tipText) {
    var thisVal = inputEle.val();//原密码

    if( thisVal == "" ||thisVal.length < 6 ) {
      handleInput(inputEle,tipEle,tipText);
      return true;
    }
  }
  /*
   * 清空输入框并且聚焦；显示提示语
   * @params inputEle:输入框；tipEle:错误提示元素；tipText:提示语内容,secondEle:第二个元素
   * @author wsz
   * @Date   2017/07/07
   * */
  function handleInput(inputEle,tipEle,tipText,secondEle) {
    inputEle.val("");
    if(secondEle){
      secondEle.val("");
    }
    inputEle[0].focus();
    tipEle.text(tipText).css("visibility","visible");
  }
  /*
   * 请求修改密码
   * @params needData:需要的参数；tips:错误提示元素
   * @author wsz
   * @Date   2017/07/07
   * */
  function doChangeLoginPassword(needData,tips,oldPassword){
    $.ajax(tokenAjax({
      type:"PUT",
      url:QHConfig.url.changePassword,//"http://192.168.59.3:6091/Authority/send/web/shopping/password",
      data:JSON.stringify(needData),
      dataType:"json",
      contentType:"application/json",
      success:function(data) {
        if(data.status == 200) {
          tips.css("visibility","hidden");//成功后隐藏提示语
          layer.closeAll();
          layer.msg("修改成功，请重新登录");
          //修改密码成功退出登录
          setTimeout(function() {
            $("#id-login-out")[0].click();
          },2000);
        }else{
          if(data.message == "您输入的原密码错误") {
            oldPassword.val("");
            oldPassword[0].focus();
          }
          tips.text(data.message).css("visibility","visible");//提示错误信息
        }
      }
    }))
  }
}