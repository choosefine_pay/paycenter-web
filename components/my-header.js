define(['vue'],function(Vue){
	function getHeadTemplate(){
		return '<header class="index-navbar index-header">'+
            '<div class="index-navbar-qhlogo">支付中心</div>'+
            '<div class="index-navbar-center"></div>'+
            '<div id="id-login-out" class="index-navbar-close" onclick="logout()">{{username}}</div>'+
            '<div class="index-action"><a href="../../tpl/set-payment-password/set-payment-password.html"><span class="index-forget-password">&nbsp;</span></a></div>'+
    '</header>'
	}
	var header=Vue.extend({
		props: ['username'],
		template: getHeadTemplate()
	});
	Vue.component('my-header',header);
	//初始化公共头部
	window.initTop=function(_vue){
		var app0 = new _vue({
		  el: '#app0',
		  data: {
			message: ''
		  },
		  methods:{
			logout:function(){
			}
		  }
		})
		window.logout=function(){
			sessionStorage.removeItem('token');
			window.location.href = "../../common/login-page/login-page.html";
		}
	}
});