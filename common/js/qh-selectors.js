/*
 Created by   : TernWii
 Created time : 2017/7/4   areas update
 File explain : JavaScript Library selector collection
 */
/*
 *+++++++++++++++++++++++++++++++++++++以下地区多选组件，适用于供货地区的选择，显示+++++++++++++++++++++++++++++++++++++++++++++
 */
function UseMultiSelector(root){
  //构造函数
  function multiSelector($_target){
    this.$target=$_target;
	this.id='multi_'+new Date().getTime();
	this.selects=[];
	this.onOpen=false;
  }
  multiSelector.prototype.init=function(_opt,_data){
    if(_opt.title)this.title=_opt.title;
    else this.title='请选择';
    if(_opt.boxWidth)this.boxWidth=_opt.boxWidth;
    else this.boxWidth=500;
    if(_opt.boxHeight)this.boxHeight=_opt.boxHeight;
    else this.boxHeight=400;
    if(_opt.offX)this.offX=_opt.offX;
    else this.offX=0;
    if(_opt.offY)this.offY=_opt.offY;
    else offY=0;
    if(_opt.textField)this.textField=_opt.textField;
    else this.textField='name';
    if(_opt.valueField)this.valueField=_opt.valueField;
    else valueField='id';
    if(_opt.childsField)this.childsField=_opt.childsField;
    else this.childsField='list';
    this.allData=_data;
    this.Dict=new Array();
    this.sonDict=new Array();
    for(var d=0;d<this.allData.length;d++){
      this.Dict[this.allData[d][this.valueField]]=this.allData[d][this.childsField];
      if(this.allData[d][this.childsField]){
        var list=this.allData[d][this.childsField];
        for(c=0;c<list.length;c++){
          this.sonDict[list[c][this.valueField]]={
            "fathor":this.allData[d][this.valueField],
            "name":list[c][this.textField],
            "value":list[c][this.valueField]
          };
        }
      }
    }
	var that=this;
    this.$target.on('click',function(){
	  if(that.onOpen)return false;
	  that.onOpen=true;
      that.render(that.selects,that.all1,that.all2);
	  $("#"+that.id).show();
	  //自动点击第一个省份
      $('.prov-item:first').click();
    });
  };
  //生成全部选择项
  multiSelector.prototype.render=function(selects,allTxt1,allTxt2){
    if(selects)this.selects=selects;
    else this.selects=[];
    if(allTxt1)this.all1=allTxt1;
    else this.all1='全部';
    if(allTxt2)this.all2=allTxt2;
    else this.all2='全部';
    //rendering...
    if(this.$target.next().attr('id')!=this.id)
    this.$target.after("<div id='"+this.id+"' class='multi-selector hide' style='position:absolute;width:"+this.boxWidth
      +"px;'><div class='multi-selector-t'><span class='multi-selector-txt'>"
      +this.title+"</span><span class='multi-selector-close'><i class='icon icon-times'></i></span></div><div id='get_"
      +this.id+"' class='multi-selector-get'></div><span class='multi-selector-all'><input class='multi all' id='multi-all' type='checkbox' value='all'>"
      +this.all1+"</span><div class='multi-selector-main' style='width: "
      +this.boxWidth+"px;'><div class='multi-provens' style='height:"+this.boxHeight
      +"px;overflow:scroll;'></div><div class='multi-cities-top'><input class='cities all' type='checkbox' value='all'>"
      +this.all2+"</div><div class='multi-cities'></div></div><span class='multi-footor'><input class='cmn-btn-multi cmn-cancel-btn' type='button' value='取消'>"
      +"&nbsp;&nbsp;<input class='cmn-btn-multi cmn-save-btn' type='button' value='确定'></span></div>");
	  
	this.offset =this.$target.position();
      $("#"+this.id).css({
        top: this.offset.top + this.offY,
        left: this.offset.left+this.offX
      }).hide();
	  
    var that=this;
    function addProvence(pid){
      if($('#selected_'+pid).length>0)
        $(".multi-selector-get").append($('#selected_'+pid));
      else
        $(".multi-selector-get").append("<span class='multi-selected prov' id='selected_"+pid+"'>"
          +$('#'+pid).text()+"&nbsp;<span>x</span></span>");
      $('#selected_'+pid).on('click',function(){
        that.cancelSelected($(this));
      })
    }
	function countProvs(){
	//选满全国的变化
	  if($(".multi-selector-get").find('.multi-selected.prov').length==that.allData.length){
		$(".multi-selector-get").find('.multi-selected.prov').remove();
		addCountry();
	  }
	  else if($(".multi-selected.all").length>0){
		$(".multi-selector-get").find('.multi-selected.prov').remove();  
	  }
	}
    function addCity(cid,pid,text) {
      if($(".multi-selector-get").find("#selected_"+cid).length>0){
        $(".multi-selector-get").append($("#selected_"+cid));
      }
      else{
        $(".multi-selector-get").append("<span class='multi-selected city "+pid+"' id='selected_"
          +cid+"'>"+text+"&nbsp;<span>x</span></span>");
      }
      $('#selected_'+cid).on('click',function(){
        that.cancelSelected($(this));
      })
    }
    function initCityChecks(pid){
      for(var c=0;c<that.Dict[pid].length;c++){
        var value=that.Dict[pid][c][that.valueField];
        var text=that.Dict[pid][c][that.textField];
        $('.multi-cities').append("<div class='city-item'><input class='"+pid+" multi-city' id='city_"
          +value+"' type='checkbox' value='"+value+"'/>"+text+"</div>");
        //已选中的市要勾选状态
        if($(".multi-selector-get").find("#selected_"+value).length>0){
          $("#selected_"+value).prop('checked',true);
        }
        else{
          $("#selected_"+value).prop('checked',false);
        }
      }
    }
    function addCountry(){
	  $("#multi-all").prop('checked',true);
      if($(".multi-selector-get").find("#selected-all").length==0){
        $(".multi-selector-get").append("<span class='multi-selected all' id='selected-all'>"+that.all1+"&nbsp;<span>x</span></span>");
      }
      else{
        $(".multi-selector-get").append($("#selected-all"));
      }
      $('#selected-all').on('click',function(){
        that.cancelSelected($(this));
      })
    }
    function addAllProvBut(pid){
      for(key in that.Dict){
        if(key==pid)continue;
        else{
          addProvence(key);
        }
      }
    }
	function allSelected(){
		addCountry();
        //选中全部市区
        $('.multi-city').prop('checked',true);
        $('.cities.all').prop('checked',true);
	}
    function addCityEvent($city){
      $city.on('click',function(){
        //是取消市的选择
        var provid=$('.prov-item.on').attr('id');
        var cityid=$(this).attr('id').replace('city_','');
        if(!$(this).prop('checked')){
			$(".multi.all").prop('checked',false);
          if($(".multi-selector-get").find("#selected-all").length>0){
            $(".multi-selector-get #selected-all").remove();
            //还原其他全部省份标签
            addAllProvBut(provid);
          }
          //再dele全省
          if($(".multi-selector-get").find("#selected_"+provid).length>0){
            $("#selected_"+provid).remove();
          }
          $('.cities.all').prop('checked',false);
          //还原出其他市
          $('.multi-city').each(function(){
            if($(this).prop('checked')){
              var cid=$(this).attr('id').replace('city_','');
              addCity(cid,provid,$(this).parent().text());
            }
          });
          //dele本市
          if($(".multi-selector-get").find("#selected_"+cityid).length>0){
            $(".multi-selector-get #selected_"+cityid).remove();
          }
        }
        else{
          addCity(cityid,provid,$(this).parent().text());
          //选满全省的变化
          if(that.Dict[provid].length==$(".multi-selector-get").find('.'+provid).length){
            $('.cities.all').prop('checked',true);
            $(".multi-selector-get").find('.'+provid).each(function(){
              $(this).remove();
            });
            addProvence(provid);
			countProvs();
          }
          
        }
      });
    }

    //生成所有省份
	$('.multi-provens').html('');
    for(var p=0;p<that.allData.length;p++){
      $('.multi-provens').append("<div class='prov-item' id='"+that.allData[p][that.valueField]+"'>"+that.allData[p][that.textField]+"<div>");
    }
    //全国按钮的点击事件
    $('#multi-all').on('click',function(){
      $(".multi-selector-get").empty();//先清空全部
      if($('#multi-all').prop('checked')){
        //选中全国
        allSelected();
      }
      else{
        //取消全国,取消选中全部市区
        $('.multi-city').prop('checked',false);
        $('.cities.all').prop('checked',false);
      }
    });

    /*省份菜单的点击事件                          start                             */
    $('.prov-item').on('click',function(){
      $('.multi-cities').html('');
      $('.prov-item').attr('class','prov-item');
      $(this).attr('class','prov-item on');
      initCityChecks($(this).attr('id'));
      //全国选中时,或有本省标签在，要默认全选中全部市
      if($('#multi-all').prop('checked')||$(".multi-selector-get").find('#selected_'+$(this).attr('id')).length>0){
        $('.multi-city').prop('checked',true);
        $('.cities.all').prop('checked',true);
      }
      else{
        //看哪些市被选中了。
        if($(".multi-selector-get").find('.'+$(this).attr('id')).length==that.Dict[$(this).attr('id')].length){
			if(that.Dict[$(this).attr('id')].length>0)
				$('.cities.all').prop('checked',true);
			else
				$('.cities.all').prop('checked',false);
        }
        else
          $('.cities.all').prop('checked',false);
        $(".multi-selector-get").find("."+$(this).attr('id')).each(function(){
          $('#city_'+$(this).attr('id').replace('selected_','')).prop('checked',true);
        });

      }
      //每个市的点击事件
      $('.multi-city').each(function(){
        addCityEvent($(this));
      });
    })/*                                         end                             */


    //全省按钮的点击事件
    $('.cities.all').on('click',function(){
      var provid=$('.prov-item.on').attr('id');
      //无条件dele省下市标签
      $('.multi-selected.city.'+provid).remove();
      if($('.cities.all').prop('checked')){
        //选中全省
        $('.'+provid+'.multi-city').prop('checked',true);
        addProvence(provid);
        //删除所有添加过的本省内市标签
        $('.multi-selected '+provid).remove();
        //为省标签添加点击事件
        $('#selected_'+provid).on('click',function(){
          that.cancelSelected($(this));
        })
		//console.log($(this).attr('class'));
        countProvs();
      }
      else{
        //取消全省，同时也取消全国选中
        $('.'+provid+'.multi-city').prop('checked',false);
        if($('#selected_'+provid).length>0)
          $('#selected_'+provid).remove();
        $('#multi-all').prop('checked',false);
        if($(".multi-selector-get").find('#selected-all').length>0){
          $('#selected-all').remove();
          //还原其他全部省份标签
          addAllProvBut(provid);
        }
      }
    })
    //默认选择已有数据项
    for(var s=0;s<that.selects.length;s++){
      var value=that.selects[s];
      if(value=='all'){
		allSelected();
		break;
	  }
      else if($('.multi-provens #'+value).length>0){
        addProvence(value);
      }
      else{
        addCity(value,that.sonDict[value].fathor,that.sonDict[value].name);
      }
    }
	function cancelAndClose(){
	  $("#"+that.id).remove();
	  $("#"+that.id).attr('id','');
	  that.onOpen=false;
	}
    //关闭按钮的点击事件
    $('.multi-selector-close i').on('click',function(){
      //$("#"+that.id).css('display','none');
	  //that.onOpen=false;
	  cancelAndClose();
    })
    //取消按钮的点击事件
    $('.cmn-btn-multi.cmn-cancel-btn').on('click',function(){
      cancelAndClose();
    })
    //保存按钮的点击事件
    $('.cmn-btn-multi.cmn-save-btn').on('click',function(){
      that.$target.text(that.multiString());
	  that.getValues();
      $("#"+that.id).css('display','none');
	  that.onOpen=false;
    })
    //先显示默认结果
    that.$target.text(that.multiString());
  };
  multiSelector.prototype.cancelSelected=function($obj){
    //无条件dele全国
    $('#multi-all').prop('checked',false);
    if($(".multi-selector-get").find('#selected-all').length>0){
		$('#selected-all').remove();
		$("#"+this.id).find("input[type='checkbox']").prop('checked',false);
	}
      
    //正在dele省
    if($obj.attr('class')=='multi-selected prov'){
      $obj.remove();
      //正在dele当前省
      var provid=$obj.attr('id').replace('selected_','');
      if(provid==$('.prov-item.on').attr('id')){
        $('.cities.all').prop('checked',false);
        $('.'+provid+'.multi-city').prop('checked',false);
      }
    }
    //正在dele市
    if($obj.attr('class').indexOf('multi-selected city')>-1){
      $obj.remove();
	  var _cid=$obj.attr('id').replace('selected_','');
	  if($("#city_"+_cid).length>0)
		$("#city_"+_cid).prop('checked',false);  
    }
  };
  multiSelector.prototype.multiString=function(){
    var str='';
    var that=this;
    $(".multi-selector-get").find('.multi-selected.all').each(function(){
      str+='+'+that.all1;
    });
    $(".multi-selector-get").find('.multi-selected.prov').each(function(){
      str+='+'+$(this).text().replace('x','');
    });
    $(".multi-selector-get").find('.multi-selected.city').each(function(){
      str+='+'+$(this).text().replace('x','');
    });
	str.replace('+','');
	if(str=='')str='未选择';
    return str.replace('+','');
  }
  multiSelector.prototype.getValues=function(){
    var values=[];
    $(".multi-selector-get").find('.multi-selected.all').each(function(){
      values.push('all');
    });
    $(".multi-selector-get").find('.multi-selected.prov').each(function(){
      values.push($(this).attr('id').replace('selected_',''));
    });
    $(".multi-selector-get").find('.multi-selected.city').each(function(){
      values.push($(this).attr('id').replace('selected_',''));
    });
	this.selects=values;
    return values;
  }
  root.multiSelectorFactory=function(target){
    return new multiSelector(target);
  }
}
/*级联选择选项完整数据在此编辑，也可从后台获取*/
function getStaticData(_name){
  var public_configs={
    "areas":[{"areaName":"北京市","areaCode":"110000","areaShortName":null,"subAreas":[{"areaName":"东城区","areaCode":"110101","areaShortName":null},{"areaName":"西城区","areaCode":"110102","areaShortName":null},{"areaName":"朝阳区","areaCode":"110105","areaShortName":null},{"areaName":"丰台区","areaCode":"110106","areaShortName":null},{"areaName":"石景山区","areaCode":"110107","areaShortName":null},{"areaName":"海淀区","areaCode":"110108","areaShortName":null},{"areaName":"门头沟区","areaCode":"110109","areaShortName":null},{"areaName":"房山区","areaCode":"110111","areaShortName":null},{"areaName":"通州区","areaCode":"110112","areaShortName":null},{"areaName":"顺义区","areaCode":"110113","areaShortName":null},{"areaName":"昌平区","areaCode":"110114","areaShortName":null},{"areaName":"大兴区","areaCode":"110115","areaShortName":null},{"areaName":"怀柔区","areaCode":"110116","areaShortName":null},{"areaName":"平谷区","areaCode":"110117","areaShortName":null},{"areaName":"密云区","areaCode":"110118","areaShortName":null},{"areaName":"延庆区","areaCode":"110119","areaShortName":null}]},{"areaName":"天津市","areaCode":"120000","areaShortName":null,"subAreas":[{"areaName":"和平区","areaCode":"120101","areaShortName":null},{"areaName":"河东区","areaCode":"120102","areaShortName":null},{"areaName":"河西区","areaCode":"120103","areaShortName":null},{"areaName":"南开区","areaCode":"120104","areaShortName":null},{"areaName":"河北区","areaCode":"120105","areaShortName":null},{"areaName":"红桥区","areaCode":"120106","areaShortName":null},{"areaName":"东丽区","areaCode":"120110","areaShortName":null},{"areaName":"西青区","areaCode":"120111","areaShortName":null},{"areaName":"津南区","areaCode":"120112","areaShortName":null},{"areaName":"北辰区","areaCode":"120113","areaShortName":null},{"areaName":"武清区","areaCode":"120114","areaShortName":null},{"areaName":"宝坻区","areaCode":"120115","areaShortName":null},{"areaName":"滨海新区","areaCode":"120116","areaShortName":null},{"areaName":"宁河区","areaCode":"120117","areaShortName":null},{"areaName":"静海区","areaCode":"120118","areaShortName":null},{"areaName":"蓟州区","areaCode":"120119","areaShortName":null}]},{"areaName":"河北省","areaCode":"130000","areaShortName":null,"subAreas":[{"areaName":"石家庄市","areaCode":"130100","areaShortName":null},{"areaName":"唐山市","areaCode":"130200","areaShortName":null},{"areaName":"秦皇岛市","areaCode":"130300","areaShortName":null},{"areaName":"邯郸市","areaCode":"130400","areaShortName":null},{"areaName":"邢台市","areaCode":"130500","areaShortName":null},{"areaName":"保定市","areaCode":"130600","areaShortName":null},{"areaName":"张家口市","areaCode":"130700","areaShortName":null},{"areaName":"承德市","areaCode":"130800","areaShortName":null},{"areaName":"沧州市","areaCode":"130900","areaShortName":null},{"areaName":"廊坊市","areaCode":"131000","areaShortName":null},{"areaName":"衡水市","areaCode":"131100","areaShortName":null},{"areaName":"定州市","areaCode":"139001","areaShortName":null},{"areaName":"辛集市","areaCode":"139002","areaShortName":null}]},{"areaName":"山西省","areaCode":"140000","areaShortName":null,"subAreas":[{"areaName":"太原市","areaCode":"140100","areaShortName":null},{"areaName":"大同市","areaCode":"140200","areaShortName":null},{"areaName":"阳泉市","areaCode":"140300","areaShortName":null},{"areaName":"长治市","areaCode":"140400","areaShortName":null},{"areaName":"晋城市","areaCode":"140500","areaShortName":null},{"areaName":"朔州市","areaCode":"140600","areaShortName":null},{"areaName":"晋中市","areaCode":"140700","areaShortName":null},{"areaName":"运城市","areaCode":"140800","areaShortName":null},{"areaName":"忻州市","areaCode":"140900","areaShortName":null},{"areaName":"临汾市","areaCode":"141000","areaShortName":null},{"areaName":"吕梁市","areaCode":"141100","areaShortName":null}]},{"areaName":"内蒙古自治区","areaCode":"150000","areaShortName":null,"subAreas":[{"areaName":"呼和浩特市","areaCode":"150100","areaShortName":null},{"areaName":"包头市","areaCode":"150200","areaShortName":null},{"areaName":"乌海市","areaCode":"150300","areaShortName":null},{"areaName":"赤峰市","areaCode":"150400","areaShortName":null},{"areaName":"通辽市","areaCode":"150500","areaShortName":null},{"areaName":"鄂尔多斯市","areaCode":"150600","areaShortName":null},{"areaName":"呼伦贝尔市","areaCode":"150700","areaShortName":null},{"areaName":"巴彦淖尔市","areaCode":"150800","areaShortName":null},{"areaName":"乌兰察布市","areaCode":"150900","areaShortName":null},{"areaName":"兴安盟","areaCode":"152200","areaShortName":null},{"areaName":"锡林郭勒盟","areaCode":"152500","areaShortName":null},{"areaName":"阿拉善盟","areaCode":"152900","areaShortName":null}]},{"areaName":"辽宁省","areaCode":"210000","areaShortName":null,"subAreas":[{"areaName":"沈阳市","areaCode":"210100","areaShortName":null},{"areaName":"大连市","areaCode":"210200","areaShortName":null},{"areaName":"鞍山市","areaCode":"210300","areaShortName":null},{"areaName":"抚顺市","areaCode":"210400","areaShortName":null},{"areaName":"本溪市","areaCode":"210500","areaShortName":null},{"areaName":"丹东市","areaCode":"210600","areaShortName":null},{"areaName":"锦州市","areaCode":"210700","areaShortName":null},{"areaName":"营口市","areaCode":"210800","areaShortName":null},{"areaName":"阜新市","areaCode":"210900","areaShortName":null},{"areaName":"辽阳市","areaCode":"211000","areaShortName":null},{"areaName":"盘锦市","areaCode":"211100","areaShortName":null},{"areaName":"铁岭市","areaCode":"211200","areaShortName":null},{"areaName":"朝阳市","areaCode":"211300","areaShortName":null},{"areaName":"葫芦岛市","areaCode":"211400","areaShortName":null}]},{"areaName":"吉林省","areaCode":"220000","areaShortName":null,"subAreas":[{"areaName":"长春市","areaCode":"220100","areaShortName":null},{"areaName":"吉林市","areaCode":"220200","areaShortName":null},{"areaName":"四平市","areaCode":"220300","areaShortName":null},{"areaName":"辽源市","areaCode":"220400","areaShortName":null},{"areaName":"通化市","areaCode":"220500","areaShortName":null},{"areaName":"白山市","areaCode":"220600","areaShortName":null},{"areaName":"松原市","areaCode":"220700","areaShortName":null},{"areaName":"白城市","areaCode":"220800","areaShortName":null},{"areaName":"延边朝鲜族自治州","areaCode":"222400","areaShortName":null}]},{"areaName":"黑龙江省","areaCode":"230000","areaShortName":null,"subAreas":[{"areaName":"哈尔滨市","areaCode":"230100","areaShortName":null},{"areaName":"齐齐哈尔市","areaCode":"230200","areaShortName":null},{"areaName":"鸡西市","areaCode":"230300","areaShortName":null},{"areaName":"鹤岗市","areaCode":"230400","areaShortName":null},{"areaName":"双鸭山市","areaCode":"230500","areaShortName":null},{"areaName":"大庆市","areaCode":"230600","areaShortName":null},{"areaName":"伊春市","areaCode":"230700","areaShortName":null},{"areaName":"佳木斯市","areaCode":"230800","areaShortName":null},{"areaName":"七台河市","areaCode":"230900","areaShortName":null},{"areaName":"牡丹江市","areaCode":"231000","areaShortName":null},{"areaName":"黑河市","areaCode":"231100","areaShortName":null},{"areaName":"绥化市","areaCode":"231200","areaShortName":null},{"areaName":"大兴安岭地区","areaCode":"232700","areaShortName":null}]},{"areaName":"上海市","areaCode":"310000","areaShortName":null,"subAreas":[{"areaName":"黄浦区","areaCode":"310101","areaShortName":null},{"areaName":"徐汇区","areaCode":"310104","areaShortName":null},{"areaName":"长宁区","areaCode":"310105","areaShortName":null},{"areaName":"静安区","areaCode":"310106","areaShortName":null},{"areaName":"普陀区","areaCode":"310107","areaShortName":null},{"areaName":"虹口区","areaCode":"310109","areaShortName":null},{"areaName":"杨浦区","areaCode":"310110","areaShortName":null},{"areaName":"闵行区","areaCode":"310112","areaShortName":null},{"areaName":"宝山区","areaCode":"310113","areaShortName":null},{"areaName":"嘉定区","areaCode":"310114","areaShortName":null},{"areaName":"浦东新区","areaCode":"310115","areaShortName":null},{"areaName":"金山区","areaCode":"310116","areaShortName":null},{"areaName":"松江区","areaCode":"310117","areaShortName":null},{"areaName":"青浦区","areaCode":"310118","areaShortName":null},{"areaName":"奉贤区","areaCode":"310120","areaShortName":null},{"areaName":"崇明区","areaCode":"310151","areaShortName":null}]},{"areaName":"江苏省","areaCode":"320000","areaShortName":null,"subAreas":[{"areaName":"南京市","areaCode":"320100","areaShortName":null},{"areaName":"无锡市","areaCode":"320200","areaShortName":null},{"areaName":"徐州市","areaCode":"320300","areaShortName":null},{"areaName":"常州市","areaCode":"320400","areaShortName":null},{"areaName":"苏州市","areaCode":"320500","areaShortName":null},{"areaName":"南通市","areaCode":"320600","areaShortName":null},{"areaName":"连云港市","areaCode":"320700","areaShortName":null},{"areaName":"淮安市","areaCode":"320800","areaShortName":null},{"areaName":"盐城市","areaCode":"320900","areaShortName":null},{"areaName":"扬州市","areaCode":"321000","areaShortName":null},{"areaName":"镇江市","areaCode":"321100","areaShortName":null},{"areaName":"泰州市","areaCode":"321200","areaShortName":null},{"areaName":"宿迁市","areaCode":"321300","areaShortName":null}]},{"areaName":"浙江省","areaCode":"330000","areaShortName":null,"subAreas":[{"areaName":"杭州市","areaCode":"330100","areaShortName":null},{"areaName":"宁波市","areaCode":"330200","areaShortName":null},{"areaName":"温州市","areaCode":"330300","areaShortName":null},{"areaName":"嘉兴市","areaCode":"330400","areaShortName":null},{"areaName":"湖州市","areaCode":"330500","areaShortName":null},{"areaName":"绍兴市","areaCode":"330600","areaShortName":null},{"areaName":"金华市","areaCode":"330700","areaShortName":null},{"areaName":"衢州市","areaCode":"330800","areaShortName":null},{"areaName":"舟山市","areaCode":"330900","areaShortName":null},{"areaName":"台州市","areaCode":"331000","areaShortName":null},{"areaName":"丽水市","areaCode":"331100","areaShortName":null}]},{"areaName":"安徽省","areaCode":"340000","areaShortName":null,"subAreas":[{"areaName":"合肥市","areaCode":"340100","areaShortName":null},{"areaName":"芜湖市","areaCode":"340200","areaShortName":null},{"areaName":"蚌埠市","areaCode":"340300","areaShortName":null},{"areaName":"淮南市","areaCode":"340400","areaShortName":null},{"areaName":"马鞍山市","areaCode":"340500","areaShortName":null},{"areaName":"淮北市","areaCode":"340600","areaShortName":null},{"areaName":"铜陵市","areaCode":"340700","areaShortName":null},{"areaName":"安庆市","areaCode":"340800","areaShortName":null},{"areaName":"黄山市","areaCode":"341000","areaShortName":null},{"areaName":"滁州市","areaCode":"341100","areaShortName":null},{"areaName":"阜阳市","areaCode":"341200","areaShortName":null},{"areaName":"宿州市","areaCode":"341300","areaShortName":null},{"areaName":"六安市","areaCode":"341500","areaShortName":null},{"areaName":"亳州市","areaCode":"341600","areaShortName":null},{"areaName":"池州市","areaCode":"341700","areaShortName":null},{"areaName":"宣城市","areaCode":"341800","areaShortName":null}]},{"areaName":"福建省","areaCode":"350000","areaShortName":null,"subAreas":[{"areaName":"福州市","areaCode":"350100","areaShortName":null},{"areaName":"厦门市","areaCode":"350200","areaShortName":null},{"areaName":"莆田市","areaCode":"350300","areaShortName":null},{"areaName":"三明市","areaCode":"350400","areaShortName":null},{"areaName":"泉州市","areaCode":"350500","areaShortName":null},{"areaName":"漳州市","areaCode":"350600","areaShortName":null},{"areaName":"南平市","areaCode":"350700","areaShortName":null},{"areaName":"龙岩市","areaCode":"350800","areaShortName":null},{"areaName":"宁德市","areaCode":"350900","areaShortName":null}]},{"areaName":"江西省","areaCode":"360000","areaShortName":null,"subAreas":[{"areaName":"南昌市","areaCode":"360100","areaShortName":null},{"areaName":"景德镇市","areaCode":"360200","areaShortName":null},{"areaName":"萍乡市","areaCode":"360300","areaShortName":null},{"areaName":"九江市","areaCode":"360400","areaShortName":null},{"areaName":"新余市","areaCode":"360500","areaShortName":null},{"areaName":"鹰潭市","areaCode":"360600","areaShortName":null},{"areaName":"赣州市","areaCode":"360700","areaShortName":null},{"areaName":"吉安市","areaCode":"360800","areaShortName":null},{"areaName":"宜春市","areaCode":"360900","areaShortName":null},{"areaName":"抚州市","areaCode":"361000","areaShortName":null},{"areaName":"上饶市","areaCode":"361100","areaShortName":null}]},{"areaName":"山东省","areaCode":"370000","areaShortName":null,"subAreas":[{"areaName":"济南市","areaCode":"370100","areaShortName":null},{"areaName":"青岛市","areaCode":"370200","areaShortName":null},{"areaName":"淄博市","areaCode":"370300","areaShortName":null},{"areaName":"枣庄市","areaCode":"370400","areaShortName":null},{"areaName":"东营市","areaCode":"370500","areaShortName":null},{"areaName":"烟台市","areaCode":"370600","areaShortName":null},{"areaName":"潍坊市","areaCode":"370700","areaShortName":null},{"areaName":"济宁市","areaCode":"370800","areaShortName":null},{"areaName":"泰安市","areaCode":"370900","areaShortName":null},{"areaName":"威海市","areaCode":"371000","areaShortName":null},{"areaName":"日照市","areaCode":"371100","areaShortName":null},{"areaName":"莱芜市","areaCode":"371200","areaShortName":null},{"areaName":"临沂市","areaCode":"371300","areaShortName":null},{"areaName":"德州市","areaCode":"371400","areaShortName":null},{"areaName":"聊城市","areaCode":"371500","areaShortName":null},{"areaName":"滨州市","areaCode":"371600","areaShortName":null},{"areaName":"菏泽市","areaCode":"371700","areaShortName":null}]},{"areaName":"河南省","areaCode":"410000","areaShortName":null,"subAreas":[{"areaName":"郑州市","areaCode":"410100","areaShortName":null},{"areaName":"开封市","areaCode":"410200","areaShortName":null},{"areaName":"洛阳市","areaCode":"410300","areaShortName":null},{"areaName":"平顶山市","areaCode":"410400","areaShortName":null},{"areaName":"安阳市","areaCode":"410500","areaShortName":null},{"areaName":"鹤壁市","areaCode":"410600","areaShortName":null},{"areaName":"新乡市","areaCode":"410700","areaShortName":null},{"areaName":"焦作市","areaCode":"410800","areaShortName":null},{"areaName":"濮阳市","areaCode":"410900","areaShortName":null},{"areaName":"许昌市","areaCode":"411000","areaShortName":null},{"areaName":"漯河市","areaCode":"411100","areaShortName":null},{"areaName":"三门峡市","areaCode":"411200","areaShortName":null},{"areaName":"南阳市","areaCode":"411300","areaShortName":null},{"areaName":"商丘市","areaCode":"411400","areaShortName":null},{"areaName":"信阳市","areaCode":"411500","areaShortName":null},{"areaName":"周口市","areaCode":"411600","areaShortName":null},{"areaName":"驻马店市","areaCode":"411700","areaShortName":null},{"areaName":"济源市","areaCode":"419001","areaShortName":null}]},{"areaName":"湖北省","areaCode":"420000","areaShortName":null,"subAreas":[{"areaName":"武汉市","areaCode":"420100","areaShortName":null},{"areaName":"黄石市","areaCode":"420200","areaShortName":null},{"areaName":"十堰市","areaCode":"420300","areaShortName":null},{"areaName":"宜昌市","areaCode":"420500","areaShortName":null},{"areaName":"襄阳市","areaCode":"420600","areaShortName":null},{"areaName":"鄂州市","areaCode":"420700","areaShortName":null},{"areaName":"荆门市","areaCode":"420800","areaShortName":null},{"areaName":"孝感市","areaCode":"420900","areaShortName":null},{"areaName":"荆州市","areaCode":"421000","areaShortName":null},{"areaName":"黄冈市","areaCode":"421100","areaShortName":null},{"areaName":"咸宁市","areaCode":"421200","areaShortName":null},{"areaName":"随州市","areaCode":"421300","areaShortName":null},{"areaName":"恩施土家族苗族自治州","areaCode":"422800","areaShortName":null},{"areaName":"仙桃市","areaCode":"429004","areaShortName":null},{"areaName":"潜江市","areaCode":"429005","areaShortName":null},{"areaName":"天门市","areaCode":"429006","areaShortName":null},{"areaName":"神农架林区","areaCode":"429021","areaShortName":null}]},{"areaName":"湖南省","areaCode":"430000","areaShortName":null,"subAreas":[{"areaName":"长沙市","areaCode":"430100","areaShortName":null},{"areaName":"株洲市","areaCode":"430200","areaShortName":null},{"areaName":"湘潭市","areaCode":"430300","areaShortName":null},{"areaName":"衡阳市","areaCode":"430400","areaShortName":null},{"areaName":"邵阳市","areaCode":"430500","areaShortName":null},{"areaName":"岳阳市","areaCode":"430600","areaShortName":null},{"areaName":"常德市","areaCode":"430700","areaShortName":null},{"areaName":"张家界市","areaCode":"430800","areaShortName":null},{"areaName":"益阳市","areaCode":"430900","areaShortName":null},{"areaName":"郴州市","areaCode":"431000","areaShortName":null},{"areaName":"永州市","areaCode":"431100","areaShortName":null},{"areaName":"怀化市","areaCode":"431200","areaShortName":null},{"areaName":"娄底市","areaCode":"431300","areaShortName":null},{"areaName":"湘西土家族苗族自治州","areaCode":"433100","areaShortName":null}]},{"areaName":"广东省","areaCode":"440000","areaShortName":null,"subAreas":[{"areaName":"广州市","areaCode":"440100","areaShortName":null},{"areaName":"韶关市","areaCode":"440200","areaShortName":null},{"areaName":"深圳市","areaCode":"440300","areaShortName":null},{"areaName":"珠海市","areaCode":"440400","areaShortName":null},{"areaName":"汕头市","areaCode":"440500","areaShortName":null},{"areaName":"佛山市","areaCode":"440600","areaShortName":null},{"areaName":"江门市","areaCode":"440700","areaShortName":null},{"areaName":"湛江市","areaCode":"440800","areaShortName":null},{"areaName":"茂名市","areaCode":"440900","areaShortName":null},{"areaName":"肇庆市","areaCode":"441200","areaShortName":null},{"areaName":"惠州市","areaCode":"441300","areaShortName":null},{"areaName":"梅州市","areaCode":"441400","areaShortName":null},{"areaName":"汕尾市","areaCode":"441500","areaShortName":null},{"areaName":"河源市","areaCode":"441600","areaShortName":null},{"areaName":"阳江市","areaCode":"441700","areaShortName":null},{"areaName":"清远市","areaCode":"441800","areaShortName":null},{"areaName":"东莞市","areaCode":"441900","areaShortName":null},{"areaName":"中山市","areaCode":"442000","areaShortName":null},{"areaName":"潮州市","areaCode":"445100","areaShortName":null},{"areaName":"揭阳市","areaCode":"445200","areaShortName":null},{"areaName":"云浮市","areaCode":"445300","areaShortName":null}]},{"areaName":"广西壮族自治区","areaCode":"450000","areaShortName":null,"subAreas":[{"areaName":"南宁市","areaCode":"450100","areaShortName":null},{"areaName":"柳州市","areaCode":"450200","areaShortName":null},{"areaName":"桂林市","areaCode":"450300","areaShortName":null},{"areaName":"梧州市","areaCode":"450400","areaShortName":null},{"areaName":"北海市","areaCode":"450500","areaShortName":null},{"areaName":"防城港市","areaCode":"450600","areaShortName":null},{"areaName":"钦州市","areaCode":"450700","areaShortName":null},{"areaName":"贵港市","areaCode":"450800","areaShortName":null},{"areaName":"玉林市","areaCode":"450900","areaShortName":null},{"areaName":"百色市","areaCode":"451000","areaShortName":null},{"areaName":"贺州市","areaCode":"451100","areaShortName":null},{"areaName":"河池市","areaCode":"451200","areaShortName":null},{"areaName":"来宾市","areaCode":"451300","areaShortName":null},{"areaName":"崇左市","areaCode":"451400","areaShortName":null}]},{"areaName":"海南省","areaCode":"460000","areaShortName":null,"subAreas":[{"areaName":"海口市","areaCode":"460100","areaShortName":null},{"areaName":"三亚市","areaCode":"460200","areaShortName":null},{"areaName":"三沙市","areaCode":"460300","areaShortName":null},{"areaName":"儋州市","areaCode":"460400","areaShortName":null},{"areaName":"五指山市","areaCode":"469001","areaShortName":null},{"areaName":"琼海市","areaCode":"469002","areaShortName":null},{"areaName":"文昌市","areaCode":"469005","areaShortName":null},{"areaName":"万宁市","areaCode":"469006","areaShortName":null},{"areaName":"东方市","areaCode":"469007","areaShortName":null},{"areaName":"定安县","areaCode":"469021","areaShortName":null},{"areaName":"屯昌县","areaCode":"469022","areaShortName":null},{"areaName":"澄迈县","areaCode":"469023","areaShortName":null},{"areaName":"临高县","areaCode":"469024","areaShortName":null},{"areaName":"白沙黎族自治县","areaCode":"469025","areaShortName":null},{"areaName":"昌江黎族自治县","areaCode":"469026","areaShortName":null},{"areaName":"乐东黎族自治县","areaCode":"469027","areaShortName":null},{"areaName":"陵水黎族自治县","areaCode":"469028","areaShortName":null},{"areaName":"保亭黎族苗族自治县","areaCode":"469029","areaShortName":null},{"areaName":"琼中黎族苗族自治县","areaCode":"469030","areaShortName":null}]},{"areaName":"重庆市","areaCode":"500000","areaShortName":null,"subAreas":[{"areaName":"万州区","areaCode":"500101","areaShortName":null},{"areaName":"涪陵区","areaCode":"500102","areaShortName":null},{"areaName":"渝中区","areaCode":"500103","areaShortName":null},{"areaName":"大渡口区","areaCode":"500104","areaShortName":null},{"areaName":"江北区","areaCode":"500105","areaShortName":null},{"areaName":"沙坪坝区","areaCode":"500106","areaShortName":null},{"areaName":"九龙坡区","areaCode":"500107","areaShortName":null},{"areaName":"南岸区","areaCode":"500108","areaShortName":null},{"areaName":"北碚区","areaCode":"500109","areaShortName":null},{"areaName":"綦江区","areaCode":"500110","areaShortName":null},{"areaName":"大足区","areaCode":"500111","areaShortName":null},{"areaName":"渝北区","areaCode":"500112","areaShortName":null},{"areaName":"巴南区","areaCode":"500113","areaShortName":null},{"areaName":"黔江区","areaCode":"500114","areaShortName":null},{"areaName":"长寿区","areaCode":"500115","areaShortName":null},{"areaName":"江津区","areaCode":"500116","areaShortName":null},{"areaName":"合川区","areaCode":"500117","areaShortName":null},{"areaName":"永川区","areaCode":"500118","areaShortName":null},{"areaName":"南川区","areaCode":"500119","areaShortName":null},{"areaName":"璧山区","areaCode":"500120","areaShortName":null},{"areaName":"铜梁区","areaCode":"500151","areaShortName":null},{"areaName":"潼南区","areaCode":"500152","areaShortName":null},{"areaName":"荣昌区","areaCode":"500153","areaShortName":null},{"areaName":"开州区","areaCode":"500154","areaShortName":null},{"areaName":"梁平县","areaCode":"500228","areaShortName":null},{"areaName":"城口县","areaCode":"500229","areaShortName":null},{"areaName":"丰都县","areaCode":"500230","areaShortName":null},{"areaName":"垫江县","areaCode":"500231","areaShortName":null},{"areaName":"武隆县","areaCode":"500232","areaShortName":null},{"areaName":"忠县","areaCode":"500233","areaShortName":null},{"areaName":"云阳县","areaCode":"500235","areaShortName":null},{"areaName":"奉节县","areaCode":"500236","areaShortName":null},{"areaName":"巫山县","areaCode":"500237","areaShortName":null},{"areaName":"巫溪县","areaCode":"500238","areaShortName":null},{"areaName":"石柱土家族自治县","areaCode":"500240","areaShortName":null},{"areaName":"秀山土家族苗族自治县","areaCode":"500241","areaShortName":null},{"areaName":"酉阳土家族苗族自治县","areaCode":"500242","areaShortName":null},{"areaName":"彭水苗族土家族自治县","areaCode":"500243","areaShortName":null}]},{"areaName":"四川省","areaCode":"510000","areaShortName":null,"subAreas":[{"areaName":"成都市","areaCode":"510100","areaShortName":null},{"areaName":"自贡市","areaCode":"510300","areaShortName":null},{"areaName":"攀枝花市","areaCode":"510400","areaShortName":null},{"areaName":"泸州市","areaCode":"510500","areaShortName":null},{"areaName":"德阳市","areaCode":"510600","areaShortName":null},{"areaName":"绵阳市","areaCode":"510700","areaShortName":null},{"areaName":"广元市","areaCode":"510800","areaShortName":null},{"areaName":"遂宁市","areaCode":"510900","areaShortName":null},{"areaName":"内江市","areaCode":"511000","areaShortName":null},{"areaName":"乐山市","areaCode":"511100","areaShortName":null},{"areaName":"南充市","areaCode":"511300","areaShortName":null},{"areaName":"眉山市","areaCode":"511400","areaShortName":null},{"areaName":"宜宾市","areaCode":"511500","areaShortName":null},{"areaName":"广安市","areaCode":"511600","areaShortName":null},{"areaName":"达州市","areaCode":"511700","areaShortName":null},{"areaName":"雅安市","areaCode":"511800","areaShortName":null},{"areaName":"巴中市","areaCode":"511900","areaShortName":null},{"areaName":"资阳市","areaCode":"512000","areaShortName":null},{"areaName":"阿坝藏族羌族自治州","areaCode":"513200","areaShortName":null},{"areaName":"甘孜藏族自治州","areaCode":"513300","areaShortName":null},{"areaName":"凉山彝族自治州","areaCode":"513400","areaShortName":null}]},{"areaName":"贵州省","areaCode":"520000","areaShortName":null,"subAreas":[{"areaName":"贵阳市","areaCode":"520100","areaShortName":null},{"areaName":"六盘水市","areaCode":"520200","areaShortName":null},{"areaName":"遵义市","areaCode":"520300","areaShortName":null},{"areaName":"安顺市","areaCode":"520400","areaShortName":null},{"areaName":"毕节市","areaCode":"520500","areaShortName":null},{"areaName":"铜仁市","areaCode":"520600","areaShortName":null},{"areaName":"黔西南布依族苗族自治州","areaCode":"522300","areaShortName":null},{"areaName":"黔东南苗族侗族自治州","areaCode":"522600","areaShortName":null},{"areaName":"黔南布依族苗族自治州","areaCode":"522700","areaShortName":null}]},{"areaName":"云南省","areaCode":"530000","areaShortName":null,"subAreas":[{"areaName":"昆明市","areaCode":"530100","areaShortName":null},{"areaName":"曲靖市","areaCode":"530300","areaShortName":null},{"areaName":"玉溪市","areaCode":"530400","areaShortName":null},{"areaName":"保山市","areaCode":"530500","areaShortName":null},{"areaName":"昭通市","areaCode":"530600","areaShortName":null},{"areaName":"丽江市","areaCode":"530700","areaShortName":null},{"areaName":"普洱市","areaCode":"530800","areaShortName":null},{"areaName":"临沧市","areaCode":"530900","areaShortName":null},{"areaName":"楚雄彝族自治州","areaCode":"532300","areaShortName":null},{"areaName":"红河哈尼族彝族自治州","areaCode":"532500","areaShortName":null},{"areaName":"文山壮族苗族自治州","areaCode":"532600","areaShortName":null},{"areaName":"西双版纳傣族自治州","areaCode":"532800","areaShortName":null},{"areaName":"大理白族自治州","areaCode":"532900","areaShortName":null},{"areaName":"德宏傣族景颇族自治州","areaCode":"533100","areaShortName":null},{"areaName":"怒江傈僳族自治州","areaCode":"533300","areaShortName":null},{"areaName":"迪庆藏族自治州","areaCode":"533400","areaShortName":null}]},{"areaName":"西藏自治区","areaCode":"540000","areaShortName":null,"subAreas":[{"areaName":"拉萨市","areaCode":"540100","areaShortName":null},{"areaName":"日喀则市","areaCode":"540200","areaShortName":null},{"areaName":"昌都市","areaCode":"540300","areaShortName":null},{"areaName":"林芝市","areaCode":"540400","areaShortName":null},{"areaName":"山南市","areaCode":"540500","areaShortName":null},{"areaName":"那曲地区","areaCode":"542400","areaShortName":null},{"areaName":"阿里地区","areaCode":"542500","areaShortName":null}]},{"areaName":"陕西省","areaCode":"610000","areaShortName":null,"subAreas":[{"areaName":"西安市","areaCode":"610100","areaShortName":null},{"areaName":"铜川市","areaCode":"610200","areaShortName":null},{"areaName":"宝鸡市","areaCode":"610300","areaShortName":null},{"areaName":"咸阳市","areaCode":"610400","areaShortName":null},{"areaName":"渭南市","areaCode":"610500","areaShortName":null},{"areaName":"延安市","areaCode":"610600","areaShortName":null},{"areaName":"汉中市","areaCode":"610700","areaShortName":null},{"areaName":"榆林市","areaCode":"610800","areaShortName":null},{"areaName":"安康市","areaCode":"610900","areaShortName":null},{"areaName":"商洛市","areaCode":"611000","areaShortName":null}]},{"areaName":"甘肃省","areaCode":"620000","areaShortName":null,"subAreas":[{"areaName":"兰州市","areaCode":"620100","areaShortName":null},{"areaName":"嘉峪关市","areaCode":"620200","areaShortName":null},{"areaName":"金昌市","areaCode":"620300","areaShortName":null},{"areaName":"白银市","areaCode":"620400","areaShortName":null},{"areaName":"天水市","areaCode":"620500","areaShortName":null},{"areaName":"武威市","areaCode":"620600","areaShortName":null},{"areaName":"张掖市","areaCode":"620700","areaShortName":null},{"areaName":"平凉市","areaCode":"620800","areaShortName":null},{"areaName":"酒泉市","areaCode":"620900","areaShortName":null},{"areaName":"庆阳市","areaCode":"621000","areaShortName":null},{"areaName":"定西市","areaCode":"621100","areaShortName":null},{"areaName":"陇南市","areaCode":"621200","areaShortName":null},{"areaName":"临夏回族自治州","areaCode":"622900","areaShortName":null},{"areaName":"甘南藏族自治州","areaCode":"623000","areaShortName":null}]},{"areaName":"青海省","areaCode":"630000","areaShortName":null,"subAreas":[{"areaName":"西宁市","areaCode":"630100","areaShortName":null},{"areaName":"海东市","areaCode":"630200","areaShortName":null},{"areaName":"海北藏族自治州","areaCode":"632200","areaShortName":null},{"areaName":"黄南藏族自治州","areaCode":"632300","areaShortName":null},{"areaName":"海南藏族自治州","areaCode":"632500","areaShortName":null},{"areaName":"果洛藏族自治州","areaCode":"632600","areaShortName":null},{"areaName":"玉树藏族自治州","areaCode":"632700","areaShortName":null},{"areaName":"海西蒙古族藏族自治州","areaCode":"632800","areaShortName":null}]},{"areaName":"宁夏回族自治区","areaCode":"640000","areaShortName":null,"subAreas":[{"areaName":"银川市","areaCode":"640100","areaShortName":null},{"areaName":"石嘴山市","areaCode":"640200","areaShortName":null},{"areaName":"吴忠市","areaCode":"640300","areaShortName":null},{"areaName":"固原市","areaCode":"640400","areaShortName":null},{"areaName":"中卫市","areaCode":"640500","areaShortName":null}]},{"areaName":"新疆维吾尔自治区","areaCode":"650000","areaShortName":null,"subAreas":[{"areaName":"乌鲁木齐市","areaCode":"650100","areaShortName":null},{"areaName":"克拉玛依市","areaCode":"650200","areaShortName":null},{"areaName":"吐鲁番市","areaCode":"650400","areaShortName":null},{"areaName":"哈密市","areaCode":"650500","areaShortName":null},{"areaName":"昌吉回族自治州","areaCode":"652300","areaShortName":null},{"areaName":"博尔塔拉蒙古自治州","areaCode":"652700","areaShortName":null},{"areaName":"巴音郭楞蒙古自治州","areaCode":"652800","areaShortName":null},{"areaName":"阿克苏地区","areaCode":"652900","areaShortName":null},{"areaName":"克孜勒苏柯尔克孜自治州","areaCode":"653000","areaShortName":null},{"areaName":"喀什地区","areaCode":"653100","areaShortName":null},{"areaName":"和田地区","areaCode":"653200","areaShortName":null},{"areaName":"伊犁哈萨克自治州","areaCode":"654000","areaShortName":null},{"areaName":"塔城地区","areaCode":"654200","areaShortName":null},{"areaName":"阿勒泰地区","areaCode":"654300","areaShortName":null},{"areaName":"自治区直辖县级行政区划","areaCode":"659000","areaShortName":null}]},{"areaName":"台湾省","areaCode":"710000","areaShortName":null,"subAreas":[]},{"areaName":"香港特别行政区","areaCode":"810000","areaShortName":null,"subAreas":[]},{"areaName":"澳门特别行政区","areaCode":"820000","areaShortName":null,"subAreas":[]}]
	,"demo":[{name:"不限",id:"p-0",list:[]},{name:"X类",id:"p-1",list:[{name:"钢材",id:"c-10",list:[{name:"普钢",id:"q-100",list:[]},{name:"不锈钢",id:"q-101",list:[]}]}]},{name:"Y类",id:"p-2",list:[{name:"木料",id:"c-20",list:[{name:"檀木",id:"q-200",list:[]},{name:"杉木",id:"q-201",list:[]},{name:"梨木",id:"q-202",list:[]}]},{name:"其他",id:"c-21",list:[{name:"沙",id:"q-210",list:[]},{name:"水泥",id:"q-211",list:[]},{name:"地板砖",id:"q-212",list:[]},{name:"瓷砖",id:"q-213",list:[]}]}]}]};
  return public_configs[_name];
}
/*
 *+++++++++++++++++++++++++++++++++++++以下combSelector 地区/产品分类级联选择，用于固定数据情形+++++++++++++++++++++++++++++++++++++++++++++
 */
function UseCombSelector(root){
  'use strict';
  /*
   *构造级联选择器combSelector开始
   */
  function combSelector(_textField,_valueField,_childsField){
    if(_textField)this.textField=_textField;
    else this.textField='name';
    if(_valueField)this.valueField=_valueField;
    else this.valueField='id';
    if(_childsField)this.childsField=_childsField;
    else this.childsField='list';
  }
  combSelector.prototype.initOptions=function($select,list,lev){
    //$select为select控件，list为数组
    $select.html("");
    //隐藏本身及之后的全部控件
    for(var e=lev;e<this.jqElements.length;e++){
      this.jqElements[e].hide();
    }
    if(!list||list.length==0)return;
    for(var i=0;i<list.length;i++){
      $select.append("<option value='"+list[i][this.valueField]+"'>"+list[i][this.textField]+"</option>");
    }
    $select.show();
    if(lev<this.jqElements.length-1)
    {
      this.lists[lev+1]=list[$select.get(0).selectedIndex][this.childsField];
      this.initOptions(this.jqElements[lev+1],this.lists[lev+1],lev+1);
      if(this.lists[lev+1]&&this.lists[lev+1].length>0)
        this.jqElements[lev+1].show();
      else
        this.jqElements[lev+1].hide();
    }
  };
  /*初始化级联选择器，参数说明如下
   *_jqElements 由DOM页面上的多个select控件组成的一个数组
   *_allData 完整的层级json数据
   */
  combSelector.prototype.init=function(_jqElements,dataconfig){
    this.lists=[];
    var allData;
    if(dataconfig==undefined||dataconfig==''||dataconfig==null)
      allData=getStaticData('areas');
    else
      allData=getStaticData(dataconfig);
    this.lists.push(allData);
    this.jqElements=_jqElements;
    //生成一级下拉列表
    this.initOptions(this.jqElements[0],this.lists[0],0);
    if(this.jqElements.length>1){
      var that=this;
      for(var i=0;i<that.jqElements.length;i++){
        if(i!=0)that.lists.push([]);
        var el=that.jqElements[i];
        el.attr('index',i);
        //为控件组分别添加change事件，最后一个控件不添加
        if(i<that.jqElements.length-1){
          el.on("change",function(){
            var _index=parseInt($(this).attr('index'));
            that.lists[_index+1]=that.lists[_index][$(this).get(0).selectedIndex][that.childsField];
            that.initOptions(that.jqElements[_index+1],that.lists[_index+1],_index+1);
            if(that.lists[_index+1]&&that.lists[_index+1].length>0)
              that.jqElements[_index+1].show();
            else
              that.jqElements[_index+1].hide();
          });
        }

      }

    }
  };
  //为级联选择器设置已选项，数据可能由后台获得
  combSelector.prototype.setSelected=function(ids){
    function loadSelected(_that,_id,_list,lev){
      _that.jqElements[lev].val(_id);
      if(lev<_that.jqElements.length-1){
        for(var i=0;i<_list.length;i++){
          if(_id==_list[i][_that.valueField]){
            _that.lists[lev+1]=_list[i][_that.childsField];
            break;
          }
        }
        _that.initOptions(_that.jqElements[lev+1],_that.lists[lev+1],lev+1);
        if(_that.lists[lev+1]&&_that.lists[lev+1].length>0)_that.jqElements[lev+1].show();
      }
    };
    for(var i=0;i<ids.length;i++){
      var id=ids[i];
      loadSelected(this,id,this.lists[i],i);
    }
  };
  //返回当前选择结果
  combSelector.prototype.getSelected=function(){
    var ids=[];
    for(var i=0;i<this.jqElements.length;i++)
      ids.push(this.jqElements[i].val());
    return ids;
  };
  root.COMBSelector=new combSelector();
  //外部创建对象开放入口
  root.combSelectorFactory=function(field1,field2,field3){
    return new combSelector(field1,field2,field3);
  }
}

/*
 *+++++++++++++++++++++++++++++++++++++以下ajaxSelector 产品/品种分类级联选择，用于异步数据情形+++++++++++++++++++++++++++++++++++++++++++++
 */
function UserAjaxSelector(root){
  //构造级联选择器开始
  function ajaxSelector(){
    var $line=$('body');
    var lists=[],submitAreas='';
    var jqElements,selectAreas;
    var showField='name',valueField='id';
    var depth=5;
    var outAjaxHandler=function(item){
      console.log('----ajax request');
    }
    var $selects=[];
  }
  /*
   *参数配置示例
   *var  _opt={
   el:$("#selects"),
   showField:'name',
   valueField:'id',
   depth:5,
   list0:list,
   outAjaxHandler:myAjaxHandler
   }
   */
  ajaxSelector.prototype.setConfig=function(_opt){
    if(_opt.el)this.$line=_opt.el;
    if(_opt.showField)this.showField=_opt.showField;
    if(_opt.valueField)this.valueField=_opt.valueField;
    if(_opt.depth)this.depth=_opt.depth;
    this.lists=[];
    if(typeof(_opt.list0)=='object')this.lists.push(_opt.list0);
    if(typeof(_opt.outAjaxHandler)=='function')
      this.outAjaxHandler=_opt.outAjaxHandler;
    this.setOptions(0,this.lists[0]);
  }
  ajaxSelector.prototype.setOptions=function(lev,list){
    if(lev>=this.depth)return;
    if($("#"+this.$line.attr('id')+'_'+lev).length==0){
      this.$line.append("<select id='"+this.$line.attr('id')+"_"+lev+"' index='"+lev+"'></select>");
      var that=this;
      $("#"+this.$line.attr('id')+"_"+lev).change(function(){
        var _index=parseInt($(this).attr('index'));
        that.clearOptions(_index+1);
        that.outAjaxHandler({text:$(this).find("option:selected").text(),value:$(this).val(),index:_index});
      });
    }
    else{
      $("#ajselect_"+lev).html('');
    }
    if(list&&list.length>0){
      var opts='';
      for(var i=0;i<list.length;i++){
        opts+="<option json-data='"+JSON.stringify(list[i])+"' value='"+list[i][this.valueField]+"'>"+list[i][this.showField]+"</option>";
      }
      $("#"+this.$line.attr('id')+"_"+lev).show();
      $("#"+this.$line.attr('id')+"_"+lev).append(opts);
	  $("#"+this.$line.attr('id')+"_"+lev).change();
    }
    else $("#"+this.$line.attr('id')+"_"+lev).hide();

  };
  ajaxSelector.prototype.clearOptions=function(lev){
    for(var i=lev;i<this.depth;i++){
      var $select=$("#"+this.$line.attr('id')+'_'+i);
      if($select){

        $select.html('');$select.hide()
      }
    }
  };
  //为级联选择器设置已选项，数据可能由后台获得
  ajaxSelector.prototype.setSelected=function(ids){};
  //返回当前选择结果
  ajaxSelector.prototype.getSelected=function(){
    var re=':';
    var $select0=$("#"+this.$line.attr('id')+'_0');
    if($select0.length>0)re=$select0.val()+re;
    var $select;
    for(var i=1;i<this.depth;i++){
      var $temp=$("#"+this.$line.attr('id')+'_'+i);
      if(!$temp||$temp.html()=='')break;
      $select=$temp;
    }
    if($select.length>0)re=re+$select.val();
    return re;
  };
  root.AjaxSelector=new ajaxSelector();
  //外部创建入口
  root.ajaxSelectorFactory=function(){
    return new ajaxSelector();
  }
}

/*
 *+++++++++++++++++++++++++++以下watchingSelector 用户搜索下拉选择，用于用户边输入边搜索的下拉列表生成+++++++++++++++++++++++
 *如 建筑公司查找、公司下项目查找等场景
 */
function UseWatchingSelector(root){
  //开始构建组件
  function watchingSelector(_opt){
	this.last='';
	this.clickSetting='yes';
	/*begin read options*/
    this.$input=_opt.watchingInput;
    this.textField=_opt.textField;
    this.valueField=_opt.valueField;
    this.$hidden=_opt.saveValueAt;
	if(typeof(_opt.ajaxUrl)=='object'){
		this.jdata=_opt.ajaxUrl;
		this.url='';
	}
	else{
		this.url=_opt.ajaxUrl;
		this.jdata=null;
	}
    this.params=_opt.ajaxParams;
	if(_opt.width)this.ulWidth=_opt.width;
	else this.ulWidth=0;
	if(_opt.clickSetting)this.clickSetting=_opt.clickSetting;
	/*read end*/
    this.id='flt_'+this.$input.attr('id');
    this.offset =this.$input.position();
    this.$input.after("<div class='cmn-watch-list' id='"+this.id+"'></div>");
	var _width;
	if(this.ulWidth==0)_width=this.$input.width()+42;
	else _width=this.ulWidth;
    $("#"+this.id).css({
      top: this.offset.top + 34,
      left: this.offset.left+10,
      width:_width
    }).hide();
    var that=this;
	for(var p=0;p<this.params.length;p++){
	  if(typeof(this.params[p].paramValue)=='object'){
		if(this.params[p].paramValue.attr('id')!=this.$input.attr('id')){
		  this.params[p].paramValue.on('change',function(){
		  that.$input.val('');
		  that.$hidden.val('').change();
		  console.log("++++++++++++envents++++++++++++++")
		  console.log($(this).attr('id'));
		  });
		}
	  }
	 }
    this.$input.on('keyup click mouseout',function(){
	  var txt = $(this).val();
	  if(event.type=='mouseout'&&txt==that.last)
		  return;
	  that.last=txt;
	  if(event.type=='click'&&that.clickSetting!='yes'&&txt=='')
		return;
      that.goAjax();
      event.stopPropagation();
    });
	/*
	*手动清空input中内容时，同时要清空隐藏域的值
	*/
	this.$input.on('change',function(){
		if($(this).val()==''){
			if(that.$hidden){
			that.$hidden.val('');
			}
		}
	});
    $("#"+this.id).click(function(event){
      $("#"+that.id).hide();
      event.stopPropagation();
    });
    $(document).click(function(){
      $("#"+that.id).hide();
    });

  }
  /*
   *直接设置json数据
   */
  watchingSelector.prototype.setData=function(_jdata){
	  this.jdata=_jdata;
  };
  /*
   *异步请求查询数据
   */
  watchingSelector.prototype.goAjax=function(){
    var that=this;
	var oList=[];
	if(!this.jdata){
		var _url=this.url;
		for(var p=0;p<this.params.length;p++){
		  var _value='';
		  if(typeof(this.params[p].paramValue)=='object')
			_value=this.params[p].paramValue.val();
		  else
			_value=this.params[p].paramValue;
		  if(_value!='')
		  _url+='&'+this.params[p].paramName+'='+_value;
		}
		_url=_url.replace('&','?');
		/*$.ajax({
		  type:'get',
		  url:_url,
		  dataType:"json",
		  success:function(data){
			oList=data.data;
			that.render(oList);
		  }
		});
		*/
		$.ajax(tokenAjax({
		  type:'get',
		  url:_url,
		  dataType:"json",
		  success:function(data){
			oList=data.data;
			that.render(oList);
		  }
		}));
		
	}
	else{
		oList=[];
		var searchTxt='';
		for(var p=0;p<this.params.length;p++){
			if(typeof(this.params[p].paramValue)=='object'){
				searchTxt=this.params[p].paramValue.val();
				break;
			}
		}
		if(searchTxt==''){
		  oList=this.jdata;

		}
		else{
		  for(var j=0;j<this.jdata.length;j++){
			if(this.jdata[j][this.textField].indexOf(searchTxt)>-1){
			  oList.push(this.jdata[j]);
			}
		  }
		}
		this.render(oList);
	}
    
  };
  /*
   *生成html
   */
  watchingSelector.prototype.render=function(oList){
    var oHtml='';var that=this;
	if(!oList||typeof(oList)!='object'||oList.length==0){
		$("#"+that.id).html('');
		$("#"+that.id).hide();
		return;
	}
    if(oList&&oList.length>0){
      oHtml+='<ul class="cmn-watch-wrapper">';
      for(var i=0;i<oList.length;i++){
        oHtml+="<li item-data='"+oList[i][this.valueField]+"'>"+oList[i][this.textField]+"</li>";
      }
      oHtml+='</ul>';
      $("#"+that.id).html(oHtml);
      $("#"+that.id).show();
      var that=this;
      $('.cmn-watch-wrapper li').click(function(){
        var oValue=$(this).attr("item-data");
        var oName=$(this).text();
        that.$input.val(oName);
		that.last=oName;
        that.$hidden.attr("data-name",that.valueField);
        that.$hidden.val(oValue).change();
        $("#"+that.id).hide();
        event.stopPropagation();
        return;
      });
    }else{
      $("#"+that.id).html(oHtml);
      $("#"+that.id).hide();
    }
  };
  /*
   *传入参数示例
   *var options={
   *	watchingInput:$("#myinput"),
   *	textField:"companyName",
   *	valueField:"companyCode",
   *	saveValueAt:$("#myhidden"),
   *	ajaxUrl:'http://192.168.59.3:8082/console/condition/company',
   *	ajaxParams:[{paramName:"companyName",paramValue:$("#myinput")},{paramName:"others",paramValue:''}]
   *}
   *
   */
  root.addWatchInputing=function(opt){
    return new watchingSelector(opt);
  };
}
/*
 *+++++++++++++++++++++++++++以下levMenus 非zui 一次性完整数据生成多层级浮动菜单+++++++++++++++++++++++
 *
 */
function UseLevMenus(root){
  var reObj={name:'',id:''};
  //构造级联选择器开始
  function levMenus(_opt){
	this.id='';
    this.temp=0;
    this.isCategory=0;
	this.tops=[0,0,0,0];
	this.dict=new Array();
	this.dict['1']={};
    if(_opt){
		if(_opt.mWidth)this.mWidth=_opt.mWidth;
		else this.mWidth=200;
		if(_opt.mHeight)this.mHeight=_opt.mHeight;
		else this.mHeight=22;
		if(_opt.offX)this.offX=_opt.offX;
		else this.offX=0;
		if(_opt.offY)this.offY=_opt.offY;
		else this.offY=0;
		if(_opt.menuClass)this.menuClass=_opt.menuClass;
		else this.menuClass='';
		if(this.menuClass=='')
			this.menuStyle="width:100%;height:"+this.mHeight+"px;line-height:"+this.mHeight+"px;text-align:center;";
		else
			this.menuStyle='';
	}
  }
  levMenus.prototype.initMenus=function($_box,list,liLev,pad,_dep){
    //生成lev级菜单，$box为菜单盒子，list为数组
    $_box.html("");
    if(!list||list.length==0){
      $_box.hide();
      return;
    }
    var dep=_dep;
    var _textField=this.textField;
    var _valueField=this.valueField;
    var _childsField=this.childsField;
    if(this.temp==1&&dep==0){
      _textField='supername';
      _valueField='superid';
      _childsField='list';
    }
	$_box.append("<ul style='border:solid 1px #eee;'></ul>");
	
	var $ul=$_box.find('ul');
	var menuText='';
	var curLi;
    for(var i=0;i<list.length;i++){
		if(this.temp==1&& list[i].isleaf=="1")
			menuText=list[i][_textField]+"; 单位 :"+list[i].unit;
		else if(this.isCategory==1&&list[i].isleaf=="1")
			menuText=list[i][_textField]+"; 单位 :"+list[i].unit;
		else
			menuText=list[i][_textField];
	  this.dict[list[i][_valueField]]=list[i];
      $ul.append("<li style='"+this.menuStyle+"' class='"+this.menuClass+"' data-value='menu_"+list[i][_valueField]
      +"'><a href='javascript:void(0);' class='comb_m'>"+menuText+"</a></li>");
	  
      curLi=$_box.find("li:last");
	  if(list[i][_childsField]&&list[i][_childsField].length>0)
		curLi.addClass('lev-notleaf');
	  this.menuEvents(curLi,liLev,list[i][_childsField],i,pad,dep+1);
    }
  };
  levMenus.prototype.menuEvents=function($clickItem,clickLev,list,index,pad,dep){
	  var lev=parseInt(clickLev);
	  var that=this;
	  $clickItem.on('mouseover click',function(){
		if(event.type=='mouseover')
		{
			if($clickItem==that.$menuBox){
				event.stopPropagation();
				return;
			}
		}
		  var $targetMenu=that.$containor.find('.dep-'+(lev+1));
		  if($targetMenu.length>0){
			$targetMenu.html('');
		  }
		  else{
			that.$containor.append("<div class='lev-menu dep-"+(lev+1)+"' style='width:"+that.mWidth+"px;float:left;'></div>");
			$targetMenu=that.$containor.find('.dep-'+(lev+1));
		  }
		  $targetMenu.fadeIn();
		that.$containor.find('.lev-menu').hide();
		for(var i=1;i<=lev+1;i++){
			that.$containor.find('.lev-menu.dep-'+i).show();
		}
        if(!list||list.length==0){
		  if(lev>0){
			if(event.type=='click'){
				var _str=$(this).attr('data-value').split('_');
				var _len=_str.length;
				that.setSelected(_str[_len-1],$(this).find('a').text());
				if(that.$menuBox.is('input')){
					that.$menuBox.val($(this).find('a').text());
				}else{
					that.$menuBox.text($(this).find('a').text());
				}
				that.$containor.find('.lev-menu').hide();
				if(that.clickBack)that.clickBack();
			}
		  }
        }
		else{
          var scroll=$(this).parent().scrollTop();
		  var rootPad=0;
		  if(lev>1)
			rootPad=that.tops[lev-2];
		  that.tops[lev-1]=rootPad+that.mHeight*index-scroll;
		  $targetMenu.css({"padding-top": that.tops[lev-1]});
          //子菜单
		  that.initMenus($targetMenu,list,lev+1,(index+pad),dep);
		}
		event.stopPropagation();  
	  });
  }
  /*初始化级联选择器，参数说明如下
   *_jqElements 由DOM页面上的多个select控件组成的一个数组
   *_allData 完整的层级json数据
   */
  levMenus.prototype.render=function($_menuBox,_allData,_textField,_valueField,_childsField,_text0){
    this.$menuBox=$_menuBox;
	this.$menuBox.css({"cursor": 'pointer'});
    this.allData=_allData;
    this.textField=_textField;
    this.valueField=_valueField;
    this.childsField=_childsField;
    if(_text0)dftText=_text0;
    else dftText='--请选择--';
	if(this.$menuBox.is('input')){
		this.$menuBox.val(dftText);
	}else{
		this.$menuBox.text(dftText);
	}
    //生成菜单盒子
	if(this.$menuBox.attr('id'))
		this.id=this.$menuBox.attr('id')+"_"+new Date().getTime();
	else
		this.id="_"+new Date().getTime();
    this.$menuBox.after("<div class='lev-containor' id='"+this.id+"' style='position:absolute;min-width:200px;'></div>");
	this.$containor=$("#"+this.id);
	this.offset =this.$menuBox.position();
      this.$containor.css({
        top: this.offset.top + this.offY,
        left: this.offset.left+this.offX
      }).show();
    //生成一级菜单
    //var ul0=this.$menuBox.find("ul");
    this.menuEvents(this.$menuBox,0,this.allData,0,0,0);
	$(document).click(function(){
      $(".lev-menu").hide();
    });
  };
  //为材料分类菜单设置区分参数
  levMenus.prototype.isCategorySet=function(type){
    this.isCategory=type;
  };
  //为材料分类菜单设置区分参数
  levMenus.prototype.setTempType=function(type){
    this.temp=type;
  };
  //设置点击回调函数
  levMenus.prototype.setClickBack=function(fun){
	this.clickBack=fun;
  };
  //为级联选择器设置已选项，数据可能由后台获得
  levMenus.prototype.setSelected=function(id,name){
    reObj.id=id;
    reObj.name=name;
    //this.$menuBox.find('button').text(name);
    this.$menuBox.attr('data-name',id);
  };
  //返回当前选择结果
  levMenus.prototype.getSelected=function($box){
    if(!$box)
      return reObj;
    else{
      return this.dict[$box.attr('data-name')];
    }
  };
  root.LevMenus=new levMenus();
  //外部创建对象入口
  root.levMenusFactory=function(_opt){
    return new levMenus(_opt);
  }
}; 
/*
 *+++++++++++++++++++++++++++以下combMenus 基于zui框架 一次性完整数据生成多层级浮动菜单+++++++++++++++++++++++
 *
 */
function UseCombMenus(root){
  'use strict';
  var allData;
  var textField,valueField;
  var reObj={name:'',id:''};
  var textField,valueField,childsField;
  var dftText;
  var temp=0;
  //构造级联选择器开始
  function combMenus(){}
  
  combMenus.prototype.initSubMenus=function($_box,list,_dep){
    //生成lev级菜单，$box为父级盒子，list为数组
	var dep=_dep;
    $_box.html("");
    if(!list||list.length==0){
      $_box.remove();
      return;
    }
	$_box.parent().addClass("-box");
    for(var i=0;i<list.length;i++){
		if(temp==1&& list[i].isleaf=="1"){
		   $_box.append("<li class='dropdown-submenu' data-value='menu_"+list[i][valueField]
        +"'><a href='javascript:void(0);' class='comb_m'>"+list[i][textField]+"; 单位 :"+list[i].unit+"</a></li>");
      }else{
         $_box.append("<li class='dropdown-submenu' data-value='menu_"+list[i][valueField]
        +"'><a href='javascript:void(0);' class='comb_m'>"+list[i][textField]+"</a></li>");
      }
     
      var that=this;
      var curLi=$_box.find("li.dropdown-submenu:last");
      curLi.on('click',function(){
        var _str=$(this).attr('data-value').split('_');
        var _len=_str.length;

        if($(this).find('ul').length==0||$(this).find('ul').html()==''){
          that.setSelected(_str[_len-1],$(this).find('a').text());
          if(that.clickBack){
            that.clickBack();
          }
        }
		event.stopPropagation();
      });
      curLi.append("<ul class='dropdown-menu'></ul>");
      var curUl=curLi.find("ul");
      this.initSubMenus(curUl,list[i][childsField],dep+1);
    }
  };
  /*初始化级联选择器，参数说明如下
   *_jqElements 由DOM页面上的多个select控件组成的一个数组
   *_allData 完整的层级json数据
   */
  combMenus.prototype.render=function($_menuBox,_allData,_textField,_valueField,_childsField,_text0){
    this.$menuBox=$_menuBox;
    allData=_allData;
    if(_textField)textField=_textField;
    else textField='name';
    if(_valueField)valueField=_valueField;
    else valueField='id';
    if(_childsField)childsField=_childsField;
    else childsField='list';
    if(_text0)dftText=_text0;
    else dftText='--请选择--';
    //生成菜单盒子
    this.$menuBox.append("<div class='dropdown'><button class='btn' type='button' data-toggle='dropdown'>"+dftText
      +"<span class='caret'></span></button><ul class='dropdown-menu' id='box_lev_0'></ul></div>");
    //生成一级菜单
    var ul0=this.$menuBox.find("ul");
    this.initSubMenus(ul0,allData,0);
	$(document).click(function(){
      $(".lev-menu").hide();
    });
  };
  //为材料分类菜单设置区分参数
  combMenus.prototype.isCategorySet=function(type){
    temp=type;
  };
  //设置点击回调函数
  combMenus.prototype.setClickBack=function(fun){
	this.clickBack=fun;
  };
  //为级联选择器设置已选项，数据可能由后台获得
  combMenus.prototype.setSelected=function(id,name){
    reObj.id=id;
    reObj.name=name;
    this.$menuBox.find('button').text(name);
    //this.$menuBox.find('button').attr('id',id);
    this.$menuBox.attr('data-name',id);
  };
  //返回当前选择结果
  combMenus.prototype.getSelected=function($box){
    if(!$box)
      return reObj;
    else{
      return {id:$box.attr('data-id'),name:$box.find('button').text()};
    }
  };
  root.COMBMenus=new combMenus();
  //外部创建对象入口
  root.combMenusFactory=function(){
    return new combMenus();
  }
};
/*
 *+++++++++++++++++++++++++++以下qhLayer 弹窗+++++++++++++++++++++++
 *
 */
function UseLayer(root){
  'use strict';
  var $blackLayer=null;
  //构造级联选择器开始
  function qhLayer(_opt){
	  if(_opt.target)this.$target=_opt.target;
	  else this.$target=null;
	  if(_opt.name)this.code=_opt.name;
	  else this.code='';
	  if(_opt.width)this.htmWidth=_opt.width;
	  else this.htmWidth=100;
	  if(_opt.height)this.htmHeight=_opt.height;
	  else this.htmHeight=100;
	  if(_opt.closeHandler)this.closeHandler=_opt.closeHandler;
	  else this.closeHandler=function(){return true;};
	  if(_opt.saveHandler)this.saveHandler=_opt.saveHandler;
	  else this.saveHandler=function(){return true;};
	  if(_opt.cancelHandler)this.cancelHandler=_opt.cancelHandler;
	  else this.this.cancelHandler=function(){return true;};
	  if(!$blackLayer){
		  $("body").append("<div class='-layer cmn-blacklayer' style='position:absolute;width:100%;height:3000px;bottom:0;right:0;background-color:#000;opacity:.5;z-index:3000;display:none;'></div>");
		  $blackLayer=$(".cmn-blacklayer");
	  }
	  $("body").append("<div class='-layer cmn-layer-box' id='layer_"+this.code+"' style='position:absolute;width:"+this.htmlWith+"px;height:"+this.htmHeight
	  +"px;box-sizing:border-box;top: 50%;left: 50%;margin:-"+this.htmHeight/2+"px;-"+this.htmWidth/2+"px;z-index:3001;display:none;'></div>");
	  $("#layer_"+this.code).append(this.$target.remove());
	  var that=this;
	  this.$target.find('.layer-close').on('click',function(){
		  if(that.closeHandler())
		  $('.-layer').fadeOut();
	  });
	  this.$target.find('.layer-cancel').on('click',function(){
		  if(that.cancelHandler())
		  $('.-layer').fadeOut();
	  });
	  this.$target.find('.layer-save').on('click',function(){
		  if(that.saveHandler())
		  $('.-layer').fadeOut();
	  });
  }
  //打开显示layer
  qhLayer.prototype.open=function(){
	  $blackLayer.fadeIn();
	  $("#layer_"+this.code).fadeIn();
  }
  //close
  qhLayer.prototype.close=function(){
    $('.-layer').fadeOut();
  }
  //外部创建layer对象入口
  root.layerFactory=function(_opt){
    return new qhLayer(_opt);
  }
};