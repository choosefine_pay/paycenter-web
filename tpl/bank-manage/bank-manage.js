/**
 * Created by   :wangshuzhong
 * Created time :2017/5/11
 * File explain :施小包账户-银行卡管理
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    config:["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["layer","zui","boot","config"], operationCodeFun)
});
//代码容器

function operationCodeFun() {
  const QHConfig = $("body").data("config");//配置文件常量
  var userCode = QHConfig.getSearchDataFun().userCode;//获取上个页面信息;
  //layer框架配置
  layer.config({
    path:"../../libs/layer/"
  });

  /*页面渲染*/
  renderBankCard();//渲染银行卡，个人与企业

  /*事件操作*/
  //返回我的账户
  $(".cmn-active-breadcrumb").on("click",function() {
    parent.layer.closeAll();
  });
  //tab切换，个人与企业银行卡
  $(".bank-tab").on("click",function() {
    $(this).addClass("active").siblings(".bank-tab").removeClass("active");
    if($(this).hasClass("accord-scale") == true) {
      $(".person-bank").removeClass("hidden");
      $(".company-bank").addClass("hidden");
    }else if($(this).hasClass("accord-fixed-money") == true) {
      $(".company-bank").removeClass("hidden");
      $(".person-bank").addClass("hidden");
    }
  });
  //删除个人银行卡
  $(".person-bank-del").on("click",function() {
    var cardNum = $(this).attr("data-id");//银行卡id
    var thisWrap = $(this).parent();

    deleteBankCard(cardNum,thisWrap);
  });
  //删除企业银行卡
  $(".company-bank-del").on("click",function() {
    var cardNum = $(this).attr("data-id");//银行卡id
    var thisWrap = $(this).parent();

    deleteBankCard(cardNum,thisWrap);
  });
  //添加个人与企业银行卡
  $(".add-bankCard").on("click",addNewCard);

  /*to do 其他函数*/
  function renderBankCard() {
    $.ajax(tokenAjax({
      type:"get",
      async:false,
      url:QHConfig.url.bankCardList,
      contentType:"application/json",
      success:function(data) {
        if(data.data) {
          autoCreateBankCard(data.data);//生成银行卡
        }
      }
    }))
  }
  /*
   * 弹出新页面
   * @params url,收货详情页面地址
   * @author wangshuzhong
   * @Date   2017/5/11
   * */
  function layerNew(url) {
    layer.open({
      title: false,
      shade: 0.8,
      move:false,
      closeBtn:0,
      resize:false,
      scrollbar:false,
      type: 2,
      area: ['100%', '100%'],
      content: url,
      end:function() {
        window.location.reload();
      }
    })
  };
  /*
   * 个人与企业银行卡生成
   * */
  function autoCreateBankCard(data) {
    var person = "";//存放个人银行卡模板
    var public = "";//存放企业银行卡模板
    for(var i=0;i<data.length;i++) {
      if(data[i].isPublic == 1) {
        person += `<div class="bank-detail">
          <div class="bank-detail-top">
            <img src="${data[i].logo}" alt="">
            <span class="bank-name">${data[i].bankName}</span>
            <span class="bank-last">尾号：</span>
            <span class="bank-num">${data[i].bankcardNo}</span>
            ${data[i].cardType?`<span class="bank-card-save">${data[i].cardType}</span>`:""}
          </div>
          <div class="person-bank-del" data-id="${data[i].id}">
            删除
          </div>
        </div>`;
      }else {
        public += `<div class="bank-detail">
          <div class="bank-detail-top" data-bankName="${data[i].bankName}" data-bankAccountName="${data[i].bankAccountName}"
           data-bankcardNo="${data[i].bankcardNo}" data-bankCardLength="${data[i].bankcardDigits}">
            <img src="${data[i].logo}" alt="">
            <span class="bank-name">${data[i].bankName}</span>
            <span class="bank-last">尾号：</span>
            <span class="bank-num">${data[i].bankcardNo}</span>
            ${data[i].cardType?`<span class="bank-card-save">${data[i].cardType}</span>`:""}
          </div>
          <div class="person-bank-del" data-id="${data[i].id}">
            删除
          </div>
        </div>`;
      }
    };
    $(".add-personal").before(person);//个人银行卡
    $(".add-public").before(public);//企业银行卡
  }
  /*
   * 删除银行卡
   * */
  function deleteBankCard(cardNum,ele) {
    layer.open({
      title: ["温馨提示","background:#009CFF;color:#fff;font-size:16px"],
      shade: 0.5,
      move:false,
      closeBtn:1,
      resize:false,
      scrollbar:false,
      type: 2,
      area: ['320px', '220px'],
      content: "../common/delete-bank-card.html",
      success:function(layero,index) {
        var closeBtn=$(".layui-layer-setwin a");//layer的title关闭按钮
        var sure=layer.getChildFrame(".cmn-save-btn",index);//确定
        var cancel=layer.getChildFrame(".cmn-cancel-btn",index);//取消

        closeBtn.css({"background":"#009CFF","color":"#fff","font-size":"16px","margin-top":"-5px"});//关闭按钮样式
        closeBtn.text("X");
        cancel.on("click",function() {
          layer.close(index);
        });
        sure.on("click",function() {
          $.ajax(tokenAjax({
            type:"delete",
            url:QHConfig.url.delBankCard + "?accountBankCardId=" + cardNum,
            contentType:"application/json",
            success:function(data) {
              if(data.status == 200) {
                ele.remove();//删除元素
                layer.close(index);
              }
            }
          }))
        });
      }
    })
  }
  /*
   * 添加个人与企业银行卡
   * */
  function addNewCard() {
    var myFlag = $(this).attr("data-flag");//判断是个人还是企业，下个页面判断
    var addUrl = "../add-bank-card/add-bank-card.html" + "?myFlag=" +myFlag;//新增银行卡页面地址

    layerNew(addUrl);
  }
}