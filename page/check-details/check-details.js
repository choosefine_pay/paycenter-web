/**
 * Created by   :chengwei
 * Created time :2017/7/6
 * File explain :账单详情相关
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    comConfig: ["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["layer","comConfig"], operationCodeFun)
});
function operationCodeFun() {
  // 由于使用require框架整合，我们需要为layer.js配置读取css文件的路径
  layer.config({
    path: "../../libs/layer/"
  });
  //todo 声明变量
  var QHConfig = $("body").data("config");
  var manageInfo = QHConfig.getSearchDataFun();
  manageInfo.amount = manageInfo.amount.substr(1);
  //todo 页面初始化
  getCheckDetails();
  //todo 点击事件
  //面包屑
  $(".my-account").on("click",function () {
    if(manageInfo.type=="1"){
      parent.layer.closeAll()
    }else {
      parent.parent.layer.closeAll()
    }
  });
  $(".check").on("click",function () {
    if(manageInfo.type=="1"){
      parent.layer.closeAll();
      parent.$(".all-deal span").click();
    }else {
      parent.layer.closeAll()
    }
  });
  $(".check-result-right").on("click",".check-state,.check-pay",billClickFun);
  //todo 其他功能函数
  //加载数据
  function getCheckDetails() {
    $.ajax(tokenAjax({
      url: QHConfig.url.accountBillDetail,
      data:{accountBillId:manageInfo.accountBill},
      contentType: "application/json",
      dataType: "json",
      success: function (data) {
        var oData=data.data;
        if (data.status == 200) {
          imgURl(oData.status); //交易状态图片
          //判断账单右边按钮操作
          if(oData.status=="等待付款"&&oData.tradeType=="转账"){
            $(".check-result-right").show();
          }else {
            $(".check-result-right").hide();
          }
          //判断交易结束时间
          if(oData.finishTime!=null){
            $(".check-time-end").show();
          }else{
            $(".check-time-end").hide();
          }
          oData.createTime = QHConfig.timeFmt(oData.createTime, 2);
          oData.finishTime= oData.finishTime==null? "":QHConfig.timeFmt(oData.finishTime, 2);
          oData.targetAccountName= oData.targetAccountName==null? "":oData.targetAccountName;
          oData.targetRealName=oData.targetRealName==null? "":oData.targetRealName;
          $(".cmn-main").find("[data-name]").each(function(i,c){
            var k=$(c).attr("data-name");
            if(k == "target"){
              $(c).html(oData.targetRealName+"&nbsp"+oData.targetAccountName);
            }else {
              $(c).text(oData[k]);
            }
          })
        }
      },
      error: function () {
        layer.alert("网络错误");
      }
    }))
  }
  //交易状态图片
  function imgURl(status) {
    var imgurl = "";
    if(status=="交易成功"){
      imgurl="checkSucc.png";
    }else if(status=="交易失败"||status=="交易关闭"){
      imgurl="checkFail.png";
    }else if(status=="等待付款"){
      imgurl="noPayment.png";
    } else if(status=="处理中"){
      imgurl="checkLoad.png";
    }
    $(".check-result-img").attr("src","../../common/img/private/"+imgurl);
  }
  function billClickFun(){
    var that=this;
    if($(this).hasClass("check-state")){
      clickCloseFun(that)
    }else if($(this).hasClass("check-pay")){
      getBIzzSn(that);
    }
  }
  function clickCloseFun() {
    layer.confirm("您确定要关闭交易吗？关闭后，不能恢复", {
      btn: ["确定关闭", "取消"],
      title: "温馨提示"
    }, function () {
      $.ajax(tokenAjax({
        type: "PUT",
        url: QHConfig.url.billClose + "?accountBillId=" + manageInfo.accountBill,
        contentType: "application/json;charset=UTF-8",
        success: function (res) {
          if (res.message == "success") {
            window.location.reload();
          } else {
            layer.msg(res.message);
          }
        },
        error: function () {
          layer.alert("网络错误");
        }
      }));
    });
  }
  function getBIzzSn() {
    $.ajax(tokenAjax({
      type:"POST",
      url:QHConfig.url.accountBillId+manageInfo.accountBill,
      contentType: "application/json;charset=UTF-8",
      dataType:"json",
      success:function(res){
        var payUrl = "";
        if(res.status==400){
          layer.msg(res.message)
        }else{
          if(manageInfo.transfer == "RECHARGE"){
            payUrl = "../../tpl/project-recharge/project-recharge.html" + "?transfer=" + manageInfo.transfer + "&balance=" + manageInfo.balance+ "&amount=" + manageInfo.amount + "&bizzSn=" + manageInfo.bizzSn + "&operName=" + manageInfo.operName + "&userCode=" + manageInfo.userCode;
          }else{
            payUrl = "../../tpl/buyer-pay/buyer-pay.html" + "?transfer=" + manageInfo.transfer + "&balance=" + manageInfo.balance
              + "&amount=" + manageInfo.amount + "&bizzSn=" + manageInfo.bizzSn + "&operName=" + manageInfo.operName + "&accountName=" + manageInfo.accountName + "&userCode=" + manageInfo.userCode + "&bizzSys=" + manageInfo.bizzSys+"&tradeMemo="+manageInfo.tradeMemo+"&formBill=true";
          }
          layerNew2(payUrl);
        }
      }
    }));
  }
  function layerNew2(url) {
    parent.layer.open({
      title: false,
      shade: 0.8,
      move: false,
      closeBtn: 0,
      resize: false,
      scrollbar: false,
      type: 2,
      area: ['100%', '100%'],
      content: url,
      end:function() {
        window.location.reload();
      }
    })
  }
}