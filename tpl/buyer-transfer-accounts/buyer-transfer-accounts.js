/**
 * Created by   :宁建浩
 * Created time :2017/5/10
 * File explain :转账页面
 */
require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    config: ["../common/js/common-config"],
  }
});
require(["jquery"], function () {
  require(["layer", "zui", "boot", "config"], operationCodeFun)
});
//代码容器

function operationCodeFun() {
  var QHConfig = $("body").data("config");//公共文件
  var userInfo = QHConfig.getSearchDataFun();//获取地址栏参数
  var paySn = null;//生成的订单号
  $(".transfer-people-import").on("keyup", peopleImportFun).on("blur", peopleBlurFun);//输入收款人事件
  var peopleFlag = true;//验证收款人是否存在
  $(".transfer-money-import").on("blur", moneyImportFun);//输入金额事件
  $(".next-step-orange").on("click", nextStepFun);//下一步
  $(".add-remark-btn").on("click", ClickremarkFun);//
  $("#id-remark").on("keyup",beizhuKey).on("keyup",QHConfig.specialCharTest);

  layer.config({
    path: "../../libs/layer/"
  });
  //输入事件回调
  function peopleImportFun() {
    var txt = this.value;//收款人
    $(this).attr("data-val",txt);
    if (!txt) {
      $(".no-transfer-people").show().html("请输入收款人");
    } else {
      if (QHConfig.testReg.specialChar.test(txt)) {
        this.value = "";
        $(".no-transfer-people").show().html("请输入收款人");
      }
      $(".no-transfer-people").hide();
    }
  }

  //金额事件回调
  function moneyImportFun() {
    var txt = this.value;
    var reg = /^\d+(?:\.\d{1,2})?$/;
    if (!txt) {
      $(".no-transfer-money").show().html("金额不能为空");
    } else {
      if (!reg.test(txt)) {
        this.value = "";
      }
      $(".no-transfer-money").hide();
    }
  }

  //点击下一步回调
  function nextStepFun() {
    var userAccount = $(".transfer-people-import").data("val");//收款人
    var amount = $(".transfer-money-import").val();//打款金额
    var tradeMemo = $("#id-remark").val();//备注
    if (!userAccount && peopleFlag) {//验证存在且不为空
      $(".no-transfer-people").show().html("请输入收款人");
      return;
    } else if (!amount) {
      $(".no-transfer-money").show().html("请输入转账金额");
      peopleFlag = false;
      return;
    }
    if (peopleFlag) {
      var accountCode = "";
      var bizzSn = null;
      //通过账号获得租户编号
      $.ajax(tokenAjax({
        type: "GET",
        async: false,
        url: QHConfig.url.otherUserCode + "?accountName=" + userAccount,
        success: function (data) {
          accountCode = data.data;
        }
      }));
      //获取订单流水号
      $.ajax(tokenAjax({
        type: "GET",
        url: QHConfig.url.transferSN,
        async: false,
        success: function (res) {
          bizzSn = res.data;
          layer.open({
            type: 2,
            title: false,
            closeBtn: 0,
            area: ["100%", "100%"],
            scrollbar: false,
            content: `../buyer-pay/buyer-pay.html?userCode=${userInfo.userCode}&amount=${amount}&balance=${userInfo.balance}&transfer=${userInfo.transfer}&operName=${userInfo.operName}&bizzSn=${bizzSn}&accountName=${accountCode}&tradeMemo=${tradeMemo?tradeMemo:"普通转账"}&bizzSys=SELF`,
            end:function() {
              window.location.reload();
            }
          });
        }
      }));
    }
  }

  //收款人失去焦点时判断是否存在
  function peopleBlurFun() {
    var userAccount = $(this).attr("data-val");//当前收款人
    if(!userAccount){
      $(".no-transfer-people").show().html("请输入收款人");
      return;
    }
    $.ajax(tokenAjax({
      type: "GET",
      async: false,
      url: QHConfig.url.exists + "?accountName=" + userAccount,
      success: function (data) {
        var msg = data.message;//提示信息
        if (data.status == 400) {
          $(".no-transfer-people").show().html(msg);//显示错误信息
          peopleFlag = false;//并为false
        } else {
          $(".transfer-people-import").attr("data-val", userAccount).val(userAccount.replace(userAccount[0], "*"));
          $(".no-transfer-people").hide();//如果正确，隐藏信息
          peopleFlag = true;//并未true
        }
      }
    }));
  }

  //备注隐藏显示
  function ClickremarkFun() {
    if (this.innerHTML == "添加备注") {
      this.innerHTML = "关闭备注"
    } else {
      this.innerHTML = "添加备注"
    }
    $(".remark-con").toggle();
  }
  function beizhuKey(){
    var val=this.value;
    $("#id-bz").html(50-val.length);
  }
}