define(['vue'],function(Vue){
	function getHeadTemplate(){
		return '<div class="cmn-password-title">'+
			'<div class="cmn-password-qhLogo">支付中心</div>'+
			'<span class="cmn-find-password">{{username}}</span>'+
		'</div>';
		
	}
	var header=Vue.extend({
		props: ['username'],
		template: getHeadTemplate()
	});
	Vue.component('pay-header',header);
	//初始化公共头部
	window.initTop=function(_vue,_pos){
		var app0 = new _vue({
		  el: '#app0',
		  data: {
			message:_pos
		  },
		  methods:{
			logout:function(){
			}
		  }
		})
	}
});