/**
 * Created by   :wsz
 * Created time :2017/05/11
 * File explain :分包单位 > 施小包资金账户 -打分包款
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    zui: ["zui/js/zui"],
    layer:["layer/layer"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    comConfig: ["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["zui", "boot", "comConfig" ,"layer"], operationCodeFun)
});
function operationCodeFun() {
  // 由于使用require框架整合，我们需要为layer.js配置读取css文件的路径
  layer.config({
    path: "../../libs/layer/"
  });
  //变量
  var QHConfig = $("body").data("config");
  var search = QHConfig.getSearchDataFun();//获取上个页面信息;
  var userCode = window.sessionStorage.getItem("userCode");//获取userCode
  var detail = JSON.parse(window.sessionStorage.getItem("detail"));//获取施小包账户详情+
  var roleId=detail.roleId==4?5:2;
  var piadOver = $(".total-paid-price");//已付
  var totalPrice = $(".lump-sum-price");//分包总价
  var noPaid = $(".unpaid-num");//未付
  var teamName = $(".team-name-info");//班组名称
  var teamControl = $(".team-person-info");//班组承包人
  var receiver = $(".receive-person");//收款人
  var orderNum = null;//业务流水单号，打分包款
  //end
  //页面渲染
  render();
  //end

  //事件操作
  $(".enter-amount-input").on("keyup blur",clearNoNum);//金额输入校验
  $(".to-pay-btn").on("click",loginBankFun);//去付款
  //end

  //to do 其他功能函数
  /*
   * 页面渲染
   * */
  function render()  {
    noPaid.text(addTurnMoney(search.teamTotalPrice-search.paid));//未付
    totalPrice.text(addTurnMoney(search.teamTotalPrice));//分包总价
    piadOver.text(addTurnMoney(search.paid));//已付
    teamName.text(search.teamName);//班组名称
    teamControl.text(search.teamContractorName);//班组承包人
    receiver.text(search.companyName);//收款人
  }
  //只能输入小数点后两位
  function clearNoNum(){
    this.value = this.value.replace(/[^\d.]/g,""); //清除"数字"和"."以外的字符
    this.value = this.value.replace(/^\./g,""); //验证第一个字符是数字而不是
    this.value = this.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的
    this.value = this.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    this.value = this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3'); //只能输入两个小数
  }
  /*
   * 转换金额，加判断层
   * */
  function addTurnMoney(val) {
    if(val) {
      return QHConfig.turnMoney(val,2);
    }else {
      return "0.00";
    }
  }
  //金额输入事件回调
  function moneyKeyUpFun(){
    var val=$(this).val();
    var reg=new RegExp("^[0-9]+\.?[0-9]{0,2}$");

    if(!reg.test(val)){
      $(this).val("");
    }else{
      $(this).val(val);
    }
  }
  //去付款
  function loginBankFun(){
    var moneyFlag=false;
    var money = $(".enter-amount-input").val();//输入金额
    if(money == "") {
      $(".enter-amount-money").css("visibility","visible");//显示提示本次打款金额必须大于0且不超过未付金额
      $(".enter-amount-input")[0].focus();//聚焦输入框
      moneyFlag=false;
      return;
    }else if(money<=0||money>parseFloat(search.teamTotalPrice-search.paid)){

      $(".enter-amount-money").css("visibility","visible").html("本次打款金额必须大于0且不超过未付金额");//显示提示
      $(".enter-amount-input")[0].focus();//聚焦输入框
      moneyFlag=false;
    }else {
      $(".enter-amount-money").css("visibility","hidden");//隐藏提示
      moneyFlag=true;
    }
    //打分包款生成业务流水单号
    if(moneyFlag){
      createOrder();
      //需传值到支付页面
      var payUrl = "../buyer-pay/buyer-pay.html" + "?transfer=SUBPACKAGE" + "&balance=" + detail.availableBalance
        + "&amount=" + money + "&bizzSn=" + orderNum + "&operName=" + detail.realName + "&accountName=" + search.teamUserCode
        + "&userCode=" + userCode+"&bizzSys=LABOR";
      if(orderNum) {
        layerNew(payUrl);
      }
    }
  }
  /*
   * 弹出新页面
   * @params url,收货详情页面地址
   * @author wangshuzhong
   * @Date   2017/5/11
   * */
  function layerNew(url) {
    layer.open({
      title: false,
      shade: 0.8,
      move:false,
      closeBtn:0,
      resize:false,
      scrollbar:false,
      type: 2,
      area: ['100%', '100%'],
      content: url
    })
  }
  /*
   *
   * */
  function createOrder() {
    var needData = {
      projectCode:search.projectCode,
      teamId:search.teamId,
      roleId:roleId,
      amount:$(".enter-amount-input").val(),
      tradeType:"SUBPACKAGE",
      tradeMemo:"打分包款",
      userName:detail.realName,
      targetUserCode:search.affiliatedUnitsCode
    };
    $.ajax(tokenAjax({
      type:"post",
      //url:QHConfig.url.payProcess+`?tradeType=${needData.tradeType}&tradeMemo=${needData.tradeMemo}&amount=${needData.amount}&username=${needData.userName}&targetUserCode=${needData.targetUserCode}&roleId=${roleId}&teamId=${needData.teamId}&projectCode=${needData.projectCode}&userCode=${search.userCode}`,
	  url:"http://construction-test.sxiaobao.com/construction/bank/payProcess"+`?tradeType=${needData.tradeType}&tradeMemo=${needData.tradeMemo}&amount=${needData.amount}&username=${needData.userName}&targetUserCode=${needData.targetUserCode}&roleId=${roleId}&teamId=${needData.teamId}&projectCode=${needData.projectCode}&userCode=${search.userCode}`,
      async:false,
      dataType:"json",
      contentType:"application/json",
      success:function(data) {
        if(data.status == 200) {
          orderNum = data.data;//自动创建的业务流水单号
        }else {
          layer.msg(data.message);
        }
      }
    }))
  }
  //end
}
