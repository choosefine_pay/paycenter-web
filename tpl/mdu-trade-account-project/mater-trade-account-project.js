/**
 * Created by   :Wangshuzhong
 * Created time :2017-04-08
 * File explain :交易账务（项目）
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    bootTable: ["bootstrap-table-dist/dist/js/bootstrap-table"],
    comConfig: ["../common/js/common-config"],
    QHPagination: ["../common/js/qhpager"],
    temp: ["template"]
  }
});
require(["jquery"], function () {
  require(["QHPagination"], function () {
    require(["temp", "zui", "layer", "bootTable", "comConfig"], operationCodeFun)
  })
});
function operationCodeFun(template) {
  // 由于使用require框架整合，我们需要为layer.js配置读取css文件的路径
  layer.config({
    path: "../../libs/layer/"
  })

  //变量声明
  var QHConfig = $("body").data("config");
  var nowYear = null;//当前年份
  var tableHead = $("#id-user-account-table");//伪表格表头
  var pager = window.qhPaginationFactroy("project");//分页器功能获取
  var _Data = {};//表格数据容器
  var detailCon = null; // 展开详情容器;
  var _detailData = {name: ""}; // 详情表格数据 name 表格名

  //表格数据请求参数
  var ajaxData = null;

  // 展开详情所需参数
  var detailParams = {
    date:"",
    projectCode:"",
    buyerCompanyCode:""
  };

  // 表格数据渲染参数
  var fakeTableOpt = {
    operationDate : {name : "", clsName : "cmn-detail-operationDate cmn-account-project"},
    projectName : {name : "", clsName : "cmn-detail-projectName cmn-account-project cmn-project-text"},
    buyerCompanyName : {name : "", clsName : "cmn-detail-companyName cmn-account-project cmn-company-text"},
    lastUnpaid : {name : "", clsName : "cmn-detail-lastUnpaid cmn-account-project"},
    currentTotal : {name : "", clsName : "cmn-detail-currentTotal cmn-account-project"},
    currentAdjust : {name : "", clsName : "cmn-detail-currentAdjust cmn-account-project"},
    currentHaspaid : {name : "", clsName : "cmn-detail-currentHaspaid cmn-account-project"},
    notReceipt : {name : "", clsName : "cmn-detail-notReceipt cmn-account-project"}
  };

  // 分页栏渲染参数
  var pagerOptions = {
    tag_Class: 'cmn-page_tag',//分页标签默认样式
    tag_on_Class: 'cmn-page_tag_on',//分页标签选中样式
    input_Class: 'cmn-pageInput',//分页搜索框样式
    showTags: 5,//标签省略距离
    pageSizes:[],
    pageSearch: true//是否启用页面搜索跳转
  };
  //end

  //页面初始化
  getYears();//获取年份
  tableHead.bootstrapTable();//应收款表格渲染，表头样式
  QHConfig.fakeTable($("#fakeTableCon"), "lg", fakeTableOpt);// 伪表格生成文本前置
  getTradeAccountProject(nowYear);//根据年份渲染页面信息


  //end

  //事件绑定
  //点击面包屑返回交易账户页面
  $(".cmn-backTo-tradeAccount").on("click",function() {
    parent.layer.closeAll();
  });
  //改变年份，页面的内容随着年分变
  $("#id-year-choose").on("change",function(){
    nowYear = $(this).val();
    getTradeAccountProject(nowYear);
  });
  // 清空条件按钮点击事件绑定
  $("#id-acc-clear-btn").on("click", function () {
    $("#id-companyName").html("<option value=''>全部</option>");
    $(".cmn-query-container").find("[data-name]").val(""); // 清空查询条件
    $.extend(ajaxData, getQueryData()); // 更新查询请求数据
  });
  // 查询按钮点击事件绑定
  $("#id-acc-query-btn").on("click", function () {
    $.extend(ajaxData, getQueryData()); // 更新查询请求数据
    tradeAccountTableRender(); // 刷新伪表格
  });
  //根据项目获取建筑公司
  $("#id-projectName").on("change",function() {
    var pjCode = $(this).val();

    if(pjCode != "") {
      var getCompanyData = {projectCode : pjCode};

      getCompanyByProjectCode(getCompanyData);//根据项目编号获取建筑公司
    }else {
      $("#id-companyName").html("<option value=''>全部</option>");
    }

  })
  //end

  //to do 其他功能函数
  /*
   * 获取当前供应商的入驻时间
   * @author Wangshuzhong
   * @Date   2017/04/08
   */
  function getYears(){
    $.ajax(tokenAjax({
      url:QHConfig.url.getYears,
      async:false,
      dataType:"json",
      success:function(data){
        var _data=data.data;
        var oHtml="";

        if(_data) {
          for(var i=0;i<_data.length;i++){
            oHtml += `<option value="${_data[i]}">${_data[i]}</option>`;
          }
        }
        $("#id-year-choose").html(oHtml);
      }
    }));
    nowYear = $("#id-year-choose").val();
  }
  /*
   * 根据年份渲染页面信息，获取项目名称，交易账务表格渲染
   * @param year,选择的年份，（进来页面默认为今年）
   * @author Wangshuzhong
   * @Date   2017/04/08
   */
  function getTradeAccountProject(year) {
    ajaxData = {
      pageNum : 1,
      pageSize : 10,
      date : year
    };
    getThisYearProject(year);//获取选择年的供应商全部项目
    tradeAccountTableRender();//交易账务表格渲染
  }
  /*
   * 根据年份获取项目名称，渲染到页面上
   * @param year,选择的年份
   * @author wangshuzhong
   * @Date   2017/4/08
   * */
  function getThisYearProject(year) {
    $.ajax(tokenAjax({
      url:QHConfig.url.projectByTime,
      data:{yearData:year},
      dataType:"json",
      success:function(data){
        var _data = data.data;
        var oHtml = "<option value=''>全部</option>";

        if(_data) {
          for(var i=0;i<_data.length;i++) {
            oHtml += `<option value="${_data[i].projectCode}">${_data[i].projectName}</option>`;
          }
        }
        $("#id-projectName").html(oHtml);
      }
    }))
  }
  /*
   * 根据项目编号获取对应的建筑公司
   * @param code,选择的项目编号
   * @author wangshuzhong
   * @Date   2017/4/08
   * */
  function getCompanyByProjectCode(code) {
    $.ajax(tokenAjax({
      url:QHConfig.url.companyByProject,
      data:code,
      dataType:"json",
      success:function(data){
        var _data = data.data;
        var oHtml = "<option value=''>全部</option>";

        if(_data) {
          for(var i=0;i<_data.length;i++) {
            oHtml += `<option value="${_data[i].companyCode}">${_data[i].companyName }</option>`;
          }
        }
        $("#id-companyName").html(oHtml);
      }
    }))
  }
  /*
   * 查询条件收集
   * @return queryMsg 查询数据，用户选择或输入需要查询的内容
   * @author wangshuzhong
   * @Date   2017/4/07
   * */
  function getQueryData() {
    var queryMsg = {}; // 声明数据载体
    $(".cmn-query-container").find("[data-name]").each(function () {
      var k = $(this).data("name"); // 字段名
      var val = $(this).val(); // 值

      queryMsg[k] = val; // 注入载体
    });
    return queryMsg; // 返回查询数据
  }

  /*
   * 伪表格刷新
   * 仿bootstrapTable刷新操作
   * @author wangshuzhong
   * @Date   2017/4/08
   * */
  function tradeAccountTableRender() {
    if(pager.getPage()){
      ajaxData.pageNum = pager.getPage();
    }
    // 数据请求
    fakeTableDataAjaxFun();
    // 生成伪表格HTML文本
    var fakeHtml = template("fakeTableHtml", _Data);

    // 注入表格容器
    $("#fakeTableCon").html(fakeHtml);
    renderPagerFun();

    var _companyName = $("#fakeTableCon").find(".cmn-detail-companyName");
    _companyName.each(function(i,e) {
      if($(e).text() == "" ) {
        $(e).text("-");
      }
    });

    //伪表格展示内容格式转换
    wrapTableContentTurn();

    // 伪表格展开收起按钮点击
    $(".cmn-hide-show-btn").on("click", hideAndShowClick);
    QHConfig.fakeTableDetailFun("lg");
  }
  /*
   * 伪表格数据请求
   *
   * */
  function fakeTableDataAjaxFun() {
    for(var key in ajaxData) {
      if(ajaxData[key] == "") {
        delete ajaxData[key];
      }
    };//删除为空参数
    $.ajax(tokenAjax({
      url: QHConfig.url.getCarryOverProject,
      data: ajaxData,
      async: false,
      dataType: "json",
      success: function (data) {
        _Data = data.data;
        if(_Data.list.length>0){
          $("#tip").hide();
        }else {
          $("#tip").show();
        }
      }
    }));
  }
  /*
   * 渲染分页器
   * 引用组件功能对分页器渲染
   * */
  function renderPagerFun() {
    pager.init($(".cmn-table-pager"), tradeAccountTableRender, pagerOptions);// 初始化分页器对象
    pager.setParameters(_Data.pageNum, _Data.pageSize, _Data.pages);// 设置分页器参数
    pager.render(_Data.pageNum);//渲染
  }
  /*
   * 展开收起按钮点击事件
   * 业务部分，渲染详情部分
   * @author wangshuzhong
   * @Date   2017/4/08
   * */
  function hideAndShowClick() {
    var i = $(this).data("i"); // 获取下标
    var that = this; // 明确指向
    detailCon = $(this).parent().parent().next(); // 为当前详情容器赋值
    detailParams.date = (_Data.list[i].operationDate.split("-"))[0];
    detailParams.projectCode = _Data.list[i].projectCode;
    detailParams.buyerCompanyCode = _Data.list[i].buyerCompanyCode;
    if (detailCon.html() == "") {
      // 如果详情容器中不存在内容则为其渲染 否则不做操作
      _detailData.name = $(that).attr("href").slice(1) + "table";
      detailTableRender();
      detailTableFmt(i);
    }
    ;
  }
  /*
   * 详情表格渲染
   * 为当前详情容器加入表格
   * */
  function detailTableRender() {
    var detailHtml = template("deliveryText", _detailData);
    detailCon.html(detailHtml);
  }
  /*
   * 详情表格初始化
   * 初始化表格
   * */
  function detailTableFmt(index) {
    // 初始化表格
    $("#" + _detailData.name).bootstrapTable({
      url: QHConfig.url.projectMonthDetail,
      queryParams: function () {
        // 传参
        return detailParams;
      },
      ajax:tableAjax,
      responseHandler: function (res) {
        // 数据处理
        var needData = QHConfig.BSTTableResHandler(res, resDataFmt);

        needData.index = index;
        return needData;
      },
      onLoadSuccess: detailTableLoadSuc
    });
  }
  /*
   * 详情表格加载成功回调函数
   * */
  function detailTableLoadSuc() {
    var lookMonthDetail = detailCon.find(".cmn-active-breadcrumb");
    var that = this;
    var $table = $("#"+_detailData.name); // 详情表格
    var allTr = $table.find("tbody tr");

    for(var i=0;i<allTr.length;i++) {
      allTr.eq(i).find("td").eq(1).addClass("cmn-project-text");
      allTr.eq(i).find("td").eq(2).addClass("cmn-company-text");
    }

    // 隐藏默认分页器
    detailCon.find(".fixed-table-pagination").hide();
    //点击查看详情，跳转到当月交易明细页面
    lookMonthDetail.on("click",function(){
      var index=$(this).parent().parent()[0].rowIndex-1;//当前行序号
      var rowData = that.data[index];//当前行数据
      var thisData=addZero(rowData.operationDate.replace("月",""));//获取当月具体日期
      var needTime = nowYear + "-" +thisData;//拼接时间“207-04”
      var pjCode = rowData.projectCode;//项目编号
      var pjName = rowData.projectName;//项目名称
      var cmpCode = rowData.buyerCompanyCode;//公司code

      projectMonthAccountDetail(needTime,cmpCode,pjCode,pjName);//查看交易账务（项目）--月明细
    })
  }
  /*
   * 表格详情加工
   * @author wangshuzhong
   * @Date   2017/4/08
   * */
  function resDataFmt($data) {
    $data.operationDate = parseInt(($data.operationDate.split("-"))[1]) + "月";
    $data.lastUnpaid = QHConfig.turnMoney($data.lastUnpaid/10000,2);
    $data.currentTotal = QHConfig.turnMoney($data.currentTotal/10000,2);
    $data.currentAdjust = $data.currentAdjust == 0?"无":$data.currentAdjust;
    $data.currentHaspaid = QHConfig.turnMoney($data.currentHaspaid/10000,2);
    $data.notReceipt = QHConfig.turnMoney($data.notReceipt/10000,2);
    $data.lookDetail = "<span class='cmn-active-breadcrumb'>查看详情</span>";
  }
  /*
   * 伪表格
   * @author wangshuzhong
   * @Date   2017/4/08
   * */
  function wrapTableContentTurn(){
    var _thisYear = $(".cmn-detail-operationDate");
    for(var i=0;i<_thisYear.length;i++) {
      var _yearText = (_thisYear.eq(i).text().split("-"))[0];//年份
      var _lastUnpaidText = parseFloat($(".cmn-detail-lastUnpaid").eq(i).text())/10000;//结转额（万元）
      var _currentTotalText = parseFloat($(".cmn-detail-currentTotal").eq(i).text())/10000;//应收款（万元）
      var _currentHaspaidText = parseFloat($(".cmn-detail-currentHaspaid").eq(i).text())/10000;//实收款（万元）
      var _notReceiptText = parseFloat($(".cmn-detail-notReceipt").eq(i).text())/10000;//未收款（万元）
      var adjustText = $(".cmn-detail-currentAdjust").eq(i).text();//调整

      _thisYear.eq(i).text(_yearText + "年");
      $(".cmn-detail-currentAdjust").eq(i).text(adjustText == "0"?"无":adjustText);
      $(".cmn-detail-lastUnpaid").eq(i).text(QHConfig.turnMoney(_lastUnpaidText,2));
      $(".cmn-detail-currentTotal").eq(i).text(QHConfig.turnMoney(_currentTotalText,2));
      $(".cmn-detail-currentHaspaid").eq(i).text(QHConfig.turnMoney(_currentHaspaidText,2));
      $(".cmn-detail-notReceipt").eq(i).text(QHConfig.turnMoney(_notReceiptText,2));
    }
  }
  /*
   * 年度交易账务（项目）--月交易明细表格渲染
   * @params date,时间“2017-04”
   * @params code,项目编号
   * @params name,项目名称
   * @author Wangshuzhong
   * @Date   2017/04/08
   */
  function projectMonthAccountDetail(date,cmpCode,pjCode,name) {
    var strUrl = "../mdu-month-trade-proreceivable/mdu-month-trade-proreceivable.html";
    var needInfo = {
      operationDate : date,
      buyerCompanyCode : cmpCode,
      projectCode : pjCode,
      projectName : name
    };

    $("body").data("search",needInfo);//下个子页面需要的值
    layer.open({
      title: false,
      shade: 0.8,
      move:false,
      closeBtn:0,
      resize:false,
      scrollbar:false,
      type: 2,
      area: ['100%', '100%'],
      content: strUrl
    })
  }
  /*
  * 为小于10的月份前面加上0
  * */
  function addZero(val) {
    return val > 9 ? val : "0" + val;
  }
  //end
}