/**
 * Created by   :chengwei
 * Created time :2017/4/14
 * File explain :忘记密码
 */
require.config({
    baseUrl: "../../libs",
    paths: {
        jquery: ["jquery/jquery-1.11.3"],
        comConfig: ["../common/js/common-config"],
        layer: ["layer/layer"],
        zui: ["zui/js/zui"],
        static: ["//static.geetest.com/static/tools/gt"]

    }
});
require(["jquery"], function () {
    require(["comConfig","layer","zui", "static"], operationCodeFun)
});
function operationCodeFun() {
    // layer框架路径配置
    layer.config({path: "../../libs/layer/"});
    //todo 变量声明
    var QHConfig = $("body").data("config"); // 配置文件内容
    var _phone = $("#id-telNumber"); //注册的手机号
    var _identifying = $("#id-identifying"); //验证码框
    var countdown=60; //倒计时60秒
    var keyWord = null;//获取ajax的head验证码参数

    //todo 初始化渲染页面
    //向服务器请求验证码
    getKeyWordByServer();

    //todo 点击事件
    /*输入绑定手机号码*/
    //点击获取验证码获得验证码
    $("#id-verificat").on("click",getVerificat);
    //手机号
    $("#id-telNumber").on("blur change",phoneTest);
    //验证码输入框
    $("#id-identifying").on("blur change",function (){
        $(".password-tip").addClass("hide");
        activeBtn($(".first"),$(".next"));
    });
    //手机号里的清除
    $(".clear").on("click",function () {
       $(this).siblings("input").val("");
       $(".password-tip").addClass("hide");
    });
    //下一步
    $(".next").on("click",function () {
        var phoneVal = _phone.val();
        var identifyingVal = _identifying.val();
        //判断验证码
        if (keyWord == null) {
            $(".verification").removeClass("hide");
            return;
        } else {
            $(".verification").addClass("hide");
            //设置ajax头部
            $.ajaxSetup({
                headers: {
                    geetest_challenge: keyWord.challenge,
                    geetest_validate: keyWord.validate,
                    geetest_seccode: keyWord.seccode
                }
            });
        }
        $.ajax(tokenAjax({
            url:QHConfig.url.affirmVerificationCode+phoneVal+"/"+identifyingVal,
            contentType:"application/json",
            dataType:"Json",
            success: function(data) {
               if(data.status==200){
                  $(".first-step").addClass("hide");
                  $(".second-step").removeClass("hide");
                  $(".progressbar-pass").addClass("active")
               }else{
                 $(".note").removeClass("hide");
               }
            }
        }))
    });
    /*设置新登录密码*/
    $(".second").on("blur change",function () {
        //判断格式是不是错误
        passwordClick.bind(this)();
        //判断两个密码是否一致
        twoPassword();
    });
    //密码的删除
    $(".new-clear").each(function(i,el){
        $(el).on("click",function () {
            $(this).siblings("input").val("");
            if(i=0){
                $(".new-password").addClass("hide");
            }else {
                $(".affirm-password").addClass("hide");
            }
        });
    });
    //确定修改
    $(".verify").on("click",function () {
       var option= {
           userAccount:_phone.val(),
           password:$("#id-affirm-password").val()
       }
        $.ajax(tokenAjax({
            type:"put",
            url:QHConfig.url.changeCode,
            data:JSON.stringify(option),
            contentType:"application/json",
            dataType:"Json",
            success: function(data) {
                console.log(data);
                if(data.status==200){
                    $(".warp").addClass("hide");
                    $(".third-step").removeClass("hide");
                    $(".progressbar-complete").addClass("active")
                }else if(data.status==400){
                    layer.msg(data.message)
                }
            }
        }))
    });
    /*立即登录*/
    $(".sure-login").on("click",function () {
        //页面的跳转
        window.location.href="../login/login.html";
    });
    
    //todo 其他功能
    //向服务器获取验证码
    function getKeyWordByServer() {
        var nowTime = (new Date()).getTime();//获取当前时间戳
        // 验证开始需要向网站主后台获取id，challenge，success（是否启用failback）
        $.ajax({
            url: QHConfig.url.loginCode + nowTime, // 加随机数防止缓存
            type: "get",
            async: false,
            dataType: "json",
            success: function (data) {
                data = JSON.parse(data.data);
                // 使用initGeetest接口
                // 参数1：配置参数
                // 参数2：回调，回调的第一个参数验证码对象，之后可以使用它做appendTo之类的事件
                initGeetest({
                    gt: data.gt,
                    challenge: data.challenge,
                    product: "float", // 产品形式，包括：float，embed，popup。注意只对PC版验证码有效
                    offline: !data.success // 表示用户后台检测极验服务器是否宕机，一般不需要关注
                }, handlerPopup);
            }
        });
    };
    //获取验证码成功回调
    function handlerPopup(captchaObj) {
        // 成功的回调
        captchaObj.onSuccess(function () {
            var validate = captchaObj.getValidate();//方法

            keyWord = {
                challenge: validate.geetest_challenge,
                validate: validate.geetest_validate,
                seccode: validate.geetest_seccode
            };//获取登录ajax的head参数
        });
        // 将验证码加到id为popup-captcha的元素里
        captchaObj.appendTo("#popup-captcha");
    };
    //按钮的点击事件
    function activeBtn(elename,btn) {
        var flag=true;
        var tip = $(".password-tip");
        //判断输入框为空么
        elename.each(function (i,e) {
            var text = $(e).val();
            if(text==""){
                flag=false;
            }
        });
        //判断有没有报错
        for (var i=0;i<tip.length;i++){
            if(tip.eq(i).hasClass("hide")==false){
                flag=false;
                break;
            }
        }
        if(flag==true){
            btn.addClass("active").attr("disabled",false);
        }else {
            btn.removeClass("active").attr("disabled",true);
        }
    }
    //验证手机号码
    function phoneTest() {
        var phone = /^1(3[0-9]|4[57]|5[0-35-9]|7[0135678]|8[0-9])\d{8}$/;
        var val = $(this).val();
        $(".password-tip").addClass("hide");
        if(val!=""){
            $(".password-tip").addClass("hide");
            if(phone.test(val)==false){
                $(".phone").removeClass("hide");
                activeBtn($(".first"),$(".next"));
            }else {
                $(".phone").addClass("hide");
                activeBtn($(".first"),$(".next"));
            }
        }else{
            $(".not-phone").removeClass("hide");
        }
    }
    // 倒计时
    function settime(obj) {
        if (countdown == 0) {
            obj.attr("disabled",false);
            obj.val("获取验证码");
            countdown = 60;
            return;
        } else {
            obj.attr("disabled",true);
            obj.val("重新发送("+countdown+")s");
            countdown--;
        }
        setTimeout(function(){
                settime(obj)
        },1000)
    }
    //验证码获取
    function getVerificat() {
        var phoneNum = _phone.val();
        var phone = /^1(3[0-9]|4[57]|5[0-35-9]|7[0135678]|8[0-9])\d{8}$/;
        //判断号码为空且不能为错的
        if(phoneNum!=""&&phone.test(phoneNum)==true){
            $.ajax(tokenAjax({
                url:QHConfig.url.sendVerificationCode+phoneNum,
                contentType:"application/json",
                dataType:"Json",
                success: function(data) {
                    if(data.status==200){
                        layer.msg(data.data);
                        settime($("#id-verificat"));
                    }else{
                        layer.msg("发送失败")
                    }
                }
            }));
        }
    }
    //新登录的密码
    function passwordClick() {
        var _id = $(this).attr("id");
        var _val = $(this).val();
        var _length = _val.length;
        if(_length!=0){
            if(_length>5&&_length<21){
                if(_id=="id-new-password"){
                    $(".new-password").addClass("hide");
                }else{
                    $(".affirm-password").addClass("hide");
                }
                activeBtn($(".second"),$(".verify"))
            }else{
                if(_id=="id-new-password"){
                    $(".new-password").removeClass("hide");
                }else{
                    $(".affirm-password").removeClass("hide");
                }
            }
        }
    }
    //判断两个密码是否一致
    function twoPassword() {
        var _newPassword = $("#id-new-password").val();
        var _affirmPassword = $("#id-affirm-password").val();
        if(_newPassword.length>5&&_newPassword.length<21){
            if(_newPassword!=""&&_affirmPassword!=""){
                if(_newPassword==_affirmPassword){
                    $(".contrast-password").addClass("hide");
                }else {
                    $(".contrast-password").removeClass("hide");
                }
            }
        }
    }
}