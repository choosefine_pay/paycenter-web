/**
 * Created by   :wsz
 * Created time :2017/03/15
 * File explain :材料--商家中心
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    comConfig: ["../common/js/common-config"],
    qhSelect: ["../common/js/qh-selectors"]
  }
});
require(["jquery"], function () {
  require(["qhSelect"],function(){
    require(["layer", "zui", "boot", "comConfig"], operationCodeFun)
  })
});
function operationCodeFun() {
  // 由于使用require框架整合，我们需要为layer.js配置读取css文件的路径
  layer.config({
    path: "../../libs/layer/"
  });
  //全局变量
  var token=sessionStorage.getItem('token');//权限相关
  var QHConfig=$("body").data("config");//配置文件

  //to do 页面初始化，页面渲染
  parent.$("body").data("businessSave",true);//没改动过的标记
  $(".bus-area-tip").hide();//初始化隐藏提示语句
  //获取公司详情，渲染到页面中
  getDetailsByUserCode();
  //end

  //to do 绑定事件函数
  //点击省列表某一项，将省的code和名称储存，便于保存时收集数据保存,再根据该省的code获取该省下的市区列表；
  $("#id-send-province").on("change",function() {
    getCityListByProvinceCode($(this),$("#id-send-city"));
    parent.$("body").data("businessSave",false);//改动过商家信息的标记
  });
  $("#id-send-city").on("change",function() {
    parent.$("body").data("businessSave",false);//改动过商家信息的标记
  })
  //公司简介，当输入的时候实时计算还可以输入多少字；包括复制贴字的方法也要考虑
  var introductce=$(".bus-combrief-text").val();

  $(".bus-combrief-text").on("keyup", function () {
    var inputNum = $(".bus-combrief-text").val().length;//计算企业介绍字数

    $(".bus-txt-allowInput").text((500 - inputNum));//还可输入字数
    if($(this).val() != introductce){
      parent.$("body").data("businessSave",false);//改动过商家信息的标记
    }
  }).on("blur", function () {
    var inputNum = $(".bus-combrief-text").val().length;//计算企业介绍字数

    $(".bus-txt-allowInput").text((500 - inputNum));//还可输入字数
    if($(this).val() != introductce){
      parent.$("body").data("businessSave",false);//改动过商家信息的标记
    }
  });
  //保存修改，判断发货和供货地区是否为空,收集数据保存
  $("#id-save-change").on("click", function () {
    checkMustDataEmpty();//判断必传参数是否为空
  })

  //end

  //to do 响应成功回调函数
  /*
   * 根据userCode获取公司信息，渲染到页面上
   * @param data, 请求成功返回的成功数据
   * @author Wangshuzhong
   * @Date   2017/03/15
   */
  function domRender(data) {
    if(data.status == 200) {
      var companyInfo = data.data.company || "公司信息缺失";//公司信息
      var shopInfo = data.data.shopVO;//商家信息

      if (shopInfo) {
        renderByCompanyAndShop(companyInfo,shopInfo);//渲染主页公司名称和logo

        var introduceNum = $(".bus-combrief-text").val().length;//企业介绍字数
        var sendAreaCityCode=shopInfo.sendGoodsAreaCode;//发货地区城市code
        var sendAreaProvinceCode=shopInfo.sendGoodsProvince;//发货地区省份code
        var supplyAreaCode=shopInfo.supplyAreaCode;//供货地区code
        var areaCode = {
          province:sendAreaProvinceCode,
          cityCode:sendAreaCityCode,
          cityName:shopInfo.sendGoodsAreaName,
          supplyAreaCode:shopInfo.supplyAreaCode
        };

        //发货地区渲染
        sessionStorage.setItem("areaCode",JSON.stringify(areaCode));
        //一次性获取所有省列表，先加载，便于用户体验
        getAllProvince($("#id-send-province"),sendAreaProvinceCode);
        //如果发货地区的值不为空，则需渲染至页面上
        if(sendAreaCityCode) {
          renderSendArea(sendAreaProvinceCode,sendAreaCityCode);
        }
        //供货地区渲染
        $("#id-supply-area").val(supplyAreaCode);
        chooseSupplyArea(supplyAreaCode);//供货地区渲染
        //企业介绍字数计算
        $(".bus-txt-allowInput").text((500 - introduceNum));//还可以输入字数
      }
    }
  }
  //end

  //to do 其他功能函数
  /*
   * 根据userCode获取商家中心详情
   * @param  code, userCode
   * @author Wangshuzhong
   * @Date   2017/03/15
   */
  function getDetailsByUserCode() {
    $.ajax(tokenAjax({
      type: "get",
      async: false,
      url: QHConfig.url.getCenterInfo,
      headers: {
        Accept: "application/json; charset=utf-8",
        Authorization:token
      },
      dataType: "json",
      success: domRender
    }))
  }
  /*
   * 渲染商家和公司信息
   * @param  companyInfo, 公司信息
   * @param  shopInfo, 商家信息
   * @author Wangshuzhong
   * @Date   2017/03/15
   */
  function renderByCompanyAndShop(companyInfo,shopInfo){
    if(companyInfo.companyName) {
      $(".bus-data-companyName").text(companyInfo.companyName);//公司名称
    }
    if(companyInfo.companyShortname) {
      $(".bus-data-companyShortname").text(companyInfo.companyShortname);//公司简称
    }
    $(".bus-com-logo-img").attr("src", shopInfo.imgUrl);//公司标志，logo
    $(".bus-combrief-text").val(shopInfo.synopsis);//公司简介
    $(".cmn-main-list").attr("main-id", shopInfo.id);//主键id，储存便于收集数据
    //首页的公司名字和logo
    if(companyInfo.companyName&&shopInfo.imgUrl){
      parent.$(".index-nav-logo").html("");
      parent.$(".index-nav-logo").append(`<img class="index-company-logo" src="${shopInfo.imgUrl}" title="公司logo" />`);
      parent.$(".index-nav-logo-title").text(companyInfo.companyName);
    }else if(companyInfo.companyName&&!shopInfo.imgUrl){
      parent.$(".index-nav-logo-title").text(companyInfo.companyName);
    }else if(!companyInfo.companyName&&shopInfo.imgUrl){
      parent.$(".index-nav-logo").html("");
      parent.$(".index-nav-logo").append(`<img class="index-company-logo" src="${shopInfo.imgUrl}" title="公司logo" />`);
      parent.$(".index-nav-logo-title").text("");
    }else if(!companyInfo.companyName&&!shopInfo.imgUrl){
      parent.$(".index-nav-logo-title").text("");
    }
  }
  /*
   * 发货地区
   * @param  sendAreaProvinceCode, 省编号
   * @param  sendAreaCityCode, 市编号
   * @author Wangshuzhong
   * @Date   2017/03/15
   */
  function renderSendArea(sendAreaProvinceCode,sendAreaCityCode) {
    $("#id-send-province").val(sendAreaProvinceCode);//省的值
    $.ajax(tokenAjax({
      type: "get",
      async: false,
      contentType: 'application/json',
      dataType:"json",
      url: QHConfig.url.citylist + sendAreaProvinceCode,//根据省获取市列表
      success: function (data) {
        var oHtml = ``;

        data=data.data;
        for (var i = 0; i < data.length; i++) {
          oHtml += `<option value=${data[i].cityid}>${data[i].city}</option>`;
        }
        $("#id-send-city").html(oHtml);
        $("#id-send-city").val(sendAreaCityCode);//市的值
      }
    }));
  }
  /*
   * 获取省列表
   * @param  proSelect, 省列表select框元素
   * @param  citySelect, 市列表select框元素
   * @param  proCode, 省code
   * @param  cityCode, 市code
   * @author Wangshuzhong
   * @Date   2017/03/15
   */
  function getAllProvince(proSelect,proCode) {
    //获取所有的省份
    $.ajax(tokenAjax({
      type: "get",
      async: false,
      contentType: 'application/json',
      dataType:"json",
      url: QHConfig.url.provincelist,
      success: function (data) {
        var sendArea = `<option value="">请选择发货省份</option>`;//发货地区省份
        var supplyArea="";//供货地区省份

        data=data.data;
        for (var i = 0; i < data.length; i++) {
          sendArea += `<option value=${data[i].provinceid}>${data[i].province}</option>`
        }
        proSelect.html(sendArea);
        proSelect.val(proCode);
      }
    }));
  }
  /*
   * 根据省code获取该省管辖的市区列表
   * @param  proSelect, 省列表select框元素
   * @param  provinceCode, 省code
   * @param  citySelect, 市列表select框元素
   * @author Wangshuzhong
   * @Date   2017/03/15
   */
  function getCityListByProvinceCode(proSelect,citySelect) {
    //选择省地区
    var provinceCode = proSelect.val();//获取省code；

    citySelect.html(`<option value="">请选择发货城市</option>`);//每次改变省列表的值后，清空市区列表
    //获取市区列表，参数provinceCode为省code
    if(provinceCode != "") {
      $.ajax(tokenAjax({
        type: "get",
        contentType: 'application/json',
        dataType:"json",
        url: QHConfig.url.citylist + provinceCode,
        success: function (data) {
          var oHtml = ``;

          data=data.data;
          for (var i = 0; i < data.length; i++) {
            oHtml += `<option value=${data[i].cityid}>${data[i].city}</option>`;
          }
          citySelect.html(oHtml);
        }
      }));
    }
  }

  /*
   * 选择供货地code
   * @params supplyCode,供货地区code
   * @author Wangshuzhong
   * @Date   2017/03/21
   */
  function chooseSupplyArea(supplyCode){
    var opt = {
      title: '提示信息(供货地区支持多选)',
      boxWidth: 870,
      boxHeight: 500,
      offX: 250,
      offY: -400,
      textField: "areaName",
      valueField: "areaCode",
      childsField: "subAreas"
    };
    var lastSelecteds=[];

    UseMultiSelector(window);
    window.multi = window.multiSelectorFactory($('#id-supplyAre-box'));//$('#mymulti')是个DIV，可以放文本
    window.multi.init(opt, getStaticData('areas'));
    if(supplyCode) {
      lastSelecteds = supplyCode.split(",");//后台获取的初始值
    }
    window.multi.render(lastSelecteds, '全国', '全省');
    $(".multi-footor input.cmn-save-btn").on("click", function () {
      $('#id-supply-area').val(String(window.multi.getValues()));
      if($('#id-supply-area').val() != supplyCode){
        parent.$("body").data("businessSave",false);//改动过商家信息的标记
      }
    });
  }

  /*
   * 判断必传参数是否漏填，漏填则在相应的地方显示提示语句，不收集数据提交；
   * 没有漏填则收集数据保存，更新商家中心
   * @author Wangshuzhong
   * @Date   2017/03/15
   */
  function checkMustDataEmpty() {
    var checkEmpty = true;//定义一个用于判断是否为空的变量
    var provinceVal = $("#id-send-province").val();//发货省
    var cityVal = $("#id-send-city").val();//发货市
    var supplyCityVal = $("#id-supply-area").val();//供货地区

    $(".bus-area-tip").hide();//初始化隐藏提示语句
    //判断发货地址是否为空
    if (provinceVal == "" || cityVal == "") {
      $("#id-send-city").parent().next(".bus-area-tip").show();
      checkEmpty = false;
    };
    //判断供货地址是否为空
    if (supplyCityVal == "") {
      $("#id-supply-area").next(".bus-area-tip").show();
      checkEmpty = false;
    };
    //都不为空则收集数据请求更新商家中心
    if (checkEmpty == true) {
      var needData = {};//声明一个用来存放收集数据的对象

      needData.id = parseInt($(".cmn-main-list").attr("main-id"));
      needData.sendGoodsAreaCode = $("#id-send-city").val();
      needData.synopsis = $(".bus-combrief-text").val();
      needData.supplyAreaCode=$('#id-supply-area').val();
      /*
       * 更新商家中心
       * 参数说明：id(long,true)主键id,sendGoodsAreaCode(string,true)发货地区,
       supplyAreaCode(string,true)供货地区，多个已逗号隔开,synopsis(string,false)公司简介
       * @author Wangshuzhong
       * @Date   2017/03/15
       */
      $.ajax(tokenAjax({
        type:"put",
        url: QHConfig.url.updateCenterInfo,
        contentType:"application/json",
        data:JSON.stringify(needData),
        headers: {
          Accept: "application/json; charset=utf-8",
          Authorization:token
        },
        success:function(data){
          if(data.status == 200) {
            var areaCode = JSON.parse(sessionStorage.getItem("areaCode"));

            layer.msg("保存成功");
            areaCode.province = $("#id-send-province").val();
            areaCode.cityCode = needData.sendGoodsAreaCode;
            areaCode.cityName = $("[value="+$("#id-send-city").val()+"]").text();
            areaCode.supplyAreaCode = needData.supplyAreaCode;
            sessionStorage.setItem("areaCode",JSON.stringify(areaCode))
            parent.$("body").data("businessSave",true);//保存商家信息的标记
          }else {
            layer.msg(data.message);
          }
        }
      }));
    }else{
      layer.msg("保存失败（发货地区或供货地区或发货地区和供货地区都没填）");
    }
  }
}
