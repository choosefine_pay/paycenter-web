/**
 * Created by   :
 * Created time :
 * File explain :
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    vue: ["./vue2-4-1/vue"],
	header:["../components/my-header"],
    common: ["../common/js/common-config"]
  }
});
require(["jquery"],function() {

  require(["vue","header","common"], function (Vue,header) {
    var QHConfig = $("body").data("config");
    var roleArr = ["材料供应商", "设施供应商", "设备供应商"];
    var roles = {"1":"worker.jpg","2":"group.jpg","3":"manager.jpg","4":"company.jpg","5":"unit.jpg","6":"mater.png", "7":"equip.png", "8":"install.png"};
    new Vue({
      el: "#choice-container",
      data: {
        role: JSON.parse(sessionStorage.getItem("userInfo")),
        active: 0,
        sellerRole: null
      },
      methods: {
        intoSellerCenter: function () {
          if (this.active != null) {
            var role = roles[this.role[this.active].roleId];
            sessionStorage.setItem("seller_role", role );
			var opt2={
              url: QHConfig.url.selectRole+"?userCode="+this.role[this.active].userCode,
			  type:"POST",
			  data:{
					geetestChallenge:'1',
					geetestValidate:'2',
					geetestSeccode:'3'
				  },
              success: function (data) {
				 if(data.status==200){
					sessionStorage.setItem('sellerToken', data.token);
					location.href = "../../tpl/mdu-sxb-account/mdu-sxb-account.html" 
				 }
              }
            }
            $.ajax(tokenAjax(opt2));
          }
        },
        roleClick: function (i) {
          this.active = i;
        }
      },
      filters: {
        widthFormat: function (value) {
          var percent = (1 / value).toFixed(4) * 100 + "%";
		  var h=1560/value;
          return "width: " + percent+";height:"+h+"px;";
        },
        imgFormat: function (roleId) {
          var img = roles[roleId];
          return "background:#fff url(../img/common/" + img + ") no-repeat center center;" +
            "background-size: 80% auto";
        }
      }
    })
	window.initTop(Vue);
  });
});