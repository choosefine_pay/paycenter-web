/**
 * Created by   :宁建浩
 * Created time :2017/3/15/9:38
 * File explain :材料-商品管理
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
	vue: ["./vue2-4-1/vue"],
	header:["../components/pay-header"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    bootTable: ["bootstrap-table-dist/dist/js/bootstrap-table"],
    bootTableCN: ["bootstrap-table-dist/dist/locale/bootstrap-table-zh-CN"],
    config: ["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["bootTable"], function () {
    require(["vue","header","layer", "zui", "boot", "bootTableCN", "config"], operationCodeFun)
  });
});
function operationCodeFun(vue,header) {

  $("#id-projectName").on("click",function(){
    parent.parent.parent.layer.closeAll()
  });
  window.initTop(vue,'忘记密码');
}