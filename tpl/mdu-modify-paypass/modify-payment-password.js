/**
 * Created by   :宁建浩
 * Created time :2017/4/13/13:38
 * File explain :施小包账户信息
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
	vue: ["./vue2-4-1/vue"],
	header:["../components/pay-header"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    bootTable: ["bootstrap-table-dist/dist/js/bootstrap-table"],
    bootTableCN: ["bootstrap-table-dist/dist/locale/bootstrap-table-zh-CN"],
    config: ["../common/js/common-config"],
    page: ["../common/js/qhpager"]
  }
});
require(["jquery"], function () {
  require(["bootTable"], function () {
    require(["vue","header","layer", "zui", "boot", "bootTableCN", "config", "page"], operationCodeFun)
  });
});
//代码容器

function operationCodeFun(vue,header) {
  "use strict";
  const QHConfig = $("body").data("config");//配置文件常量
  var userCode = window.sessionStorage.getItem("userCode");//获得userCode
  var manageInfo = JSON.parse(window.sessionStorage.getItem("detail"));//获得角色信息
  var userInfo = QHConfig.getSearchDataFun();//获取地址参数
  var pwdInterval=null;
  //layer框架配置
  layer.config({
    path: "../../libs/layer/"
  });
  window.initTop(vue,'修改支付密码');
  //密码输入事件
  $("#id-input-password").on("keyup", passwordBlur);
  $(".clear-img").on("click",clearPwd);
  $(".clear-img-con input").on("focus",passwordFocus);
  //设置完密码下一步
  $("#id-next-step").on("click", passwordNext);
  //返回账号按钮
  $("#id-back-account,#id-back-account2").on("click", backAccountFun);
  //新旧密码点击
  $("#id-preserve-btn").on("click", passwordSub);
  //
  $(".cmn-query-import[data-name='newPayment']").on("keyup", passwordkeyUp);

  $("#id-project-name").on("click", function () {
    parent.$("body").find(".index-sxb-list")[0].click();
  });
  //如果有密码，否则设置密码
  if (manageInfo.isSetPayPass) {
    $(".szmm").html("原始");
    $("#id-next-step").val("下一步");
    $("#id-preserve-btn").val("保存");
  } else {
    $("#id-pwd-name").html("设置");
    $(".szmm").html("设置");
    $("#id-next-step").val("保存");
  }
  
  //密码失去焦点验证
  function passwordBlur() {
    var btnFlag = false;
    var val = $(this).val();
    var txt = $(this).parents(".cmn-query-container").find(".cmn-password-yz").html();
    if (!val) {
      $(this).parent().siblings("p").removeClass("hide").html("请先输入" + txt);
      btnFlag = false;
    } else if (val.length != 6 || (!QHConfig.testReg.onlyNum.test(val))) {
      $(this).parent().siblings("p").removeClass("hide").html(txt + "输入格式不正确，请重新输入");
      $(this).siblings(".icon-qh-gou").addClass("icon-times").removeClass("icon-check");
      btnFlag = false;
    } else {
      $(this).parent().siblings("p").addClass("hide");
      $(this).siblings(".icon-qh-gou").removeClass("icon-times").addClass("icon-check");
      btnFlag = true;
    }
    if (btnFlag) {
      $("#id-next-step").removeClass("cmn-next-pass1").addClass("cmn-next-pass").prop("disabled", false);
    } else {
      $("#id-next-step").addClass("cmn-next-pass1").removeClass("cmn-next-pass").prop("disabled", true);
    }

  }

  //
  function passwordkeyUp() {
    var val = $(this).val();
    $(".cmn-fail-password").addClass("hide");
    var sub1 = $("#id-sub-password").val();
    var sub2 = $("#id-sub-password2").val();
    var txt = $(this).parents(".cmn-query-container").find(".cmn-password-yz").html();
    if (!val) {
      $(this).parent().siblings("p").removeClass("hide").html("请输入" + txt);
    } else if (val.length != 6 || (!QHConfig.testReg.onlyNum.test(val))) {
      $(this).parent().siblings("p").removeClass("hide").html(txt + "输入格式不正确，请重新输入");
    } else {
      $(this).parent().siblings("p").addClass("hide");
    }
    if(sub1.length == 6 && (QHConfig.testReg.onlyNum.test(sub1))){
      $("#id-sub-password").siblings(".icon-qh-gou").removeClass("icon-times").addClass("icon-check");
    }else{
      $("#id-sub-password").siblings(".icon-qh-gou").addClass("icon-times").removeClass("icon-check");
    }
    if (QHConfig.testReg.onlyNum.test(sub1) && QHConfig.testReg.onlyNum.test(sub2) && sub1.length == 6 && sub2.length == 6) {
      $("#id-preserve-btn").removeClass("cmn-next-pass1").addClass("cmn-next-pass").prop("disabled", false);
    } else {
      $("#id-preserve-btn").addClass("cmn-next-pass1").removeClass("cmn-next-pass").prop("disabled", true);
    }
    if(sub1==sub2&&sub1.length==6){
      $("#id-sub-password2").siblings(".icon-qh-gou").removeClass("icon-times").addClass("icon-check");
    }else{
      $("#id-sub-password2").siblings(".icon-qh-gou").removeClass("icon-check").addClass("icon-times");
    }
  }

  //下一步操作
  function passwordNext() {
    var passwordTxt = $("#id-input-password").val();
    var msg = {
      userCode: userCode,//userCode
      payPass: passwordTxt//密码
    };
    //如果有密码，否则设置密码
    if (manageInfo.isSetPayPass) {
      ajaxData.viewOldPwd(passwordTxt);
    } else {
      if (passwordYz(passwordTxt)) {
        ajaxData.setPassword(msg);
      } else {
        $(".cmn-fail-password").html("支付密码输入格式不正确，请重新输入").removeClass("hide");
      }
    }

  }

  //密码验证
  function passwordYz(passowrd) {
    //验证密码字符长度和格式
    if (QHConfig.testReg.onlyNum.test(passowrd) && passowrd.length == 6) {
      return true;
    } else {
      return false;
    }
  }

  //新旧密码验证
  function passwordSub() {
    var oldPass = $("#id-get-password").val();//
    var newPass2 = $("#id-sub-password2").val();
    var newPass = $("#id-sub-password").val();
    var msg = {
      oldPayPass: oldPass,
      payPass: newPass
    };
    //验证两次密码
    if (newPass != newPass2) {
      $(".cmn-fail-password").removeClass("hide").html("两次密码不同，请重新输入");
    }else if(!newPass || !newPass2){
      $(".cmn-fail-password").removeClass("hide").html("请输入新支付密码");
    }
    else {
      ajaxData.updatePassword(msg);
    }
  }

  //返回我的账户
  function backAccountFun() {
	/*
    manageInfo.isSetPayPass=true;
    window.sessionStorage.setItem("detail",JSON.stringify(manageInfo));
    $("#id-pwd-name").html("修改");
    $(".szmm").html("原始");
    parent.$("#id-password-status").html("修改支付密码");
    parent.$("li[data-field='mdu-sxb-account']").click();
	*/
	window.close();
  }

  //ajax提交
  var ajaxData = {
    setPassword: function (password) {
      $.ajax(tokenAjax({
        type: "put",
        async: false,
        url: "/pay/account/accountPasswordInit?payPass=" + password.payPass,
        success: function (data) {
          if (data.status == 200) {
            $("#id-set-password").addClass("hide").siblings("#id-set-success-before").removeClass("hide");
            if(userInfo.tradeType=="SALARY"){
              pwdInterval=null;
              pwdInterval=setTimeout(function(){
                parent.layer.open({
                  title: false,
                  shade: 0.8,
                  move: false,
                  closeBtn: 0,
                  resize: false,
                  scrollbar: false,
                  type: 2,
                  area: ['100%', '100%'],
                  content: "pages/buyer-pay/buyer-pay.html" + "?transfer=SALARY" + "&balance=" + manageInfo.availableBalance
                  + "&amount=" + userInfo.amount + "&bizzSn=" + userInfo.bizzSn + "&operName=" + manageInfo.realName +  "&userCode=" + manageInfo.userCode+"&bizzSys=LABOR"
                })
              },2000)
            }
          } else {
            layer.alert("网络错误");
          }
        }
      }));
    },
    updatePassword: function (password) {
      var cctv = $("body").data("cctv");
      $.ajax(tokenAjax({
        type: "put",
        async: false,
		url: QHConfig.url.passEdit,
		data:{oldPayPass:cctv,payPass:password.payPass},
        success: function (data) {
          if (data.status == 200) {
            $("#id-set-success").removeClass("hide").siblings("#id-new-password").addClass("hide");
          } else {
            layer.alert(data.message);
          }
        }
      }));
    },
    //验证原始密码
    viewOldPwd: function (password) {
      $.ajax(tokenAjax({
        type: "put",
        async: false,
		url: QHConfig.url.passVerify,
		data:{payPass:password},
        success: function (data) {
          if (data.status == 200) {
            $("#id-set-password").addClass("hide").siblings("#id-new-password").removeClass("hide");
            $("body").data("cctv", password);
          } else {
            if(data.message.indexOf("分钟")){
              $("#id-next-step").siblings(".cmn-set-password-main").find(".cmn-fail-password").removeClass("hide").html(data.message);
              return;
            }
            $("#id-next-step").siblings(".cmn-set-password-main").find(".cmn-fail-password").removeClass("hide").html("原始支付密码输入不正确，请重新输入");
          }
        }
      }));
    }
  };
  //点击删除密码
  function clearPwd() {
    $(this).siblings(".icon-qh-gou").addClass("icon-times").removeClass("icon-check");
    if($(this).siblings("input").attr("id")=="id-sub-password2"){
      $(this).siblings(".icon-qh-gou").addClass("icon-times").removeClass("icon-check").parent().siblings(".confirm-password-prompt").html("请输入新支付密码").removeClass("hide");
      $(this).parent().siblings(".cmn-fail-password").addClass("hide");
    }else if($(this).siblings("input").attr("id")=="id-input-password"){
      $(this).parent().siblings(".cmn-fail-password").addClass("hide");
    }else{
      $(this).siblings(".icon-qh-gou").addClass("icon-times").removeClass("icon-check").parent().siblings(".new-password-prompt").html("请输入新支付密码").removeClass("hide");
    }
    $(this).siblings().val("");
    $("#id-next-step").prop("disabled", true).addClass("cmn-next-pass1");
    $(this).css("visibility","hidden");
  }
  //鼠标获取焦点
  function passwordFocus(){
    $(this).siblings(".clear-img").css("visibility","visible");
  }
}