/**
 * Created by   :chengwei
 * Created time :2017/5/17
 * File explain :施小包账户-充值--成功页面
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    config:["../common/js/common-config"]
  }
});
require(["jquery"], function () {
  require(["layer","zui","boot","config"], operationCodeFun)
});
//代码容器

function operationCodeFun() {
  var QHConfig = $("body").data("config");//公共文件
  var userCode=window.sessionStorage.getItem("userCode");
  var userInfo = QHConfig.getSearchDataFun();//获取地址栏参数
  //layer框架配置
  layer.config({
    path:"../../libs/layer/"
  });
  console.log(userInfo);
  //页面渲染
  if(userInfo.success=="true"){
    $(".success-operation").show();
    $(".transfer-money").text(userInfo.money);
  }else if(userInfo.success=="false"){
    $(".fail-operation").show();
  }
  /*事件操作*/
  //返回我的账户
  $(".bank-account,.back").on("click",function() {
    parent.parent.parent.layer.closeAll();
    parent.parent.parent.parent.$("#id-main-frame").attr("src","tpl/mdu-sxb-account/mdu-sxb-account.html");
  });
  //查看转账记录
  $(".look-transfer-record").on("click",function() {
    parent.parent.parent.layer.closeAll();
    parent.parent.parent.parent.$("#id-main-frame").attr("src", "tpl/mdu-deal-record/mdu-deal-record.html").click();
  });
  //继续转账
  $(".continue-transfer").on("click",function() {
    parent.parent.parent.layer.closeAll();
    parent.parent.parent.$(".carry-btn").click();
  });
  /*to do 其他函数*/

}
