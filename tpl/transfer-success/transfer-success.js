/**
 * Created by Administrator on 2017/6/1 0001.
 */
require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    boot: ["bootstrap-3.3.7-dist/js/bootstrap"],
    config: ["../common/js/common-config"],
  }
});
require(["jquery"], function () {
  require(["layer", "zui", "boot", "config"], operationCodeFun)
});
//代码容器

function operationCodeFun() {
  var QHConfig = $("body").data("config");//公共文件
  var userCode=window.sessionStorage.getItem("userCode");
  var userInfo = QHConfig.getSearchDataFun();//获取地址栏参数
  //layer框架配置
  layer.config({
    path:"../../libs/layer/"
  });
  //页面渲染
  $(".amount").text(userInfo.amount);
  /*事件操作*/
  //返回我的账户
  $(".back-account,.account").on("click",function() {
    parent.parent.parent.layer.closeAll();
  });
  //查看转账记录
  $(".look-account").on("click",function() {
    parent.parent.parent.layer.closeAll();
    parent.parent.parent.parent.$("#id-main-frame").attr("src", "tpl/mdu-deal-record/mdu-deal-record.html?type=2").click();
  });
  //继续转账
  $(".go-pay").on("click",function() {
    parent.parent.layer.closeAll();
  });
  /*to do 其他函数*/
}