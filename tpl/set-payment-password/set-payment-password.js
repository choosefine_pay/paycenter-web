/**
 * Created by   :chen
 * Created time :
 * File explain :
 */

require.config({
  baseUrl: "../../libs",
  paths: {
    jquery: ["jquery/jquery-1.11.3"],
	vue: ["./vue2-4-1/vue"],
	header:["../components/pay-header"],
    layer: ["layer/layer"],
    zui: ["zui/js/zui"],
    comConfig: ["../common/js/common-config"]
  }
});
require(["jquery"],function() {
  require(["vue","header","zui", "layer", "comConfig"], operationCodeFun)
});
function operationCodeFun(vue,header) {
  // todo 声明变量
  // 由于使用require框架整合，我们需要为layer.js配置读取css文件的路径
  layer.config({
    path: "../../libs/layer/"
  });
  window.initTop(vue,'设置支付密码');
  var QHConfig = $("body").data("config");
  //todo 初始化页面渲染

  //todo 点击事件
  //点击面包屑
  $("#id-projectName,.success-return-btn").on("click",myAccount);
  //输入密码的框
  $("#id-payPass").on("blur keyup",hintShow);
  $("#id-payPass").on("focus",function() {
    if($(this).val() != "") {
      $(this).siblings(".clear-img").css("visibility","visible");//显示清除按钮
    }
  }).on("blur",function() {
    setTimeout(function(){
      $(".clear-img").css("visibility","hidden");//隐藏清除按钮
    },300)
  }).on("keyup",function() {
    if($(this).val() == "") {
      $(this).siblings(".clear-img").css("visibility","hidden");//隐藏清除按钮
    }else {
      $(this).siblings(".clear-img").css("visibility","visible");//显示清除按钮
    }
  });
  //点击的叉，清空内容
  $(".clear-img").on("click",function () {
    $("#id-payPass").val("");//清除内容
    $("#id-payPass").focus();
    $(this).css("visibility","hidden");
    return false;
  });
  //保存按钮
  $("#id-preserve-btn").on("click",setPayPass);
  //todo 其他功能函数
  //返回我的账户
  function myAccount() {
	  window.close();
	//top.location.href="../../tpl/mdu-sxb-account/mdu-sxb-account.html";
    //parent.$("#id-main-frame").attr("src","tpl/mdu-sxb-account/mdu-sxb-account.html").click();
  }
  //提示条件显示和隐藏
  function hintShow() {
    var reg = new RegExp("^[0-9]*$");
    var val = $(this).val();
    if(val.length==0||val.length<6){
      $(".hint").text("").addClass("hide");
      $("#id-preserve-btn").removeClass("cmn-preserve-active-btn").attr("disabled",true)
    }else {
      if(!reg.test(val)){
        $(".hint").text("支付密码输入格式不正确，请重新输入").removeClass("hide");
        $("#id-preserve-btn").removeClass("cmn-preserve-active-btn").attr("disabled",true)
      }else{
        $(".hint").text("").addClass("hide");
        $("#id-preserve-btn").addClass("cmn-preserve-active-btn").attr("disabled",false)
      }
    }
  }
  //保存按钮设置支付密码
  function setPayPass() {
    var pass=$("#id-payPass").val();
    $.ajax(tokenAjax({
      url:QHConfig.url.initPayPass,
      type:"put",
      data:JSON.stringify({payPass:pass}),
      contentType: "application/json",
      dataType:"json",
      success:function(data) {
        if(data.status==200){
          $(".setPass").remove();
          $(".success").removeClass("cmn-set-success");
        }else{
          layer.msg(data.message)
        }
      }
    }))
  }
}