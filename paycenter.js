var pubconfig = {
    //网关地址
    "gatewayIp": "//shopping-web-test.sxiaobao.com",
    "gatewayPort": "80",
    "gatewayPath": "/web/shopping",
    //开发用地址（运维可以忽略）
    "serverIp": "//192.168.59.3",
    "serverPort": "6091",
    "serverPath": "/Authority/send/paycenter/usercenter",
    //开发用支付地址（运维可以忽略）
    "serverpayIp": "//192.168.59.3",//auth-test-service.sxiaobao.com
    "serverpayPort": "6091",//80
    "serverpayPath": "/Authority/send",
    //图片上传地址
    "photoIp": "//192.168.59.2",
    "photoPort": "7090",
    "photoPath": "",
    //支付中心地址
    "paycenterIp": "//pay-test.sxiaobao.com",
    "paycenterPort": "80",
    "paycenterPath": "",
    //富文本编辑器地址
    "editorIp":"//sellercenter-test.sxiaobao.com",
    "editorPort":"80",
    "editorPath":"/ueditor/common/ueditor1_4_3-utf8-jsp/jsp/controller.jsp",
    // api 配置
    appKey: '23839157',
    appSecret: '90887ff813d92aa93af4a0e45fcfc540',
    form: 'application/x-www-form-urlencoded',
    stage: "RELEASE",
    //true为开发调试用，线上统一用false
    "isTest": true
}



